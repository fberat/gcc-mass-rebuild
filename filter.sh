#!/bin/bash

INDIR=
OUTDIR="gcc-mass-rebuild"
FILTER=
CATEGORY=
NEGATIVE=0

usage="Usage: $(basename ${0}) -i{INPUT DIRECTORY} [-d{DESTINATION}] -f{FILTER} -c{CATEGORY} [-n]"
where=" Where :\n
	i: Input directory containing the logs from an MPB build\n
	d: Root of the destination folder. Default \`gcc-mass-rebuild\`\n
	f: Apply FILTER\n
	n: negate the filter (i.e. log contains 'error: ' but not FILTER)\n
	c: CATEGORY where to put the file in
"
OPTS=`getopt -o 'i:d:f:c:n' -- "$@"`

ret=$?

if [ ! ${ret} = 0 ]
then
  echo ${usage};
  echo -e ${where};
  exit 1
fi

eval set -- "$OPTS"

while true;
do
  case "$1" in
    -i ) INDIR="$2"; shift 2;;
    -d ) OUTDIR="$2"; shift 2;;
    -f ) FILTER="$2"; shift 2;;
    -c ) CATEGORY="$2"; shift 2;;
    -n ) NEGATIVE=1; shift;;
    --) shift; break;;
    * )
        echo $usage;
        echo -e $where;
        exit 1;;
  esac
done

if [ ! -d $INDIR ]
then
  echo "Working directory $INDIR not found"
  echo $usage
  echo -e $where
  exit 1
fi

if [ ! -d $OUTDIR ]
then
  echo "Destination directory $OUTDIR not found"
  echo $usage
  echo -e $where
  exit 1
fi

if [ "${FILTER}" = "" ]
then
  echo "Filter must be provided"
  echo $usage
  echo -e $where
  exit 1
fi

if [ "${CATEGORY}" = "" ]
then
  echo "Category must be provided"
  echo $usage
  echo -e $where
  exit 1
fi

file_list=""

for failure_type in FAILED
do
  for arch in x86_64 aarch64 i386 s390x ppc64le
  do
    if [ ! -d "${INDIR}/${failure_type}/fedora-rawhide-${arch}" ]
    then
      continue
    fi
    cd "${INDIR}/${failure_type}/fedora-rawhide-${arch}" > /dev/null
    file_list="$file_list $(ls)"
    cd - > /dev/null
  done
done

components=$(echo "$file_list" | xargs -n1 | sort -u | xargs)
count=$(echo "$file_list" | xargs -n1 | sort -u | wc -l)

echo "Got ${count} components to process"

FILTERED_RESULTS="${OUTDIR}/${CATEGORY}"

mkdir -p ${FILTERED_RESULTS}
mkdir -p "${OUTDIR}/ALL"
root=$PWD

find ${root}/${INDIR} -name builder-live.log.gz -exec gunzip {} \;

echo "Unzipped all logs"

for comp in $components
do
  for arch in x86_64 aarch64 i386 s390x ppc64le
  do
    if [ ! -d "${INDIR}/FAILED/fedora-rawhide-${arch}" ]
    then
      continue
    fi

    cd "${INDIR}/FAILED/fedora-rawhide-${arch}" > /dev/null
    for log in $(ls ${comp}/*/builder-live.log 2>/dev/null)
    do
      if [ ! -f "$(realpath ${log})" ]
      then
        continue
      fi

      last=$(tail -n 1 ${log})
      found=$(echo $last | grep "RPMResults finished")

      if [ "${found}" = "" ]
      then
        errors=$(grep -iE "error:|error :" ${log})
        if [ "${errors}" = "" ]
        then
          continue
        fi

        size=$(stat -c%s $(realpath $log))
        name="builder-live.log"
        if [ $size -ge 104857600 ]
        then
          name="${name}.gz"
        fi

        mkdir -p ${root}/${OUTDIR}/ALL/GCC-14/by_components/${comp}/${arch}
        mkdir -p ${root}/${OUTDIR}/ALL/GCC-14/by_arch/${arch}
        if [ ! -f ${root}/${OUTDIR}/ALL/GCC-14/by_components/${comp}/${arch}/${name} ]
        then
          cp $(realpath $log) ${root}/${OUTDIR}/ALL/GCC-14/by_components/${comp}/${arch}/
          if [ $size -ge 104857600 ]
          then
            gzip ${root}/${OUTDIR}/ALL/GCC-14/by_components/${comp}/${arch}/builder-live.log
          fi
          ln -rs -T ${root}/${OUTDIR}/ALL/GCC-14/by_components/${comp}/${arch} ${root}/${OUTDIR}/ALL/GCC-14/by_arch/${arch}/${comp}
        fi

        checker_dirlog=$(ls ${root}/${INDIR}.checker/SUCCESS/fedora-rawhide-${arch}/${comp}/*/builder-live.log* 2>/dev/null)
        if [ ! -z $checker_dirlog ] && [ -f "$(realpath $checker_dirlog)" ]
        then
          size=$(stat -c%s $(realpath $checker_dirlog))
          name="builder-live.log"
          if [ $size -ge 104857600 ]
          then
            name="${name}.gz"
          fi

          mkdir -p ${root}/${OUTDIR}/ALL/GCC-13/by_components/${comp}/${arch}/
          mkdir -p ${root}/${OUTDIR}/ALL/GCC-13/by_arch/${arch}
          if [ ! -f ${root}/${OUTDIR}/ALL/GCC-13/by_components/${comp}/${arch}/${name} ]
          then
            cp $(realpath $checker_dirlog) ${root}/${OUTDIR}/ALL/GCC-13/by_components/${comp}/${arch}/
            if [ $size -ge 104857600 ]
            then
              gzip ${root}/${OUTDIR}/ALL/GCC-13/by_components/${comp}/${arch}/builder-live.log
            fi
            ln -rs -T ${root}/${OUTDIR}/ALL/GCC-13/by_components/${comp}/${arch} ${root}/${OUTDIR}/ALL/GCC-13/by_arch/${arch}/${comp}
          fi
        fi

        if [ ${NEGATIVE} -eq 0 ]
        then
          errors=$(grep -iE "${FILTER}" $(realpath $log))
        else
          errors=$(grep -iE "error: |error : " $(realpath $log) | grep -ivE "${FILTER}")
        fi
        if [ "${errors}" = "" ]
        then
          continue
        fi

#        echo "-----"
#        echo ""
#        echo ${log}
#        echo ${errors}
#        echo ""
#        continue

        mkdir -p ${root}/${FILTERED_RESULTS}/by_components/${comp}/${arch}
        mkdir -p ${root}/${FILTERED_RESULTS}/by_arch/${arch}/${comp}

        if [ ! -e ${root}/${FILTERED_RESULTS}/by_components/${comp}/${arch}/GCC-14 ]
        then
          ln -rs -T ${root}/${OUTDIR}/ALL/GCC-14/by_components/${comp}/${arch} ${root}/${FILTERED_RESULTS}/by_components/${comp}/${arch}/GCC-14
        fi

        if [ ! -e ${root}/${FILTERED_RESULTS}/by_arch/${arch}/${comp}/GCC-14 ]
        then
          ln -rs -T ${root}/${OUTDIR}/ALL/GCC-14/by_components/${comp}/${arch} ${root}/${FILTERED_RESULTS}/by_arch/${arch}/${comp}/GCC-14
        fi

        if [ -z $checker_dirlog ] || [ ! -f "$(realpath $checker_dirlog)" ]
        then
          echo "Checker logs for ${comp} not found."
          continue
        fi

        if [ ! -e ${root}/${FILTERED_RESULTS}/by_components/${comp}/${arch}/GCC-13 ]
        then
          ln -rs -T ${root}/${OUTDIR}/ALL/GCC-13/by_components/${comp}/${arch} ${root}/${FILTERED_RESULTS}/by_components/${comp}/${arch}/GCC-13
        fi

        if [ ! -e ${root}/${FILTERED_RESULTS}/by_arch/${arch}/${comp}/GCC-13 ]
        then
          ln -rs -T ${root}/${OUTDIR}/ALL/GCC-13/by_components/${comp}/${arch} ${root}/${FILTERED_RESULTS}/by_arch/${arch}/${comp}/GCC-13
        fi
      fi
    done
    cd - > /dev/null
  done
done
