# Mass-prebuild gcc-14_fulldeps_aarch64 (ID:131)

This report was generated using mpb-report 1.3.2.dev1+gc4d6545.d20240103

## General information

Build location: https://copr.fedorainfracloud.org/coprs/fberat/gcc-14_fulldeps_aarch64
Chroot: fedora-rawhide
Tested architectures: x86_64

Main packages tested:

    gcc: 
        Source: (distgit) fedora


## Overall status

    8462 out of 8462 builds are done.
    Success: 7351      
    Manual confirmation needed: 315       
    Failed: 796       

## List of failed packages

    3proxy: 
        Source: (distgit) fedora
        NVR: 3proxy-0.8.13-10.fc39
        Commit/branch: eecf56326ac8e71257a3e55a548a1f2e8adbefaf
    4Pane: 
        Source: (distgit) fedora
        NVR: 4Pane-8.0-4.fc39
        Commit/branch: 22986aded456be338193cdb2024fa1008bdc455a
    BackupPC-XS: 
        Source: (distgit) fedora
        NVR: BackupPC-XS-0.62-13.fc39
        Commit/branch: 21d93a8c52121638f2b7a5ffe1ad75713473ef44
    BitchX: 
        Source: (distgit) fedora
        NVR: BitchX-1.2.1-32.fc39
        Commit/branch: f6eb5e733cc97c7938d7569d65c752ddc602ddcd
    CTL: 
        Source: (distgit) fedora
        NVR: CTL-1.5.2-22.fc39
        Commit/branch: f7270c99b5c654b54a9f7a5fc57de3f5ccd5ad58
    CuraEngine: 
        Source: (distgit) fedora
        NVR: CuraEngine-5.4.0-1.fc40
        Commit/branch: 9293b0433c12efcd9df4f9af6fc764acf6ee3dfb
    DevIL: 
        Source: (distgit) fedora
        NVR: DevIL-1.7.8-43.fc40
        Commit/branch: bd0b128ee0c4487496b6f882065d31f66d6f1f83
    FlightGear-Atlas: 
        Source: (distgit) fedora
        NVR: FlightGear-Atlas-0.5.0-0.84.cvs20141002.fc40
        Commit/branch: e05b04938d73cd1d5f9b48afc23c49e0ecbc31a2
    GMT: 
        Source: (distgit) fedora
        NVR: GMT-6.4.0-10.fc40
        Commit/branch: b1c2cda94ee77743fcdd295b158e59a0642238d1
    HepMC3: 
        Source: (distgit) fedora
        NVR: HepMC3-3.2.7-3.fc40
        Commit/branch: 9a4a5b4a8a895a3446903f6830d262216addb282
    InsightToolkit: 
        Source: (distgit) fedora
        NVR: InsightToolkit-4.13.3-15.fc39
        Commit/branch: dd41232a75b8dfa0b6ec13a624b1a19b1cd02183
    LibRaw: 
        Source: (distgit) fedora
        NVR: LibRaw-0.21.2-1.fc40
        Commit/branch: d7e52c7b1b184a08788c41f9551d8aa41dc95e82
    MagicPoint: 
        Source: (distgit) fedora
        NVR: MagicPoint-1.13a-35.fc39
        Commit/branch: 0b89a583c23fbe8fd49c9adec4da42fe48949111
    OpenMolcas: 
        Source: (distgit) fedora
        NVR: OpenMolcas-23.10-1.fc40
        Commit/branch: 7db7be8001730fa2ae603ff43c88bd9b80cc7ec8
    PDAL: 
        Source: (distgit) fedora
        NVR: PDAL-2.6.2-1.fc40
        Commit/branch: 0e98cf0c2a7a7decf8ff8b8e4c97d6340836e20b
    ProDy: 
        Source: (distgit) fedora
        NVR: ProDy-2.4.1-1.fc40
        Commit/branch: b4949247e9acc0774e747641bcd6bf947929576b
    PyMca: 
        Source: (distgit) fedora
        NVR: PyMca-5.9.2-1.fc40
        Commit/branch: f46dfaf046f25d5142380a488882ea8455d18a45
    R-ape: 
        Source: (distgit) fedora
        NVR: R-ape-5.7.1-3.fc39
        Commit/branch: 7f85e1d2829eb4354ec3065bc127fbfbc95136fb
    R-mvtnorm: 
        Source: (distgit) fedora
        NVR: R-mvtnorm-1.1.3-4.fc39
        Commit/branch: eba4aef72b93c091835a1d732c25a14c36d1e58f
    R-readxl: 
        Source: (distgit) fedora
        NVR: R-readxl-1.4.2-3.fc39
        Commit/branch: b3cf59efed3c467a8be7af071e3a52c62aba4dd8
    R-timechange: 
        Source: (distgit) fedora
        NVR: R-timechange-0.2.0-3.fc39
        Commit/branch: f96274bd0c0cc891be3e9d827d2df674780ae579
    R-tweenr: 
        Source: (distgit) fedora
        NVR: R-tweenr-2.0.2-4.fc39
        Commit/branch: 98496c35bb29469ce6dec5e5488e3fce1746bc7e
    SDL_image: 
        Source: (distgit) fedora
        NVR: SDL_image-1.2.12-34.fc39
        Commit/branch: 604ab0be515dfee63ee949e22fe432550f5efc82
    Xaw3d: 
        Source: (distgit) fedora
        NVR: Xaw3d-1.6.4-3.fc39
        Commit/branch: e5a9fd17550cd4fd4ee92cc4dd3b60594cd2e668
    adobe-afdko: 
        Source: (distgit) fedora
        NVR: adobe-afdko-3.6.1-6.fc39
        Commit/branch: ee9d73d2bf3efa10404790f368702d5f620c79cd
    afflib: 
        Source: (distgit) fedora
        NVR: afflib-3.7.20-1.fc40
        Commit/branch: 68d385b3b8fe92e74d61fcc3fb756957701c5546
    afpfs-ng: 
        Source: (distgit) fedora
        NVR: afpfs-ng-0.8.1-43.fc39
        Commit/branch: ccdbd66e4a2339b1013fb7f76cf63942ec2e6d30
    agenda: 
        Source: (distgit) fedora
        NVR: agenda-1.1.2-19.fc40
        Commit/branch: b2fac532df373616c0ee1aead044a3753e688a8c
    ags: 
        Source: (distgit) fedora
        NVR: ags-3.6.0.51-1.fc40
        Commit/branch: 364f0e9f443e8acce23b97c316945f83fc4b354d
    alfont: 
        Source: (distgit) fedora
        NVR: alfont-2.0.9-25.fc39
        Commit/branch: 50a288f94ddff6255a83af16a40daed714823130
    alienarena: 
        Source: (distgit) fedora
        NVR: alienarena-7.71.2-9.fc39
        Commit/branch: 346126569108d5331503712bae5714f80de72136
    alizams: 
        Source: (distgit) fedora
        NVR: alizams-1.9.5-1.fc40
        Commit/branch: fa4bf5836be8f372ccf3f414d93680286a634910
    alsa-tools: 
        Source: (distgit) fedora
        NVR: alsa-tools-1.2.5-9.fc39
        Commit/branch: 0abdf21640eac162e5da0693ea612a1ff58b963f
    am-utils: 
        Source: (distgit) fedora
        NVR: am-utils-6.2.0-54.fc39
        Commit/branch: 3c402764ffa0dff54e5ef46a740a3902fdb244cb
    amanda: 
        Source: (distgit) fedora
        NVR: amanda-3.5.4-4.fc40
        Commit/branch: d9c69a38839b0e095101c9dcc96a0e90bf52945c
    anet: 
        Source: (distgit) fedora
        NVR: anet-0.4.1-15.fc39
        Commit/branch: cb1841d465260adc9245036420fe8fcab3514af5
    anjuta: 
        Source: (distgit) fedora
        NVR: anjuta-3.34.0-20.fc39
        Commit/branch: 1419e08ba1683fd894be0702990603157e550015
    anthy: 
        Source: (distgit) fedora
        NVR: anthy-9100h-52.fc40
        Commit/branch: 861cb04cf9c2f67960c7e6660b0b7fbcd95ec907
    anthy-unicode: 
        Source: (distgit) fedora
        NVR: anthy-unicode-1.0.0.20211224-10.fc39
        Commit/branch: 4d0b304cfd0777804b355cafe3fc21a11bf5c48e
    apbs: 
        Source: (distgit) fedora
        NVR: apbs-3.0.0-20.fc39
        Commit/branch: 6c77a00b14148e0a3ad4ba169e657ecdc8226dc2
    appeditor: 
        Source: (distgit) fedora
        NVR: appeditor-1.1.5-2.fc40
        Commit/branch: 93ee4eb472074bd95950a6a9c2b990e6f5748fb5
    apt: 
        Source: (distgit) fedora
        NVR: apt-2.7.6-1.fc40
        Commit/branch: b590519a223891e4aabdf600f2975f5c2d5adbba
    apt-cacher-ng: 
        Source: (distgit) fedora
        NVR: apt-cacher-ng-3.7.4-6.fc39
        Commit/branch: 228314c2307fc76ec7d865a61adddb2e15b9febe
    argyllcms: 
        Source: (distgit) fedora
        NVR: argyllcms-3.1.0-1.fc40
        Commit/branch: 3b87ad08707388ca2ed68ac8a8d4c0628a0d1ec1
    arx-libertatis: 
        Source: (distgit) fedora
        NVR: arx-libertatis-1.2.1-3.fc39
        Commit/branch: df1364d51bda92b5c1b8c6fcec6a4388644e754d
    asio: 
        Source: (distgit) fedora
        NVR: asio-1.28.1-2.fc39
        Commit/branch: cf0110c52b606fc9dba78e3e1a321ac14038f9f2
    assimp: 
        Source: (distgit) fedora
        NVR: assimp-5.2.5-1.fc40
        Commit/branch: 41c8bc20b9f73db729b99bf28cdcf94d839ff2bb
    asylum: 
        Source: (distgit) fedora
        NVR: asylum-0.3.2-29.fc39
        Commit/branch: 93f2c47fe3fb4824561a0029518ae744fbbf91f8
    aterm: 
        Source: (distgit) fedora
        NVR: aterm-1.0.1-39.fc39
        Commit/branch: e6746b325a724d2aec5f251a61bd4d2fcdf77c7f
    atlas: 
        Source: (distgit) fedora
        NVR: atlas-3.10.3-24.fc39
        Commit/branch: e57edad0af3a4554dd3d3b2371698131f8d14d6b
    atop: 
        Source: (distgit) fedora
        NVR: atop-2.9.0-2.fc39
        Commit/branch: f26edcb2b9a48a552024ba9e5c677670f5233dfa
    atril: 
        Source: (distgit) fedora
        NVR: atril-1.26.1-3.fc39
        Commit/branch: 7f9290e6c1dc7ad99e7bf30ac13d74ce22bc0b20
    attract-mode: 
        Source: (distgit) fedora
        NVR: attract-mode-2.7.0-6.fc40
        Commit/branch: 6250c31982f6ced81e186ac91dba81e658526d3f
    aubio: 
        Source: (distgit) fedora
        NVR: aubio-0.4.9-18.fc39
        Commit/branch: 454ac411d2af0ebcf63cdb1bacd8f229817c27c9
    aubit4gl: 
        Source: (distgit) fedora
        NVR: aubit4gl-1.6.1.p3-1.fc40
        Commit/branch: 0ac3eb46cebd3faf92203fc63712c29dc74033bd
    autodocksuite: 
        Source: (distgit) fedora
        NVR: autodocksuite-4.2.6-20.fc39
        Commit/branch: 71baa7ba05e4b42c7c77fb829e0a72efdba94a83
    autofs: 
        Source: (distgit) fedora
        NVR: autofs-5.1.9-4.fc40
        Commit/branch: 787a553722ec9aecf0c5108065bef9ef110d7639
    aws-c-common: 
        Source: (distgit) fedora
        NVR: aws-c-common-0.6.14-9.fc39
        Commit/branch: b09f0a953195069d910ccefa4b69b5b2c32b5062
    bcc: 
        Source: (distgit) fedora
        NVR: bcc-0.28.0-1.fc40
        Commit/branch: 2cce202bb46c2628549b5ecd17d7df279db26304
    bfast: 
        Source: (distgit) fedora
        NVR: bfast-0.7.0a-32.fc39
        Commit/branch: 9666f3b3f52ce75ce411cd542afe83b3f61d27bc
    bgpq3: 
        Source: (distgit) fedora
        NVR: bgpq3-0.1.36.1-5.fc39
        Commit/branch: 0098aaa093251bb7ba216c53a39374381bab8420
    bidiv: 
        Source: (distgit) fedora
        Commit/branch: 9eabeb5e68c0c9f7173b9a6abb2c1b388a13d00f
    bind: 
        Source: (distgit) fedora
        NVR: bind-9.18.20-1.fc40
        Commit/branch: 00135d6b207d17ae97d0437c39762b30f813db6e
    bind9-next: 
        Source: (distgit) fedora
        NVR: bind9-next-9.19.18-3.fc40
        Commit/branch: 3521aec3b22d7188e7471a209f86dfe2b0190cb6
    bird: 
        Source: (distgit) fedora
        NVR: bird-2.14-1.fc40
        Commit/branch: e4cf9cbb89afdd279e2d62cda4c6c1c34373b715
    blackbox-terminal: 
        Source: (distgit) fedora
        NVR: blackbox-terminal-0.14.0-2.fc40
        Commit/branch: 67860969952347cd66bc51e5b4ca4d5d94392cf9
    blis: 
        Source: (distgit) fedora
        NVR: blis-0.7.0-12.fc39
        Commit/branch: f59643c69a8ca935d8e1d5d3835fbea28c8f12d6
    blt: 
        Source: (distgit) fedora
        NVR: blt-2.4-66.z.fc39
        Commit/branch: e043d0f08fb0bb8656468d4b5968131d893b00d5
    bltk: 
        Source: (distgit) fedora
        NVR: bltk-1.1.0-29.fc39
        Commit/branch: 29ad281a619c36b55666180986bb69692c200cdb
    bonnie++: 
        Source: (distgit) fedora
        NVR: bonnie++-2.00a-8.fc39
        Commit/branch: a65e1a33fe5ff155c0f9297d2b4c5239676fccf9
    bookworm: 
        Source: (distgit) fedora
        NVR: bookworm-1.1.3-0.10.20200414git.c7c3643.fc39
        Commit/branch: 839e6a2bef4895fdb9a62ca402d22b80ce6edfa5
    boomaga: 
        Source: (distgit) fedora
        NVR: boomaga-3.3.0-16.git7f7ad47.fc39
        Commit/branch: 5efd451833ab28e5c685705be14853bbb696efca
    boost: 
        Source: (distgit) fedora
        NVR: boost-1.81.0-10.fc40
        Commit/branch: 56ec04471d48ed4694e821f4bae37ae0e8bc3a1b
    bowtie: 
        Source: (distgit) fedora
        NVR: bowtie-1.3.1-2.fc39
        Commit/branch: aad8daf4fcb9dbaf914665693cfb2f799e2b7709
    brasero: 
        Source: (distgit) fedora
        NVR: brasero-3.12.3-6.fc39
        Commit/branch: 5289ab509758e65d3207a2532a13fca106658e4a
    bristol: 
        Source: (distgit) fedora
        NVR: bristol-0.60.11-26.fc39
        Commit/branch: 45324b0dbf95ac9146783f430b1109423828cb6a
    budgie-control-center: 
        Source: (distgit) fedora
        NVR: budgie-control-center-1.3.0-1.fc39
        Commit/branch: 7ad801ac47f206815f8eb6a476d6f61cb1e87e52
    budgie-desktop: 
        Source: (distgit) fedora
        NVR: budgie-desktop-10.8.2-1.fc40
        Commit/branch: be80af8b69ad01a250960036fc67fff04eef352b
    bygfoot: 
        Source: (distgit) fedora
        NVR: bygfoot-2.3.5-1.fc38
        Commit/branch: bccc410ffaa7b43c50570a3aaffc77c066cdefc8
    c-icap-modules: 
        Source: (distgit) fedora
        NVR: c-icap-modules-0.5.6-4.20230212gitfd1a1b7.fc40
        Commit/branch: a90c8903d3e2f0d136624719a51b514086283d27
    cachelib: 
        Source: (distgit) fedora
        NVR: cachelib-17^20231016-1.fc40
        Commit/branch: a92afc6ac0cf974c0406fc7d00fb15829698c789
    cairo-dock-plug-ins: 
        Source: (distgit) fedora
        NVR: cairo-dock-plug-ins-3.4.1-46.20210730gitf24f769.fc39
        Commit/branch: 941f1f7bfbc23794336f2b4726105174516032ac
    calf: 
        Source: (distgit) fedora
        NVR: calf-0.90.3-16.fc39
        Commit/branch: dd2f3a92b7ad9933cc7f90664190769594de53d9
    cantera: 
        Source: (distgit) fedora
        NVR: cantera-3.0.0-4.fc40
        Commit/branch: b21c0c48c4144b62ac4734b4347a7483ff3690d3
    castxml: 
        Source: (distgit) fedora
        NVR: castxml-0.6.2-1.fc40
        Commit/branch: 1a2620fc0562085a9785e0b46d8c179839d3a9c9
    casync: 
        Source: (distgit) fedora
        NVR: casync-2.gitb6e81a0-5.fc39
        Commit/branch: b8cd394022524e5af8b32a6df27ef02ad7e87eb3
    catalyst: 
        Source: (distgit) fedora
        NVR: catalyst-2.0-0.7.20201218git2fc94c5.fc39
        Commit/branch: b39784d594b8ac0d485821a8fc1adc3eb89b9197
    ceph: 
        Source: (distgit) fedora
        NVR: ceph-18.2.1-3.fc40
        Commit/branch: 804d48f2334294fcba0a708872657d327c4540bd
    certmonger: 
        Source: (distgit) fedora
        NVR: certmonger-0.79.19-2.fc40
        Commit/branch: 85e3dcf93b79fe96d73665153038e1b65f00866a
    cfdg-fe: 
        Source: (distgit) fedora
        NVR: cfdg-fe-0.1-32.fc39
        Commit/branch: beb4c6a93c5d7e5837f92fa59c638acee6fb9385
    cgnslib: 
        Source: (distgit) fedora
        NVR: cgnslib-4.4.0-2.fc39
        Commit/branch: 7c70c1dffa77a09abdee51ded56c51bf3b51af91
    charliecloud: 
        Source: (distgit) fedora
        NVR: charliecloud-0.32-3.fc39
        Commit/branch: 1ef1e330ebee0b2c2aaedcdfd1ac53d982ccb8a7
    chatterino2: 
        Source: (distgit) fedora
        NVR: chatterino2-2.4.6-8.fc40
        Commit/branch: 690191e318ee3880df9ffdd9958e88983ddc7415
    cheese: 
        Source: (distgit) fedora
        NVR: cheese-44.1-1.fc39
        Commit/branch: 2a2d1bcdb1786ba1d568d2cbadfcaac2cf3b7540
    chkrootkit: 
        Source: (distgit) fedora
        NVR: chkrootkit-0.57-4.fc40
        Commit/branch: ef133d2424adf96a3af069f263a630683960d29c
    chntpw: 
        Source: (distgit) fedora
        NVR: chntpw-1.00-13.140201.fc39
        Commit/branch: 357ed638fe49b4c6e9c05de6eb9d028e6d62d7f8
    cinnamon-control-center: 
        Source: (distgit) fedora
        NVR: cinnamon-control-center-6.0.0-1.fc40
        Commit/branch: 41e2edc4923f0cf317bf947e1b4133120f8d388a
    cinnamon-settings-daemon: 
        Source: (distgit) fedora
        NVR: cinnamon-settings-daemon-6.0.0-1.fc40
        Commit/branch: fa5aa0387e8d6b4f39727fa630ae35dbce342585
    ck: 
        Source: (distgit) fedora
        NVR: ck-0.7.1-3.fc39
        Commit/branch: ce7a25878a1ffeaabd3beebbfcc5613e236450f3
    clamsmtp: 
        Source: (distgit) fedora
        NVR: clamsmtp-1.10-40.fc39
        Commit/branch: fd3d2de5828ec1aea4246954a8df93c9d92f8408
    clazy: 
        Source: (distgit) fedora
        NVR: clazy-1.11-9.fc40
        Commit/branch: 861d4b0f29565332086c3e1e18f4dc8ef7b89d85
    clearsilver: 
        Source: (distgit) fedora
        NVR: clearsilver-0.10.5-76.fc39
        Commit/branch: e299b616277310535e6f0ba461908a4138d9fc67
    clipit: 
        Source: (distgit) fedora
        NVR: clipit-1.4.5-4.D20210513gite5fa64c.fc39.5
        Commit/branch: c1140b63b34a48ad6b3adbb5727df8956a0c6706
    clonekeen: 
        Source: (distgit) fedora
        NVR: clonekeen-0.8.4-28.fc40
        Commit/branch: 1eacb4d21a56109670de8d6b48e6b35c388f0b3a
    cloudy: 
        Source: (distgit) fedora
        NVR: cloudy-17.03-4.fc39
        Commit/branch: 196fd971fcd139c7a393aaca54b12a15235554ce
    clustal-omega: 
        Source: (distgit) fedora
        NVR: clustal-omega-1.2.4-17.fc39
        Commit/branch: 2e6109531ca712abf8ff9d74fc38949af5b82a6e
    cmake: 
        Source: (distgit) fedora
        NVR: cmake-3.27.7-1.fc40
        Commit/branch: e4497063d8fc85624ecd7f34ec086ef5b833e0f5
    codec2: 
        Source: (distgit) fedora
        NVR: codec2-1.2.0-2.fc39
        Commit/branch: bfb98c5e760f5774a3f49b7138e41f8984edf55f
    coin-or-Cbc: 
        Source: (distgit) fedora
        NVR: coin-or-Cbc-2.10.5-13.fc39
        Commit/branch: fe880627d97a48b96c431d2a0a9f7668c872b850
    coin-or-SYMPHONY: 
        Source: (distgit) fedora
        NVR: coin-or-SYMPHONY-5.6.17-11.fc39
        Commit/branch: 4a2d7a4c1ef8fb6e5a3a44df4783da7f3d2f2ddd
    collectd: 
        Source: (distgit) fedora
        NVR: collectd-5.12.0-35.fc40
        Commit/branch: fcee6f7d11dd3cea89dedb9fb9dcd648354610dc
    conntrack-tools: 
        Source: (distgit) fedora
        NVR: conntrack-tools-1.4.7-3.fc39
        Commit/branch: d409463782851e7c698461e1e6da678ed42095a5
    contractor: 
        Source: (distgit) fedora
        NVR: contractor-0.3.5-14.fc39
        Commit/branch: c824275efef058d6b0ce48c157895b49d4bbfee6
    corosync: 
        Source: (distgit) fedora
        NVR: corosync-3.1.8-1.fc40
        Commit/branch: a39be7a0603803f428c67af9f39bb93fd83ebe65
    cptutils: 
        Source: (distgit) fedora
        NVR: cptutils-1.77-1.fc40
        Commit/branch: dbb2177ed07fade4c57910c1fadf14f69877530d
    cr-marcstevens-snippets: 
        Source: (distgit) fedora
        NVR: cr-marcstevens-snippets-0^20210722gite01ae88-2.fc40
        Commit/branch: 0a2966f8d228a480864341970456131db2266d6a
    crack: 
        Source: (distgit) fedora
        NVR: crack-5.0a-43.fc40
        Commit/branch: fdebc79a3ec9595ebfe1e785df06d4db6f8c74d1
    crm114: 
        Source: (distgit) fedora
        NVR: crm114-0-28.20100106.fc39
        Commit/branch: 9b6f0983de793539c6aa77c01bdf44378e4fe81e
    cronie: 
        Source: (distgit) fedora
        NVR: cronie-1.7.0-2.fc40
        Commit/branch: 550f5c0f699a175a025199673d7116bb7b4fd235
    crossfire: 
        Source: (distgit) fedora
        NVR: crossfire-1.71.0-26.fc39
        Commit/branch: 07c26b71f8c3c317895570d23fd7690e9b2ae083
    ctpl: 
        Source: (distgit) fedora
        NVR: ctpl-0.3.4-18.fc39
        Commit/branch: 42614d2c80ede83e8adf020a5f282be3eab82ce9
    cups: 
        Source: (distgit) fedora
        NVR: cups-2.4.7-6.fc40
        Commit/branch: 6f839d5bfa18b0ecfd5871c05088ba77e6cdf4f1
    curlftpfs: 
        Source: (distgit) fedora
        NVR: curlftpfs-0.9.2-34.fc39
        Commit/branch: 48d9c55bfbf083f55f191373e1571272d054885b
    cutter: 
        Source: (distgit) fedora
        Commit/branch: 841ab25cbfd01548604745b6bf3131097281e281
    cvise: 
        Source: (distgit) fedora
        NVR: cvise-2.9.0-1.fc40
        Commit/branch: 022e8b4f09df3d7d205d95d4315d280e1df5299f
    daala: 
        Source: (distgit) fedora
        NVR: daala-0-25.20200724git694d4ce.fc39
        Commit/branch: 6054394f3def6d6d28d11c2382f7288d035fb089
    dav1d: 
        Source: (distgit) fedora
        NVR: dav1d-1.2.1-2.fc39
        Commit/branch: 1bd20cd3290b35c674f08e12afa29e2f0e251c4c
    davix: 
        Source: (distgit) fedora
        NVR: davix-0.8.5-3.fc40
        Commit/branch: 70748603925d4e2f6c28de16a7baf8bcb0e599ed
    dcm2niix: 
        Source: (distgit) fedora
        NVR: dcm2niix-1.0.20220720-4.fc39
        Commit/branch: dc123a848a6e499e66440fa1e0e8cfd1846ce831
    dcmtk: 
        Source: (distgit) fedora
        NVR: dcmtk-3.6.7-4.fc39
        Commit/branch: 3e6cc2237a193445b46f7131a3f58ac6daba9845
    denemo: 
        Source: (distgit) fedora
        NVR: denemo-2.6.0-7.fc39
        Commit/branch: 782c6ac90cbe181511d92655cd701f4a0a21d1ac
    dev86: 
        Source: (distgit) fedora
        NVR: dev86-0.16.21-24.fc40
        Commit/branch: cc261cf26c087b4f76a29fdde7231f9b6fb37ddb
    dia: 
        Source: (distgit) fedora
        NVR: dia-0.97.3-24.fc39
        Commit/branch: ecb3b58ee6bd0fb24a1227956749416dff21ae70
    dieharder: 
        Source: (distgit) fedora
        NVR: dieharder-3.31.1-36.fc39
        Commit/branch: bad385f5b7772d3c655aaae6c856c240cb606529
    dippi: 
        Source: (distgit) fedora
        NVR: dippi-4.0.6-5.fc40
        Commit/branch: b1d801603d1389c7d4cb3b136178858857bf26f1
    discount: 
        Source: (distgit) fedora
        NVR: discount-2.2.7-6.fc40
        Commit/branch: 8ae3328d52a9a1e39717fc8b748b308f4fbe30ea
    disk-utilities: 
        Source: (distgit) fedora
        NVR: disk-utilities-2021.03.20-6.fc39
        Commit/branch: 286e62ff4114dca0728f6aa615046d48253ac0df
    dlm: 
        Source: (distgit) fedora
        NVR: dlm-4.2.0-3.fc39
        Commit/branch: 49dabd01271cdf24ee5fe5d5278c0505733385ee
    dnsperf: 
        Source: (distgit) fedora
        NVR: dnsperf-2.12.0-2.fc39
        Commit/branch: bb24303f243711856b47c703ea1e2230b32dc731
    doxygen: 
        Source: (distgit) fedora
        NVR: doxygen-1.10.0-1.fc40
        Commit/branch: ad93326fec45e08698d12b0ac89eded209e518eb
    dpdk: 
        Source: (distgit) fedora
        NVR: dpdk-22.11.1-2.fc39
        Commit/branch: 50d53314c90666c4c38d0fc2253cf05f247bbfe6
    dsniff: 
        Source: (distgit) fedora
        NVR: dsniff-2.4-0.42.b1.fc39
        Commit/branch: e609dfc8836e2d3ce21371f9fb34c0e515a3791a
    dvblast: 
        Source: (distgit) fedora
        NVR: dvblast-3.4-11.fc39
        Commit/branch: 45fa39262c48d2e92f734a7383a59c2fcc5c2390
    dvdauthor: 
        Source: (distgit) fedora
        NVR: dvdauthor-0.7.2-21.fc39
        Commit/branch: ecf12a97eadac2b33b1b0f7aa7754c0197ae8241
    dvdbackup: 
        Source: (distgit) fedora
        NVR: dvdbackup-0.4.2-23.fc39
        Commit/branch: 3a008290603f16dd0c8593fd9f5f4c574016197e
    dx: 
        Source: (distgit) fedora
        NVR: dx-4.4.4-63.fc39
        Commit/branch: 446cb800b4cf742d7ecbf5e2a4d60e7e14aed5ac
    easystroke: 
        Source: (distgit) fedora
        NVR: easystroke-0.6.0-42.fc39
        Commit/branch: f98863aad0507620497f75e3fce41c7339d99081
    eb: 
        Source: (distgit) fedora
        NVR: eb-4.4.3-22.fc39
        Commit/branch: dbb6110c31d9f8fdb7ff0933f424a2f5aaf608a3
    ebnetd: 
        Source: (distgit) fedora
        NVR: ebnetd-1.0-47.fc39
        Commit/branch: a7112951bab07f42d47b90df635f67a638d388cc
    echolinux: 
        Source: (distgit) fedora
        NVR: echolinux-0.17a-33.fc39
        Commit/branch: ce2938fd6a1744fb7ef326bd5a392a0c6b6b5123
    elementary-calculator: 
        Source: (distgit) fedora
        NVR: elementary-calculator-2.0.2-1.fc40
        Commit/branch: 8fbb471a43e0d620f26a30317bd6ca5beadb847e
    elementary-calendar: 
        Source: (distgit) fedora
        NVR: elementary-calendar-7.0.0-1.fc40
        Commit/branch: b50e2104dcf88761927b7ff972474f70b5c1b057
    elementary-camera: 
        Source: (distgit) fedora
        NVR: elementary-camera-6.2.2-1.fc40
        Commit/branch: 4e119bcc2b9c87b0ba714923ec1ef7d906bd5e18
    elementary-code: 
        Source: (distgit) fedora
        NVR: elementary-code-7.1.0-1.fc40
        Commit/branch: 7bd6ff29c66cee228ea48c32f9ab7d524f02ace7
    elementary-files: 
        Source: (distgit) fedora
        NVR: elementary-files-6.5.3-1.fc40
        Commit/branch: c23f11aceaa7b72b8772dbb1d4c1428b5e5bf2a0
    elementary-mail: 
        Source: (distgit) fedora
        NVR: elementary-mail-7.2.0-1.fc40
        Commit/branch: d1e6d0b13583fee30cd4883c206e5ed63fa08ec4
    elementary-music: 
        Source: (distgit) fedora
        NVR: elementary-music-7.1.0-1.fc40
        Commit/branch: 79d008f71c1f23ff49d72772f1b3d5e783762cac
    enchant: 
        Source: (distgit) fedora
        NVR: enchant-1.6.0-33.fc39
        Commit/branch: 239fbdca9cab571d8fa6627c515c2ee3d535fa3e
    envytools: 
        Source: (distgit) fedora
        NVR: envytools-0.0-0.27.git20200810.fc39
        Commit/branch: 6b71cdb36beff582d3194fa4580a2d0dcf50044e
    ergo: 
        Source: (distgit) fedora
        NVR: ergo-3.8.2-1.fc39
        Commit/branch: cc674aa9487abb6a7352047ed40f53ca85498217
    eterm: 
        Source: (distgit) fedora
        NVR: eterm-0.9.6-34.fc39
        Commit/branch: 50b56edb1c8b4e4f088d871f3e28b597cc71ad4e
    expect: 
        Source: (distgit) fedora
        NVR: expect-5.45.4-20.fc39
        Commit/branch: 07dc2106893a19c858fdf40f59878192f532953f
    f3: 
        Source: (distgit) fedora
        NVR: f3-8.0-5.fc39
        Commit/branch: b49643609c7648002cc420701c1aeb90075b4e8f
    fastlz: 
        Source: (distgit) fedora
        NVR: fastlz-0.1.0-0.20.20070619svnrev12.fc39
        Commit/branch: e9aa00b0fc1cdf225518787cde28436a27528803
    fcitx5-gtk: 
        Source: (distgit) fedora
        NVR: fcitx5-gtk-5.1.0-1.fc40
        Commit/branch: 1b90c76d01c1da6a83f08f494ae21806336ddc3b
    fflas-ffpack: 
        Source: (distgit) fedora
        NVR: fflas-ffpack-2.5.0-7.fc40
        Commit/branch: c1e5082a7983f3a3d753327193a75dd39a513f19
    ffmpeg: 
        Source: (distgit) fedora
        NVR: ffmpeg-6.0.1-2.fc40
        Commit/branch: e691a74e2bc0672608b59320de4050ae7ffde966
    filebench: 
        Source: (distgit) fedora
        NVR: filebench-1.4.9.1-21.fc39
        Commit/branch: 9e2ca4e0910151f4dab35ab32de38989cd58e432
    filezilla: 
        Source: (distgit) fedora
        NVR: filezilla-3.66.4-1.fc40
        Commit/branch: a93c6e8e8085e61395f73e7d138ecedfdaf12631
    five-or-more: 
        Source: (distgit) fedora
        NVR: five-or-more-3.32.3-4.fc39
        Commit/branch: 6724b3dedf78a6ef62557588b17c9016dbe1a36b
    flac: 
        Source: (distgit) fedora
        NVR: flac-1.4.3-2.fc39
        Commit/branch: 6001da837433c7b77cd95cb182c38123808081fe
    flexiblas: 
        Source: (distgit) fedora
        NVR: flexiblas-3.3.1-5.fc39
        Commit/branch: c06e8d825b5f63d070193e6347a06faa03bb3330
    flint: 
        Source: (distgit) fedora
        NVR: flint-2.9.0-5.fc40
        Commit/branch: 49bd34a677ac90c7812464397215856aaf5cecc9
    flow-tools: 
        Source: (distgit) fedora
        NVR: flow-tools-0.68.5.1-38.fc39
        Commit/branch: 6397f12daae746adb19e0baf5533f754cb026c3d
    fluent-bit: 
        Source: (distgit) fedora
        NVR: fluent-bit-1.9.9-5.fc40
        Commit/branch: 917f94c8db3a3a0953f74bd2aa1753d742fbe06c
    fmt-ptrn: 
        Source: (distgit) fedora
        NVR: fmt-ptrn-1.3.22-30.fc39
        Commit/branch: ad6e07a54291c35d8b59bf10f8e4e35548651c3b
    folks: 
        Source: (distgit) fedora
        NVR: folks-0.15.6-3.fc40
        Commit/branch: 46ddcfae1001e5ae2e74d07acf134b0fcec2214a
    foma: 
        Source: (distgit) fedora
        NVR: foma-0.10.0-0.7.20210601gitdfe1ccb.fc40
        Commit/branch: 828e152f290364047e0e11be299c8fe89c760ce4
    fontforge: 
        Source: (distgit) fedora
        NVR: fontforge-20230101-8.fc40
        Commit/branch: 44b91146ba81c9fb69552519a0f276d36cad18c0
    foo2zjs: 
        Source: (distgit) fedora
        NVR: foo2zjs-0.20201003-22.fc39
        Commit/branch: 8dcda043697eabeba3bf21fe69437529a081bb58
    foobillard: 
        Source: (distgit) fedora
        NVR: foobillard-3.0a-46.fc39
        Commit/branch: cad789ef73f6584413899943efb65f34f7868625
    foomatic: 
        Source: (distgit) fedora
        NVR: foomatic-4.0.13-29.fc40
        Commit/branch: ea72f42968fccc8288d1a6f6f978273027ade65f
    fotoxx: 
        Source: (distgit) fedora
        NVR: fotoxx-23.82-1.fc40
        Commit/branch: 725a6006e350f71b0aab5c3203f669a7006712f4
    four-in-a-row: 
        Source: (distgit) fedora
        NVR: four-in-a-row-3.38.1-7.fc39
        Commit/branch: 6112fc12f814bba6b620982909017f4a7cbcf020
    fox: 
        Source: (distgit) fedora
        NVR: fox-1.6.57-14.fc39
        Commit/branch: 4844bfd71f89a199ae1ebd066cd47cf186d7a364
    freecell-solver: 
        Source: (distgit) fedora
        NVR: freecell-solver-6.8.0-4.fc39
        Commit/branch: b01609e78e20444d2a18037d9e2347fe49491158
    freeipmi: 
        Source: (distgit) fedora
        NVR: freeipmi-1.6.11-1.fc40
        Commit/branch: 90829c91ccb7677e47d226d82604369677ad0647
    freeopcua: 
        Source: (distgit) fedora
        NVR: freeopcua-0-44.20220717.bd13aee.fc39
        Commit/branch: e0fa58c7b59793e657c71fd4bc5d62379870a04b
    freerdp: 
        Source: (distgit) fedora
        NVR: freerdp-2.11.2-1.fc40
        Commit/branch: 8463447beb64da0551f7770721a7b7c6abd3501b
    freewrl: 
        Source: (distgit) fedora
        NVR: freewrl-4.3.0-18.20200221gite99ab4a.fc39
        Commit/branch: 394b560aa314acdb7f9be045c5cb724a8a62bec9
    fribidi: 
        Source: (distgit) fedora
        NVR: fribidi-1.0.13-2.fc39
        Commit/branch: 5379fcc2127cdbb22c4dfd6b4e56a19aac5fccc6
    frr: 
        Source: (distgit) fedora
        NVR: frr-9.0.1-1.fc40
        Commit/branch: 30f4eb8e8efcdc43f09ef165c37536d7fe9829d4
    fsarchiver: 
        Source: (distgit) fedora
        NVR: fsarchiver-0.8.5-15.fc39
        Commit/branch: d5fc1d32eddcfb11931f0594551f6f8bbc60ed2b
    fswebcam: 
        Source: (distgit) fedora
        NVR: fswebcam-20200725-7.fc39
        Commit/branch: 1f9426f2d31f2ad158cfd4479c4bf60f6b83ea47
    functionalplus: 
        Source: (distgit) fedora
        NVR: functionalplus-0.2.22-1.fc40
        Commit/branch: ab973a0fd5e04ffdb6998b195783cf2dc71ce932
    fuse-emulator-utils: 
        Source: (distgit) fedora
        NVR: fuse-emulator-utils-1.4.3-6.fc39
        Commit/branch: 550b28fff05dbcb4468e0a5f3aae104d69b5961c
    g4music: 
        Source: (distgit) fedora
        NVR: g4music-3.4.1-1.fc40
        Commit/branch: 3b8313f5e8bcf3ae15c4b928bee55827ca541f0b
    gamemode: 
        Source: (distgit) fedora
        NVR: gamemode-1.7-3.fc39
        Commit/branch: 47109a1313128ba6b8c7164cf456e9afb7bc5126
    gammu: 
        Source: (distgit) fedora
        NVR: gammu-1.42.0-12.fc40
        Commit/branch: 6e54339ce0c1e867a961d8c6b096aabd8e5ce5b4
    ganglia: 
        Source: (distgit) fedora
        NVR: ganglia-3.7.2-42.fc39
        Commit/branch: fa7db9ef40325c6a0593cd807a72225cefbc17ec
    garmintools: 
        Source: (distgit) fedora
        NVR: garmintools-0.10-27.fc40
        Commit/branch: 2193216ca7ca815f34ac4a22c5931bd0a1196060
    gcin: 
        Source: (distgit) fedora
        NVR: gcin-2.9.0-11.fc39
        Commit/branch: 6f46964a56c2d61529fd69a9494e7509ffad368b
    gdb: 
        Source: (distgit) fedora
        NVR: gdb-14.1-1.fc40
        Commit/branch: ebdac5bb6ea5c960c4b31bbc2ac3c190ced6373f
    gdcm: 
        Source: (distgit) fedora
        NVR: gdcm-3.0.22-4.fc39
        Commit/branch: 0e766b9f0a90f7bb239a368a1e9113b5f4eed901
    gdl: 
        Source: (distgit) fedora
        NVR: gdl-1.0.4-1.fc40
        Commit/branch: 2132812c4b68455ee6e68d92dd7d69d590e7635f
    gdm: 
        Source: (distgit) fedora
        NVR: gdm-45.0.1-4.fc40
        Commit/branch: 7f72fd0529ed2744344c1b773239aebf9d7bfdc3
    gecode: 
        Source: (distgit) fedora
        NVR: gecode-6.2.0-12.fc39
        Commit/branch: 804b9012d0a97f8dbe61ef095a497967a18b7aa5
    gegl04: 
        Source: (distgit) fedora
        NVR: gegl04-0.4.46-3.fc40
        Commit/branch: bc5093222ec6464117a10e24eecf0a70bfe47022
    genders: 
        Source: (distgit) fedora
        NVR: genders-1.27.2-16.fc39
        Commit/branch: c86f4dce7772ad6c09d54d164c7162f8a91317ed
    gerbera: 
        Source: (distgit) fedora
        NVR: gerbera-1.12.1-7.fc39
        Commit/branch: ed9a541288aa80e36fa023431c2adb0a7ea7bfbe
    getdp: 
        Source: (distgit) fedora
        NVR: getdp-3.5.0-10.fc40
        Commit/branch: 9a146086dae57e3f4ad29f23a33defc8f8e803c0
    gftp: 
        Source: (distgit) fedora
        NVR: gftp-2.9.1b-5.fc39
        Commit/branch: 1306cec158e8f87c1a129ff5bd189d8fe62cf70d
    ghc-gtk: 
        Source: (distgit) fedora
        NVR: ghc-gtk-0.15.8-1.fc39
        Commit/branch: 0fe81cafdd742dc51d441a60016d3643367f64c9
    ghc9.6: 
        Source: (distgit) fedora
        NVR: ghc9.6-9.6.3-11.fc40
        Commit/branch: 7555ecc7bab01c500f4eb5b14a1ca071804d25a1
    ghostscript: 
        Source: (distgit) fedora
        NVR: ghostscript-10.02.1-4.fc40
        Commit/branch: e0ca07cfe1d0dceaa1ee4ac5715002218fc6ec89
    giac: 
        Source: (distgit) fedora
        NVR: giac-1.9.0.73-1.fc40
        Commit/branch: 4096afec3149389d0e7b40a65e202f53e29e573d
    gimp: 
        Source: (distgit) fedora
        NVR: gimp-2.10.36-2.fc40
        Commit/branch: a9e77856bb8134898cedd42948b295740246e583
    gimp-separate+: 
        Source: (distgit) fedora
        NVR: gimp-separate+-0.5.8-35.fc39
        Commit/branch: 71a3e9cd99e56f70ac908f509c01793fab549f42
    gitg: 
        Source: (distgit) fedora
        NVR: gitg-44-1.fc40
        Commit/branch: 3e56dae82b64d81606ffa4e195e662a99d135b1e
    givaro: 
        Source: (distgit) fedora
        NVR: givaro-4.2.0-1.fc40
        Commit/branch: 3fe9426ffc45f251939d6019dcde34c233e718fc
    glibc: 
        Source: (distgit) fedora
        NVR: glibc-2.38.9000-29.fc40
        Commit/branch: 259d575cdf1efb48b486811f4faadbcd836f0cbc
    glimpse: 
        Source: (distgit) fedora
        NVR: glimpse-4.12.6^git20161025.4945711-1.fc40
        Commit/branch: a7c0a56774add55caa784c2dbe6709b7e0574385
    globus-common: 
        Source: (distgit) fedora
        NVR: globus-common-18.13-5.fc39
        Commit/branch: 2780cd5b7fa937433f605862693d87e89cfd948b
    globus-gass-copy: 
        Source: (distgit) fedora
        NVR: globus-gass-copy-10.12-4.fc39
        Commit/branch: 67133f4620c024903c76e2a44bc02b0b959e5a23
    glusterfs-coreutils: 
        Source: (distgit) fedora
        NVR: glusterfs-coreutils-0.3.2-1.fc40
        Commit/branch: 043bdd0673a34634a695a131a23531108c77242a
    gmediarender: 
        Source: (distgit) fedora
        NVR: gmediarender-0.0.9-7.fc39
        Commit/branch: ee1746209ccb416de6251cf7ee7b7af87629ad13
    gmsh: 
        Source: (distgit) fedora
        NVR: gmsh-4.12.0-1.fc40
        Commit/branch: 8c2b851ce96866b5414f766f6b2ee4b80ff4dbdc
    gnokii: 
        Source: (distgit) fedora
        NVR: gnokii-0.6.31-40.fc39
        Commit/branch: 4a002d544b664c16717ae277db86087c6273682b
    gnome-2048: 
        Source: (distgit) fedora
        NVR: gnome-2048-3.38.2-8.fc39
        Commit/branch: 52177bb24adf0557d1bd5422e7ad4a4fc2a792f7
    gnome-boxes: 
        Source: (distgit) fedora
        NVR: gnome-boxes-45.0-1.fc40
        Commit/branch: 3ca4884dc05cfc06fbe8e3f7e2488fbcff8d462d
    gnome-break-timer: 
        Source: (distgit) fedora
        NVR: gnome-break-timer-2.0.3-7.fc39
        Commit/branch: 46c0f3d514087b91636d051f76c49b2cc180719c
    gnome-connections: 
        Source: (distgit) fedora
        NVR: gnome-connections-45.0-1.fc40
        Commit/branch: 8ab4ec58c3fe611a6827cea2cc3a956ac5d8214e
    gnome-contacts: 
        Source: (distgit) fedora
        NVR: gnome-contacts-45.0-1.fc40
        Commit/branch: d5dd41741f9c230ac7765211e053dff19f1feb2c
    gnome-control-center: 
        Source: (distgit) fedora
        NVR: gnome-control-center-45.2-1.fc40
        Commit/branch: 7ef504e30b6bcb8bcbaa0f28e5b93818e24f3ff0
    gnome-font-viewer: 
        Source: (distgit) fedora
        NVR: gnome-font-viewer-45.0-1.fc40
        Commit/branch: d93089592c36b7f1adbe2757e2582d8aa2202ae1
    gnome-klotski: 
        Source: (distgit) fedora
        NVR: gnome-klotski-3.38.2-8.fc39
        Commit/branch: 2ec3cebbd3521ea6149cdd6a81c9705abe31c7ee
    gnome-mahjongg: 
        Source: (distgit) fedora
        NVR: gnome-mahjongg-3.40.0-2.fc39
        Commit/branch: 23f5ff94e02d55b91d31a299b59ddb0b0840c243
    gnome-nibbles: 
        Source: (distgit) fedora
        NVR: gnome-nibbles-4.0.1-1.fc40
        Commit/branch: 38e3b5f971997efbbf07ce98ef85d3574a730241
    gnome-pomodoro: 
        Source: (distgit) fedora
        NVR: gnome-pomodoro-0.24.0-1.fc40
        Commit/branch: 2b9f7de55d4178d1457acd5d2f4509b0f2772c23
    gnome-robots: 
        Source: (distgit) fedora
        NVR: gnome-robots-40.0-7.fc39
        Commit/branch: 615c4bea5aad23b017701351f148488fbe03d770
    gnome-sudoku: 
        Source: (distgit) fedora
        NVR: gnome-sudoku-45.3-1.fc40
        Commit/branch: 5777685d1c7fab4cefc23adf049dce53f93c90e2
    gnome-taquin: 
        Source: (distgit) fedora
        NVR: gnome-taquin-3.38.1-8.fc39
        Commit/branch: 0c0e36fb3a1f8fd690e05b65db2816f2e5d046a9
    gnome-tetravex: 
        Source: (distgit) fedora
        NVR: gnome-tetravex-3.38.2-8.fc39
        Commit/branch: 00b879de36ac879ab5a7b82f61939136c0cc1403
    gnome-translate: 
        Source: (distgit) fedora
        NVR: gnome-translate-0.99-43.fc39
        Commit/branch: 4f81a46e16f1d245b7e2b1e5565e55341ac0ace5
    gnucash: 
        Source: (distgit) fedora
        NVR: gnucash-5.5-1.fc40
        Commit/branch: 1f00a1f02991cb7416bae9703f909cff822cea49
    gnumeric: 
        Source: (distgit) fedora
        NVR: gnumeric-1.12.56-2.fc40
        Commit/branch: 111df3e2ecb35485af1e5b2c5b1820e03c5c8112
    gnuradio: 
        Source: (distgit) fedora
        NVR: gnuradio-3.10.9.1-1.fc40
        Commit/branch: 39b873e65d34c6ae2719ccfaaa1880e91375d300
    gnurobots: 
        Source: (distgit) fedora
        NVR: gnurobots-1.2.0-36.fc39
        Commit/branch: 499800ac1c3090d20eafc36c7d5be800cce86af3
    gnustep-base: 
        Source: (distgit) fedora
        NVR: gnustep-base-1.29.0-1.fc40
        Commit/branch: 31da485473aff05864c5a36773e09b1f303c3a77
    goocanvas: 
        Source: (distgit) fedora
        NVR: goocanvas-1.0.0-26.fc39
        Commit/branch: bd0f8fb0d19f0a1f751a833a8b49412106de6784
    gperftools: 
        Source: (distgit) fedora
        NVR: gperftools-2.13-1.fc40
        Commit/branch: e80c435217c1c33f46b0522ab1b3c17b53e5e44b
    gq: 
        Source: (distgit) fedora
        NVR: gq-1.3.4-48.fc39
        Commit/branch: 25b83e821912801985040d40270a75067b700d50
    gqrx: 
        Source: (distgit) fedora
        NVR: gqrx-2.17.3-3.fc40
        Commit/branch: 570ba9b3c05eee1b1f7e59781728104969f86634
    granite-7: 
        Source: (distgit) fedora
        NVR: granite-7-7.4.0-1.fc40
        Commit/branch: f470bd192b7bfe2080a2f223fa8a765282232ac4
    graphviz: 
        Source: (distgit) fedora
        NVR: graphviz-9.0.0-6.fc40
        Commit/branch: 23e9084f1fd790e47b0d596bc202a7b877f94371
    grass: 
        Source: (distgit) fedora
        NVR: grass-8.3.1-3.fc40
        Commit/branch: 479fe227552c0e2759e7196b552f922ebdf37fb8
    grib_api: 
        Source: (distgit) fedora
        NVR: grib_api-1.27.0-20.fc40
        Commit/branch: cc276059c5b533e1646de8b9ed78278cb4205ca4
    groonga: 
        Source: (distgit) fedora
        NVR: groonga-13.1.0-1.fc40
        Commit/branch: 3a28c6b1eb5a3458d4139c9243e960caee94cf43
    grub2: 
        Source: (distgit) fedora
        NVR: grub2-2.06-110.fc40
        Commit/branch: cadd7a1196718a194f7e7cb650826f8c00eac1ed
    gsequencer: 
        Source: (distgit) fedora
        NVR: gsequencer-6.1.3-0.fc40
        Commit/branch: b30f6266522dfc07a2be93162c0783f4b77b3d49
    gsi-openssh: 
        Source: (distgit) fedora
        NVR: gsi-openssh-9.3p1-6.fc40
        Commit/branch: 76c16c6e5ab1f892f8ff94b99ccd957d22fb7a6e
    gsl: 
        Source: (distgit) fedora
        NVR: gsl-2.7.1-5.fc39
        Commit/branch: 71b7f187603b7988dc6522461076228f3a31b9f8
    gsoap: 
        Source: (distgit) fedora
        NVR: gsoap-2.8.124-3.fc39
        Commit/branch: 9e9e7262c3665f7ea7472f3c5eb9bcf747a28121
    gt: 
        Source: (distgit) fedora
        NVR: gt-0.4-42.fc40
        Commit/branch: d608c99ec751d11fd2cfbbf0a0ecad9b146c5a2d
    gtk-v4l: 
        Source: (distgit) fedora
        NVR: gtk-v4l-0.4-27.20220522gitd3bcbc7.fc39
        Commit/branch: e322443d7800281b6437379d8ff182d192df34aa
    gtkterm: 
        Source: (distgit) fedora
        NVR: gtkterm-1.2.1-5.fc39
        Commit/branch: c41b18dc730041a27852db433e5e673942333679
    gtkwave: 
        Source: (distgit) fedora
        NVR: gtkwave-3.3.117-1.fc40
        Commit/branch: ae7b2652cfa332a993ecb4a07adda6a842b1acc1
    guitarix: 
        Source: (distgit) fedora
        NVR: guitarix-0.44.1-7.fc39
        Commit/branch: dd2c3e545f55712363189b5b8e2bec09d90480df
    gummi: 
        Source: (distgit) fedora
        NVR: gummi-0.6.6-18.fc39
        Commit/branch: 25645585c6ed7640f6f86944b87c58f7fc269614
    gutenprint: 
        Source: (distgit) fedora
        NVR: gutenprint-5.3.4-13.fc40
        Commit/branch: e086743f8b9f64d247a06055cc65b09ee25d0a60
    h5py: 
        Source: (distgit) fedora
        NVR: h5py-3.10.0-1.fc40
        Commit/branch: 641a5148c4fec9dde1e652d4b96403f1bd402446
    harvey: 
        Source: (distgit) fedora
        NVR: harvey-1.0.2-15.fc40
        Commit/branch: 25cc305759565836795a5daaf6e88a66dc4d2b2e
    hashcat: 
        Source: (distgit) fedora
        NVR: hashcat-6.2.6-4.fc40
        Commit/branch: c98f168541345a1249d211d63703699a0b0c24ca
    hdf: 
        Source: (distgit) fedora
        NVR: hdf-4.2.15-14.fc40
        Commit/branch: e557ba533f94d39743d19b693a48288ece2e98ff
    hdparm: 
        Source: (distgit) fedora
        NVR: hdparm-9.65-2.fc39
        Commit/branch: 1292d7d7284f35703bc4899ea7c7f45cd4778d58
    heaptrack: 
        Source: (distgit) fedora
        NVR: heaptrack-1.5.0-1.fc40
        Commit/branch: ed374ab2d834a7baa0bd0404aa2a2769b289d620
    hfsutils: 
        Source: (distgit) fedora
        NVR: hfsutils-3.2.6-47.fc39
        Commit/branch: 6fb4c4b8537bfec194392bef82a8746b774dd11f
    hplip: 
        Source: (distgit) fedora
        NVR: hplip-3.23.8-1.fc40
        Commit/branch: b7a8b0b5a87a1600ca2ea8ff9ec9e5728e6a6feb
    hpx: 
        Source: (distgit) fedora
        NVR: hpx-1.9.1-2.fc40
        Commit/branch: 259f11043afff9193e4538f84781c10597a6d684
    htslib: 
        Source: (distgit) fedora
        NVR: htslib-1.15.1-3.fc39
        Commit/branch: d260a2e7d678bc02a5fa86ee665fe982e4f1bc34
    http-parser: 
        Source: (distgit) fedora
        NVR: http-parser-2.9.4-9.fc39
        Commit/branch: 3ab44c7a012f0f666fb9cb0f976cd49f1c72096f
    httperf: 
        Source: (distgit) fedora
        NVR: httperf-0.9.1-0.8.20200929git6342b16.fc39
        Commit/branch: ab81cf059daaefef1267a6e522500538a7b55f90
    hugs98: 
        Source: (distgit) fedora
        NVR: hugs98-2006.09-47.fc40
        Commit/branch: 36c5ca15c677ec74f3e5b866aa294a911a25e1e5
    hunspell: 
        Source: (distgit) fedora
        NVR: hunspell-1.7.2-5.fc39
        Commit/branch: 5675a2c4008be67ad442801eb5d436e16d382b58
    hyperestraier: 
        Source: (distgit) fedora
        NVR: hyperestraier-1.4.13-56.fc40
        Commit/branch: f27081f38ec4f383aac79de8b77336f1fc592446
    hyperhdr: 
        Source: (distgit) fedora
        NVR: hyperhdr-20.0.0.0-0.1.fc40
        Commit/branch: eacd0b85fa3ee458d34513eccc74b982ce6a61a6
    hyperrogue: 
        Source: (distgit) fedora
        NVR: hyperrogue-12.0-12.m.fc39
        Commit/branch: 93f23c78fd2b9f82cbd74b58a94ae21d8d5998f7
    hyprland: 
        Source: (distgit) fedora
        NVR: hyprland-0.32.3-2.fc40
        Commit/branch: da898e304bb4bba038e915ef01d4dd87452a4d7a
    iagno: 
        Source: (distgit) fedora
        NVR: iagno-3.38.1-8.fc39
        Commit/branch: 48c235e8474e0044804786cd657ccc967d0c899b
    iaxclient: 
        Source: (distgit) fedora
        NVR: iaxclient-2.1-0.46.beta3.fc39
        Commit/branch: 4acd764dc88b22d1671350cae4346e2c70503606
    ibus-fbterm: 
        Source: (distgit) fedora
        NVR: ibus-fbterm-1.0.2-3.fc39
        Commit/branch: acc2b0b9bd483dd3b5d61c4cdfc73df876cb1922
    ibus-handwrite: 
        Source: (distgit) fedora
        NVR: ibus-handwrite-3.0.0-22.fc39
        Commit/branch: dd030707805ebd9ffc9d90e97328a85c2702b1d6
    ibus-libpinyin: 
        Source: (distgit) fedora
        NVR: ibus-libpinyin-1.15.6-1.fc40
        Commit/branch: 2712cf891fd3406ba681663d32ec0a335271d73d
    igraph: 
        Source: (distgit) fedora
        NVR: igraph-0.10.8-1.fc40
        Commit/branch: 6f513096f87b3dd9283dd7cb834484df77dab677
    igt-gpu-tools: 
        Source: (distgit) fedora
        NVR: igt-gpu-tools-1.27.1-0.2.20230215git45da871.fc40
        Commit/branch: aee240be3210637b776107f71f3eba15afb22e0d
    ilmbase: 
        Source: (distgit) fedora
        NVR: ilmbase-2.3.0-11.fc39
        Commit/branch: 2706e204ff00a8299d4b864c25498f202f3476cd
    imake: 
        Source: (distgit) fedora
        NVR: imake-1.0.9-4.fc39
        Commit/branch: 2859a2bd85878982ff4cefb489c07a3c9766603d
    imhex: 
        Source: (distgit) fedora
        NVR: imhex-1.31.0-1.fc40
        Commit/branch: ec7a0ff896ba4a55d55696c630c165f55cf0ac5f
    inkscape: 
        Source: (distgit) fedora
        NVR: inkscape-1.3.2-2.fc40
        Commit/branch: 183945798da28b9e06d313b1e3b182042970df7f
    insight: 
        Source: (distgit) fedora
        NVR: insight-13.0.50.20220502-12.fc40
        Commit/branch: f14e3ef63dddcf0451ad36faeba78e4edf791022
    iotools: 
        Source: (distgit) fedora
        NVR: iotools-1.7~pre0-6.fc39
        Commit/branch: c70c13c3bc0aef5abbaca093fa3286192a04ba3d
    iptraf-ng: 
        Source: (distgit) fedora
        NVR: iptraf-ng-1.2.1-8.fc39
        Commit/branch: 606f926b42d54db560b6155117926914962c3c1b
    irsim: 
        Source: (distgit) fedora
        NVR: irsim-9.7.104-12.fc40
        Commit/branch: e61c397a8f5bc077f2868d2e24a3faab1101e92f
    iscsi-initiator-utils: 
        Source: (distgit) fedora
        NVR: iscsi-initiator-utils-6.2.1.9-17.gitc26218d.fc39
        Commit/branch: 8f8863ee5f7e29de88c475454af5cce6777e5412
    ispc: 
        Source: (distgit) fedora
        NVR: ispc-1.22.0-1.fc40
        Commit/branch: 82ba4f7227236c03276d4098e5a61ba9c519fd2d
    jam: 
        Source: (distgit) fedora
        NVR: jam-2.5-35.fc39
        Commit/branch: 0ddc0308537a30ff9c92752627cb38c05a9f96c7
    java-1.8.0-openjdk-portable: 
        Source: (distgit) fedora
        NVR: java-1.8.0-openjdk-portable-1.8.0.392.b08-5.fc38
        Commit/branch: ccc42bdf4ec309947f42d7c239f8144337a1ffe7
    jed: 
        Source: (distgit) fedora
        NVR: jed-0.99.19-27.fc39
        Commit/branch: f7a370d0409313b5205dd6daf6f602ddb3f74d1f
    jq: 
        Source: (distgit) fedora
        NVR: jq-1.6-17.fc39
        Commit/branch: eb71a2ecc2514b705a4692fd64966b1145d2a685
    js8call: 
        Source: (distgit) fedora
        NVR: js8call-2.2.0-19.fc39
        Commit/branch: 2a125a3ee88c389106b88defe74f7f9d3d8df99b
    kasumi: 
        Source: (distgit) fedora
        NVR: kasumi-2.5-42.fc39
        Commit/branch: f70f7648064861d23d336b3243b349baba66d60a
    kate: 
        Source: (distgit) fedora
        NVR: kate-24.01.85-1.fc40
        Commit/branch: a8c3b4b8333a29789e83250f0c7b4a034ee8e8c4
    kbd: 
        Source: (distgit) fedora
        NVR: kbd-2.6.4-1.fc40
        Commit/branch: 0c0055eae4065addff45511a060cc82e8f317551
    kdebase3: 
        Source: (distgit) fedora
        NVR: kdebase3-3.5.10-78.fc39
        Commit/branch: e5c175c131eac885b0760ac2ef08a1d20e1a9fb0
    kernelshark: 
        Source: (distgit) fedora
        NVR: kernelshark-2.2.0-4.fc39
        Commit/branch: 8158824994e5ed309e3af538ea0c83f22df2f20e
    keyutils: 
        Source: (distgit) fedora
        NVR: keyutils-1.6.3-1.fc40
        Commit/branch: 2645b1fdacb50373fcbd73f7d58e27c854eab9c1
    kf5-libkdcraw: 
        Source: (distgit) fedora
        NVR: kf5-libkdcraw-23.08.2-1.fc40
        Commit/branch: e136608191a0d46493d8795bebbf1ab5639c5ef9
    kf6-kcoreaddons: 
        Source: (distgit) fedora
        NVR: kf6-kcoreaddons-5.247.0-1.fc40
        Commit/branch: e355db2057bfc43e12615ddd92111222d2ded9ce
    kissat: 
        Source: (distgit) fedora
        NVR: kissat-3.1.1-1.fc40
        Commit/branch: a97130f4ab6e73dfb60e058e6abd5674375cd5c1
    klamav: 
        Source: (distgit) fedora
        NVR: klamav-0.46-42.fc39
        Commit/branch: d3db9ae21a43171f55298a439af63a2b8a059573
    krita: 
        Source: (distgit) fedora
        NVR: krita-5.1.5-5.fc39
        Commit/branch: d1c0a0a2af1cf92740237f3b88528825fe656492
    kstars: 
        Source: (distgit) fedora
        NVR: kstars-3.6.6-1.fc40
        Commit/branch: 5711a0b91cdaecb620dc24105a676016a78ae1e0
    lagan: 
        Source: (distgit) fedora
        NVR: lagan-2.0-39.fc39
        Commit/branch: 70aa7a11eafbcb6e4906fa1dd87561c64f9c97c0
    lame: 
        Source: (distgit) fedora
        NVR: lame-3.100-15.fc39
        Commit/branch: a39f59d750ce44316ec6b7debe23cf3d3ad41a54
    latte-integrale: 
        Source: (distgit) fedora
        NVR: latte-integrale-1.7.6-7.fc39
        Commit/branch: e80c362b0cd9d096fe1412f98f5371bb9e92b889
    lcms: 
        Source: (distgit) fedora
        NVR: lcms-1.19-37.fc39
        Commit/branch: 4e8e8c20a0e174f5d286b48d6d6dc51f4dd92398
    ldapvi: 
        Source: (distgit) fedora
        NVR: ldapvi-1.7-44.fc39
        Commit/branch: 22a71aae8525c92eb1ef0952799ae0ed9918be54
    libaccounts-glib: 
        Source: (distgit) fedora
        NVR: libaccounts-glib-1.25-13.fc39
        Commit/branch: eabdb22581fd2f0f3e0573b1b0507e0b2a05d130
    libarrow: 
        Source: (distgit) fedora
        NVR: libarrow-14.0.2-1.fc40
        Commit/branch: 5d002adb127f098c2bed6ea6cfdcf79858ba42de
    libcaca: 
        Source: (distgit) fedora
        NVR: libcaca-0.99-0.69.beta20.fc40
        Commit/branch: 8ca53b63c2d03ba64a89f049b213469927d87f9d
    libcdaudio: 
        Source: (distgit) fedora
        NVR: libcdaudio-0.99.12p2-39.fc39
        Commit/branch: ac3d415022be3a0b43855a8fc915c7c0640324eb
    libcddb: 
        Source: (distgit) fedora
        NVR: libcddb-1.3.2-39.fc39
        Commit/branch: c093115298cc780c7f0fb3cbe7853194e1acb7a9
    libcgroup: 
        Source: (distgit) fedora
        NVR: libcgroup-3.0-3.fc39
        Commit/branch: c797d70e2bc9b347f01380f28c9e161642ce3a92
    libcli: 
        Source: (distgit) fedora
        NVR: libcli-1.10.7-5.fc39
        Commit/branch: b0d63cf0d8bd929f551288236046016d08f17620
    libcomps: 
        Source: (distgit) fedora
        NVR: libcomps-0.1.20-2.fc40
        Commit/branch: 7fb7ff70fffa5ffbadb0cb48bd8390489747e56a
    libdap: 
        Source: (distgit) fedora
        NVR: libdap-3.20.10-4.fc39
        Commit/branch: a66a0a93470e484d3fb602b501e9511f878248c3
    libdatachannel: 
        Source: (distgit) fedora
        NVR: libdatachannel-0.19.3-1.fc40
        Commit/branch: 01994368f06b998ff8ad3e2c00eec9065daa2d1d
    libdb: 
        Source: (distgit) fedora
        NVR: libdb-5.3.28-58.fc40
        Commit/branch: c8d8a169a67f13d9800835acec3a051879f4cac2
    libdrm: 
        Source: (distgit) fedora
        NVR: libdrm-2.4.117-1.fc40
        Commit/branch: 0bc13e3b32f5fa9165aa4e751a03b71991add30a
    libffado: 
        Source: (distgit) fedora
        NVR: libffado-2.4.7-5.fc40
        Commit/branch: ca7b59712d430c3ef2d246c278229e91028fd752
    libfishsound: 
        Source: (distgit) fedora
        NVR: libfishsound-1.0.0-27.fc39
        Commit/branch: 2d3a53acb766984921401f8c94d4454a845bf6de
    libfm: 
        Source: (distgit) fedora
        NVR: libfm-1.3.2-4.fc40
        Commit/branch: 24413512a769e90b39e042cd8b42cdc25e017096
    libfplll: 
        Source: (distgit) fedora
        NVR: libfplll-5.4.5-1.fc40
        Commit/branch: 1f2db3443f5b6df11252cdcf4e2a2875511e83dd
    libfreenect: 
        Source: (distgit) fedora
        NVR: libfreenect-0.7.0-4.fc39
        Commit/branch: 1ef45bc200d380be0c8ebe1e3f0d1a4cf1eaaa1b
    libgda5: 
        Source: (distgit) fedora
        NVR: libgda5-5.2.10-18.fc40
        Commit/branch: 904cff1701ed146260865ce655939e6c2d12578d
    libgenht: 
        Source: (distgit) fedora
        NVR: libgenht-1.1.3-3.fc39
        Commit/branch: 3c82d665a1d12ed25fc7df15cc9849dc99bacf9d
    libgle: 
        Source: (distgit) fedora
        NVR: libgle-3.1.0-29.fc39
        Commit/branch: ac22bd41f04cbd96fea7ecd254d2ba1285370669
    libgnome-games-support: 
        Source: (distgit) fedora
        NVR: libgnome-games-support-2.0.0-2.fc39
        Commit/branch: e58f8e3f3c52f6e40fc36640a4c5875dfbb2f777
    libgnome-games-support1: 
        Source: (distgit) fedora
        NVR: libgnome-games-support1-1.8.2-2.fc39
        Commit/branch: f19bce9c58aebf2ad5c912ded8d8f14aa1fb549b
    libguess: 
        Source: (distgit) fedora
        NVR: libguess-1.2-19.fc39
        Commit/branch: 4815f9e6606749c5309e6e5128ac3949291914eb
    libguestfs: 
        Source: (distgit) fedora
        NVR: libguestfs-1.51.10-1.fc40
        Commit/branch: eb5bfa36cc7b6c1d46ed21dea03f376a5257b78c
    libijs: 
        Source: (distgit) fedora
        NVR: libijs-0.35-19.fc39
        Commit/branch: 8f4de1533660b7eb778786adbc62a938093bbd58
    libkdumpfile: 
        Source: (distgit) fedora
        NVR: libkdumpfile-0.5.4-1.fc40
        Commit/branch: 7a25815c6f1d482e6f8aff18104db6c4f015c2e0
    libkkc: 
        Source: (distgit) fedora
        NVR: libkkc-0.3.5-26.fc40
        Commit/branch: a80d936ce4322e86b19f84fe2da171cd42522fff
    libliftoff: 
        Source: (distgit) fedora
        NVR: libliftoff-0.4.1-1.fc39
        Commit/branch: c9ffbbae185c07bd40b5c7501e5463330b8fe094
    libmemcached-awesome: 
        Source: (distgit) fedora
        NVR: libmemcached-awesome-1.1.4-2.fc39
        Commit/branch: 921e68627dd8e5af2102b605c0d300c79c937e6e
    libminc: 
        Source: (distgit) fedora
        NVR: libminc-2.4.03-16.fc39
        Commit/branch: e7a6d56638b874a6630b7ca3932e9fcf61f8df96
    libmongocrypt: 
        Source: (distgit) fedora
        NVR: libmongocrypt-1.8.3-1.fc40
        Commit/branch: 6c83306ae73d49f1da94f463fe3bc5cd222891fb
    libmpcdec: 
        Source: (distgit) fedora
        NVR: libmpcdec-1.3.0-0.2.20110810svn475.fc39
        Commit/branch: 2d668b003d5969c2efde370b837297cf54dc938a
    libmpd: 
        Source: (distgit) fedora
        NVR: libmpd-11.8.17-22.fc39
        Commit/branch: 4537d5fe380ad6d67bef1eec97f371f20751f617
    libomemo-c: 
        Source: (distgit) fedora
        NVR: libomemo-c-0.5.0-3.fc39
        Commit/branch: 4b5e3f7f0e634b5ff473c76b8027d5fc5ee10d4b
    liborcus: 
        Source: (distgit) fedora
        NVR: liborcus-0.18.1-2.fc40
        Commit/branch: d9995d7b986df2b7aa6d0d11274b28ae65bbd84a
    libpasastro: 
        Source: (distgit) fedora
        NVR: libpasastro-1.4.2-1.fc40
        Commit/branch: 4a748b6e438052f3a7dc444d4c8a0c8af9704404
    libpng: 
        Source: (distgit) fedora
        NVR: libpng-1.6.40-1.fc40
        Commit/branch: 7fb15c2e899d0cfd6de5564e7853f19cc8a15e78
    libprelude: 
        Source: (distgit) fedora
        NVR: libprelude-5.2.0-21.fc39
        Commit/branch: 6107143388410f04b07f1da0aaeb47d391a58d96
    libsecret: 
        Source: (distgit) fedora
        NVR: libsecret-0.21.2-2.fc40
        Commit/branch: a858664cc30cb9103a9818cdb97a20879ab8cfb5
    libsemigroups: 
        Source: (distgit) fedora
        NVR: libsemigroups-2.7.2-1.fc40
        Commit/branch: bdcb4a24c1a7d08d54476252bdfd0a40de0920f6
    libsigrok: 
        Source: (distgit) fedora
        NVR: libsigrok-0.5.2-10.fc39
        Commit/branch: 5fae540bbb64ba8a53ecfcab835cf42d0c709c72
    libsndfile: 
        Source: (distgit) fedora
        NVR: libsndfile-1.2.2-1.fc40
        Commit/branch: 5a59c1499e2fb6657ee45ade3034c5896eddceeb
    libssh: 
        Source: (distgit) fedora
        NVR: libssh-0.10.6-2.fc40
        Commit/branch: ef6352ea97c4f2fea292de66f137f46ed0123b20
    libssh2: 
        Source: (distgit) fedora
        NVR: libssh2-1.11.0-2.fc39
        Commit/branch: afe9bca43893ad0611030168609c82c2eb0d0f92
    libstorj: 
        Source: (distgit) fedora
        NVR: libstorj-1.0.3-15.fc39
        Commit/branch: e577b849d221e91a6b03e75c08ca270b2acd38ba
    libtrash: 
        Source: (distgit) fedora
        NVR: libtrash-3.7-4.fc39
        Commit/branch: 3d88d8aa46b9be00c3e04b02ffedae71bcd78286
    libunifex: 
        Source: (distgit) fedora
        NVR: libunifex-0.4.0-1.fc40
        Commit/branch: 3499b983c761b0f164f73742cf5437d9342046ae
    libunwind: 
        Source: (distgit) fedora
        NVR: libunwind-1.7.2-1.fc40
        Commit/branch: a6ef66ee2f7bc100857e14ca3cc81060ea8987fc
    libxmp: 
        Source: (distgit) fedora
        NVR: libxmp-4.6.0-2.fc39
        Commit/branch: 2997d3e52ffea2916aadbe8ee85969f4ebf7620a
    linbox: 
        Source: (distgit) fedora
        NVR: linbox-1.7.0-2.fc40
        Commit/branch: 8b2f55a2ab3a17c9efd3ed2b4fdeb258655e1c34
    linhpsdr: 
        Source: (distgit) fedora
        NVR: linhpsdr-0-0.7.20210710git742658a9.fc40
        Commit/branch: 3e0831c90d191e47b8d01d07d73e18b409fcabb8
    linsmith: 
        Source: (distgit) fedora
        NVR: linsmith-0.99.33-5.fc40
        Commit/branch: 9428a6cf53475502c6243bafe68788c73c883747
    lizardfs: 
        Source: (distgit) fedora
        NVR: lizardfs-3.12.0-23.fc39
        Commit/branch: 08b453a2415b89c7e4f8da983510d062c12885b6
    logiops: 
        Source: (distgit) fedora
        NVR: logiops-0.3.3-1.fc39
        Commit/branch: 66f9a05e6d26c9e5849c08016acc599770e87fcb
    loudgain: 
        Source: (distgit) fedora
        NVR: loudgain-0.6.8-15.fc39
        Commit/branch: ca2dd856ee2942fe7d380521396828242e14d7e4
    lpcnetfreedv: 
        Source: (distgit) fedora
        NVR: lpcnetfreedv-0.5-3.fc40
        Commit/branch: 5be8634392e5751b47e1746c9dc190050f3d5cd0
    lrslib: 
        Source: (distgit) fedora
        NVR: lrslib-7.2-4.fc39
        Commit/branch: 5e07fbe99d2eb6c698437175f09b91645e8814d5
    lterm: 
        Source: (distgit) fedora
        NVR: lterm-1.5.1-14.fc39
        Commit/branch: add4e8655f6a2a4a58c00cee041d6bd8a55dbe06
    lttng-tools: 
        Source: (distgit) fedora
        NVR: lttng-tools-2.13.11-2.fc40
        Commit/branch: 4d0700a830b233e4dac57069dbd6744900d7e20f
    lua-dbi: 
        Source: (distgit) fedora
        NVR: lua-dbi-0.7.2-11.fc39
        Commit/branch: a3fb8f8dd0f9efdc6cfab54529533ce21fc5aab5
    lxpanel: 
        Source: (distgit) fedora
        NVR: lxpanel-0.10.1^20230918git633a2d46-1.fc40
        Commit/branch: c78bfaadef507919d89938636545a8aff82985db
    lxterminal: 
        Source: (distgit) fedora
        NVR: lxterminal-0.4.0^20230917git9b4299c2-1.fc40
        Commit/branch: de70bb16c88a2284093144d6ba6945209e1975f0
    magic: 
        Source: (distgit) fedora
        NVR: magic-8.3.454-1.fc40
        Commit/branch: 92bedb41baf28297b9b6d6f04edeb1102fb54fff
    maloc: 
        Source: (distgit) fedora
        NVR: maloc-1.5-28.fc39
        Commit/branch: ae1123dd81546fb63be329a89e6809a87409a1fd
    mame: 
        Source: (distgit) fedora
        NVR: mame-0.261-1.fc40
        Commit/branch: ed957fa7bef509ada6fe60c44a601475d7348437
    mapserver: 
        Source: (distgit) fedora
        NVR: mapserver-8.0.1-9.fc40
        Commit/branch: e4691be9fd071d880107f6c0b3b56d4ca80618a5
    mariadb: 
        Source: (distgit) fedora
        NVR: mariadb-10.5.23-1.fc40
        Commit/branch: 9fe7f4029b9e93fe32b43b43e1ed0e5521124c91
    marker: 
        Source: (distgit) fedora
        NVR: marker-0.0.2023.05.02-2.fc39
        Commit/branch: 7c747871317d4443a63587f6cf3ee49e616903cc
    massdns: 
        Source: (distgit) fedora
        NVR: massdns-0.3-8.fc39
        Commit/branch: f0981eb10a149e07c2d21ddcaaa82e04832d7957
    mate-settings-daemon: 
        Source: (distgit) fedora
        NVR: mate-settings-daemon-1.26.1-2.fc39
        Commit/branch: 34c8a8b4be4e25963f843d95562846abcb7c79c1
    mbedtls: 
        Source: (distgit) fedora
        NVR: mbedtls-2.28.5-1.fc40
        Commit/branch: 5e8fef2390f2fdd684d3ee36ef89e062a791ce3d
    mcpp: 
        Source: (distgit) fedora
        NVR: mcpp-2.7.2-34.fc39
        Commit/branch: 3d4e1a9e10efc2a285db71b7bc45ae3cbdbce7d9
    megaglest: 
        Source: (distgit) fedora
        NVR: megaglest-3.13.0-19.fc39
        Commit/branch: 6cc3618604baaa120bca3864b5f0a33dbb4fface
    mepack: 
        Source: (distgit) fedora
        NVR: mepack-1.1.0-1.fc40
        Commit/branch: 92a17c77232263d296edf011b0dbfb9a5cffc95d
    mesa: 
        Source: (distgit) fedora
        NVR: mesa-23.3.1-4.fc40
        Commit/branch: 49240cd70e9c1be161f7294c82a93fe1f66e2961
    mesa-demos: 
        Source: (distgit) fedora
        NVR: mesa-demos-9.0.0-4.fc40
        Commit/branch: 07845ceb992cd32cbb56a3894ce3d4461f13faf6
    metis: 
        Source: (distgit) fedora
        NVR: metis-5.1.0.3-3.fc40
        Commit/branch: e0dc620b109e07301a1c6077ba5390e85091ba84
    mfem: 
        Source: (distgit) fedora
        NVR: mfem-4.6-2.fc40
        Commit/branch: 6ab79e86455fc3e398afa0c03dab44276f357d64
    mhash: 
        Source: (distgit) fedora
        NVR: mhash-0.9.9.9-29.fc39
        Commit/branch: 6b186a517207e4902aab040c53d813e864689348
    milter-greylist: 
        Source: (distgit) fedora
        NVR: milter-greylist-4.6.4-9.fc39
        Commit/branch: 56aba1161ee8407b439f15976ec2ee1a0348c403
    minetest: 
        Source: (distgit) fedora
        NVR: minetest-5.8.0-2.fc40
        Commit/branch: 8b4f9c65fff73d9655a427a87b954cd22f3ef783
    mingw-boost: 
        Source: (distgit) fedora
        NVR: mingw-boost-1.78.0-6.fc39
        Commit/branch: d00236fd10759e1e139810628c6c5e244121ae78
    minipro: 
        Source: (distgit) fedora
        NVR: minipro-0.6-3.fc39
        Commit/branch: 127d7de917f7d972f7186ee8376822244fe349dc
    minisat2: 
        Source: (distgit) fedora
        NVR: minisat2-2.2.1-17.fc39
        Commit/branch: a1d2d8d84a2796c173bf8940d8fca7da611672c2
    mir: 
        Source: (distgit) fedora
        NVR: mir-2.15.0-2.fc40
        Commit/branch: 5d305605c0a40d5679e29fa1b9846eda067685c8
    mkvtoolnix: 
        Source: (distgit) fedora
        NVR: mkvtoolnix-80.0-1.fc40
        Commit/branch: e0df2a31b529ce10878f7d738b68db77e758b6b6
    mmg: 
        Source: (distgit) fedora
        NVR: mmg-5.7.2-1.fc40
        Commit/branch: 44506099d6590e6a7e91bc3cdbc3411f74736269
    mmlib: 
        Source: (distgit) fedora
        NVR: mmlib-1.4.2-10.fc40
        Commit/branch: 98dc406384ace87d0f675fffd67f05f5289051cc
    mod_qos: 
        Source: (distgit) fedora
        NVR: mod_qos-11.74-2.fc40
        Commit/branch: efa4d7b2bd03061c5c03104d5b8533cf6adcbe8e
    mod_suphp: 
        Source: (distgit) fedora
        NVR: mod_suphp-0.7.2-21.fc39
        Commit/branch: 943f18cb2047e25fc3cc907771c415a15510b01a
    mono: 
        Source: (distgit) fedora
        NVR: mono-6.12.0-14.fc40
        Commit/branch: e733e87b0ce49fad204d9ca6aa1e4525fff9420a
    moolticute: 
        Source: (distgit) fedora
        NVR: moolticute-1.03.0-1.fc40
        Commit/branch: 84f82ae8f7e8c4109f071c0655ff7e374694ea79
    mopac: 
        Source: (distgit) fedora
        NVR: mopac-22.1.0-1.fc40
        Commit/branch: b9933ca79c91b2684da804a5968917db9595ed81
    mozjs102: 
        Source: (distgit) fedora
        NVR: mozjs102-102.15.1-1.fc40
        Commit/branch: d7fe251f060db8d55a5a1379768ff10285a78cb6
    mozjs115: 
        Source: (distgit) fedora
        NVR: mozjs115-115.6.0-1.fc40
        Commit/branch: 03b2fe75af94dc8edf1f5d4c4c48aaec9932e3e9
    mozjs91: 
        Source: (distgit) fedora
        NVR: mozjs91-91.13.0-5.fc39
        Commit/branch: 5ca42ab7d2a6893182e877233f922b573418f388
    msitools: 
        Source: (distgit) fedora
        NVR: msitools-0.103-1.fc40
        Commit/branch: a52ad0a91bdf41f6ed339e51ef37950eca94eb5c
    mtd-utils: 
        Source: (distgit) fedora
        NVR: mtd-utils-2.1.6-1.fc40
        Commit/branch: addc86ba3f37cd0f1e40f7775e97a22faef263f4
    muffin: 
        Source: (distgit) fedora
        NVR: muffin-6.0.1-1.fc40
        Commit/branch: cf7e70a66d29e7f664e02216de7ce72edb0de398
    mupdf: 
        Source: (distgit) fedora
        NVR: mupdf-1.23.7-3.fc40
        Commit/branch: effcc4c06a13df8a78be5e34e04f4fc7fe19c11b
    mupen64plus: 
        Source: (distgit) fedora
        NVR: mupen64plus-2.5-19.fc39
        Commit/branch: 61e583ab536a56b5b3a7e67844114f6502c19270
    musescore: 
        Source: (distgit) fedora
        NVR: musescore-4.2.0-5.fc40
        Commit/branch: 9d49cf9bfbb5ffa3d7825a6889e5f27733d3afc4
    myproxy: 
        Source: (distgit) fedora
        NVR: myproxy-6.2.14-5.fc39
        Commit/branch: f12bed94c697571136b5d20d62f8085bb19e0bb2
    mysql-connector-odbc: 
        Source: (distgit) fedora
        NVR: mysql-connector-odbc-8.0.35-1.fc40
        Commit/branch: e7f5653048cb8798a1549b2c0de1e7476e066059
    nas: 
        Source: (distgit) fedora
        NVR: nas-1.9.5-6.fc39
        Commit/branch: 8d59fae9b5b934a1b3e74ce376643c3c00cfad10
    nauty: 
        Source: (distgit) fedora
        NVR: nauty-2.8.8-1.fc40
        Commit/branch: da392decd8187b79f7d9e766db52f03a648d0e97
    nbd: 
        Source: (distgit) fedora
        NVR: nbd-3.25-2.fc39
        Commit/branch: 906280a6e5c296bcad2d3b17e0d8f65bca3c5b40
    ncl: 
        Source: (distgit) fedora
        NVR: ncl-6.6.2-39.fc40
        Commit/branch: 68f7e2f40fb4d00901e508d05ee3b86fe008801b
    nco: 
        Source: (distgit) fedora
        NVR: nco-5.1.9-1.fc40
        Commit/branch: 6588858d44cbe7f4affd571d1e1bf3f26b31ce5e
    neochat: 
        Source: (distgit) fedora
        NVR: neochat-24.01.85-1.fc40
        Commit/branch: f378e9c00a1abfc45b7ab5bdb433c88aa1e629ae
    net-snmp: 
        Source: (distgit) fedora
        NVR: net-snmp-5.9.4-1.fc40
        Commit/branch: a1af22f2061d9dff4c1c667c51f6aecd6fec0450
    nethack-vultures: 
        Source: (distgit) fedora
        NVR: nethack-vultures-2.1.2-36.fc39
        Commit/branch: 6952fe828794dfebbbbef1a0f1d095a446be9428
    netpbm: 
        Source: (distgit) fedora
        NVR: netpbm-11.02.00-3.fc40
        Commit/branch: 4a35bcd0658dc308f90c6d119142255c015fea8f
    nextpnr: 
        Source: (distgit) fedora
        NVR: nextpnr-1-31.20231218git535709a.fc40
        Commit/branch: 669aed83ba52b856cd38e9847a445bd340e99aa3
    nfs-ganesha: 
        Source: (distgit) fedora
        NVR: nfs-ganesha-5.7-3.fc40
        Commit/branch: 1b951140cd35db04825ce1e1203ecd48bc50f2c3
    ngspice: 
        Source: (distgit) fedora
        NVR: ngspice-42-1.fc40
        Commit/branch: 508a2485266124b7b7b9612a22bf70452f297503
    ninja-build: 
        Source: (distgit) fedora
        NVR: ninja-build-1.11.1-5.fc40
        Commit/branch: 34bfaeb03e526a65f66d35276ee6def43e6ef489
    nodejs18: 
        Source: (distgit) fedora
        NVR: nodejs18-18.19.0-1.fc40
        Commit/branch: c72aa25eda84a8a62aca5e21b02690955c29f88b
    nodejs20: 
        Source: (distgit) fedora
        NVR: nodejs20-20.10.0-3.fc40
        Commit/branch: f523665c4dfdff88d67801573217dedb19a297f0
    nogravity: 
        Source: (distgit) fedora
        NVR: nogravity-2.00-42.fc39
        Commit/branch: 3e3c44f5cb987ce8698668574a7025f829d5f926
    nomacs: 
        Source: (distgit) fedora
        NVR: nomacs-3.16-17.fc39
        Commit/branch: affe4510f32bd41573c7479e0ae08f3e8bd69827
    notejot: 
        Source: (distgit) fedora
        NVR: notejot-3.5.1-16.fc40
        Commit/branch: b16236cdcd30be76ec2f238146869b153ac24685
    notes-up: 
        Source: (distgit) fedora
        NVR: notes-up-2.0.6-5.fc39
        Commit/branch: f58df6b373a69d9996d44a855c1fcccae784c352
    nrg2iso: 
        Source: (distgit) fedora
        NVR: nrg2iso-0.4-31.fc39
        Commit/branch: 2fb86a24064d9ca25c1937c2d3ba4c69f7359f43
    nsd: 
        Source: (distgit) fedora
        NVR: nsd-4.8.0-1.fc40
        Commit/branch: ab25a40ace8543b0c659f17dba0d356b595742e1
    ntfs2btrfs: 
        Source: (distgit) fedora
        NVR: ntfs2btrfs-20230501-3.fc39
        Commit/branch: 448e054da93f3c1f5799a8c47a580d7521cb4709
    ntpsec: 
        Source: (distgit) fedora
        NVR: ntpsec-1.2.3-1.fc40
        Commit/branch: 0b75bc26dfdebe99d6bf526f26894f4237d28dc9
    nx-libs: 
        Source: (distgit) fedora
        NVR: nx-libs-3.5.99.27-1.fc40
        Commit/branch: 97e1408e9434ac23f870755c8388d214aa381237
    obs-cef: 
        Source: (distgit) fedora
        NVR: obs-cef-5060^cr103.0.5060.134~git20231010.17f8588-2.fc40
        Commit/branch: f8e74c5ddd00979686f2d47143223c27887f189b
    octave: 
        Source: (distgit) fedora
        NVR: octave-8.4.0-2.fc40
        Commit/branch: 516b2c36458ee6ac4f1cffc067f7d3956014f75d
    octave-iso2mesh: 
        Source: (distgit) fedora
        NVR: octave-iso2mesh-1.9.6-13.fc39
        Commit/branch: b1605a4c95474816c14367eda436a57ab9ed0f89
    ogdi: 
        Source: (distgit) fedora
        NVR: ogdi-4.1.0-11.fc39
        Commit/branch: 26c66cfc9b47795b16f7b329c79cb6378fe5bad3
    onnxruntime: 
        Source: (distgit) fedora
        NVR: onnxruntime-1.15.1-5.fc40
        Commit/branch: 731d1aadeaca77ff4364f2c0f6d74b0ed23bc398
    open-vm-tools: 
        Source: (distgit) fedora
        NVR: open-vm-tools-12.3.5-1.fc40
        Commit/branch: c534c413cea4bd2b79fd62d2920e83d535de966c
    openal-soft: 
        Source: (distgit) fedora
        NVR: openal-soft-1.23.1-3.fc40
        Commit/branch: c9900052e05de6befe90080e0dfc879062f003f7
    openbabel: 
        Source: (distgit) fedora
        NVR: openbabel-3.1.1-21.fc39
        Commit/branch: 4b6fc05b0f62696d46b08aa116382d6cc6d5f21d
    openblas: 
        Source: (distgit) fedora
        NVR: openblas-0.3.25-1.fc40
        Commit/branch: 54c877216eb3c8c9bf139c8329dda72e9a973147
    opencc: 
        Source: (distgit) fedora
        NVR: opencc-1.1.4-4.fc39
        Commit/branch: 9b28485afbf867299f316ee38716ffd497e45435
    opencv: 
        Source: (distgit) fedora
        NVR: opencv-4.8.1-3.fc40
        Commit/branch: 98cbcd26f408a460098eac516ca251d6b69b4925
    opendbx: 
        Source: (distgit) fedora
        NVR: opendbx-1.4.6-34.fc39
        Commit/branch: 54b2741a9be70cd8ed463f8133c4a18e3fffa871
    openfst: 
        Source: (distgit) fedora
        NVR: openfst-1.8.2-5.fc39
        Commit/branch: 543338657bdbeb291456973e6f738adf45ef8431
    opengrm-ngram: 
        Source: (distgit) fedora
        NVR: opengrm-ngram-1.3.14-6.fc39
        Commit/branch: 90c3ef4c3fa017fc3c3904354ee6d75fcfd6652e
    openhpi: 
        Source: (distgit) fedora
        NVR: openhpi-3.8.0-24.fc39
        Commit/branch: c8524762e0f6a1dcba3b8551da30e1ee9493d50d
    openjfx: 
        Source: (distgit) fedora
        NVR: openjfx-17.0.0.1-6.fc38
        Commit/branch: ee9529cb346c772e855dc3fe20282c490fa8b162
    openjpeg: 
        Source: (distgit) fedora
        NVR: openjpeg-1.5.1-35.fc39
        Commit/branch: 9cb67f5f173828097dd71cebda4f5b503fbe56a3
    openldap: 
        Source: (distgit) fedora
        NVR: openldap-2.6.6-1.fc39
        Commit/branch: d0559e0c05c9ff027aa6cef536b3d6ae3246a21d
    openmpi: 
        Source: (distgit) fedora
        NVR: openmpi-5.0.1-1.fc40
        Commit/branch: c510a02ce6bba0f0ce3493638113004b38ac3993
    openriichi: 
        Source: (distgit) fedora
        NVR: openriichi-0.2.1.1-3.fc39
        Commit/branch: f1287d9fb2d067731f049280683dfa98903c8d0f
    opensc: 
        Source: (distgit) fedora
        NVR: opensc-0.24.0-1.fc40
        Commit/branch: 11e1462e9e7455e7b04a477cfd9f2ef1c314c8c7
    openscap: 
        Source: (distgit) fedora
        NVR: openscap-1.3.9-3.fc40
        Commit/branch: d2b7872b70b689fb2cf7f9803d33d75ca9a6752d
    openslide: 
        Source: (distgit) fedora
        NVR: openslide-4.0.0-1.fc40
        Commit/branch: 9f7973cbfee80078561c412c690db594da6bdd6a
    openssh: 
        Source: (distgit) fedora
        NVR: openssh-9.3p1-13.fc40.1
        Commit/branch: 5c1da775a9bc6269f469d5b079d9e75a311a931c
    openvswitch: 
        Source: (distgit) fedora
        NVR: openvswitch-3.2.1-1.fc40
        Commit/branch: 59e7a54dd0cd2d581c9da856f53d4f95c903fe04
    opus: 
        Source: (distgit) fedora
        NVR: opus-1.4-1.fc40
        Commit/branch: c3ad3d6ec0d7c966dfd5a27a4fc077af93e18d16
    osgearth: 
        Source: (distgit) fedora
        NVR: osgearth-3.5-2.fc40
        Commit/branch: d48bdd0fb10f4e216cd74ad19e29ae7dc981feb7
    osmctools: 
        Source: (distgit) fedora
        NVR: osmctools-0.9-12.fc39
        Commit/branch: 36ca5faa0aa3bc6081574bcc857871a2a6cfe34f
    ovn: 
        Source: (distgit) fedora
        NVR: ovn-23.09.0-91.fc40
        Commit/branch: dcf38edabd873fd7cb708d815678ce9f0761895f
    paho-c: 
        Source: (distgit) fedora
        NVR: paho-c-1.3.13-2.fc40
        Commit/branch: 24d09595a4d174d0f4184eef02a3a3a0c53b49e1
    palp: 
        Source: (distgit) fedora
        NVR: palp-2.11-7.fc39
        Commit/branch: 321df05f365829a9f2da3bde6704a410aeae02ec
    paps: 
        Source: (distgit) fedora
        NVR: paps-0.8.0-5.fc40
        Commit/branch: 740450aa394ea2dc4f2944cf5fdbb97d82018b6e
    paraview: 
        Source: (distgit) fedora
        NVR: paraview-5.11.2-5.fc40
        Commit/branch: ff281d31b69fb3c42744017ecaffacf9f6eefbe6
    passt: 
        Source: (distgit) fedora
        NVR: passt-0^20231230.gf091893-1.fc40
        Commit/branch: 7e522e779c78d5166afc7df52dcfb0e475092f2c
    pcb-rnd: 
        Source: (distgit) fedora
        NVR: pcb-rnd-2.4.0-8.fc39
        Commit/branch: c71d3ccc8bee2801fc6469bc00f796a804132b11
    pcem: 
        Source: (distgit) fedora
        NVR: pcem-17-6.fc39
        Commit/branch: 04a98902409f39c9dc49828317aa08e49c37c9bb
    pcmanfm: 
        Source: (distgit) fedora
        NVR: pcmanfm-1.3.2^20230917gite6b422b2-1.fc40
        Commit/branch: c8046fb638bf165a8d39a9bc79551db0a6424b0b
    pcp: 
        Source: (distgit) fedora
        NVR: pcp-6.1.1-1.fc40
        Commit/branch: fc70568e935f8f4c88937cbe8fc17db9995ae7d5
    perl-CGI-SpeedyCGI: 
        Source: (distgit) fedora
        NVR: perl-CGI-SpeedyCGI-2.22-52.fc39
        Commit/branch: 6d7d98846a899ba416cbb8e993bbe5f994f550cc
    perl-Crypt-U2F-Server: 
        Source: (distgit) fedora
        NVR: perl-Crypt-U2F-Server-0.45-2.fc40
        Commit/branch: a43e0d9f3a42979972807b9cddb3a40ea50f0120
    perl-DBD-Firebird: 
        Source: (distgit) fedora
        NVR: perl-DBD-Firebird-1.34-8.fc39
        Commit/branch: 798b1287cee18f5f1f8a5feacb9dc9b27e83b100
    perl-PDF-Haru: 
        Source: (distgit) fedora
        NVR: perl-PDF-Haru-1.00-42.fc39
        Commit/branch: 489d4c4864b205525adc72f2a08558f67037a822
    perl-Tk: 
        Source: (distgit) fedora
        NVR: perl-Tk-804.036-12.fc39
        Commit/branch: de89145e479da45444ea673c4b578b46d233b388
    perl-XML-Bare: 
        Source: (distgit) fedora
        NVR: perl-XML-Bare-0.53-34.fc39
        Commit/branch: cf70d5adf7aa1e1dabc54ebcfc14400b8218bf60
    perl-re-engine-PCRE: 
        Source: (distgit) fedora
        NVR: perl-re-engine-PCRE-0.17-37.fc40
        Commit/branch: 3d7f828afe2c9520c789a5b701bcb7b44a3e984a
    pesign: 
        Source: (distgit) fedora
        NVR: pesign-116-2.fc39
        Commit/branch: 04f02e8cd7210749cf49f1a6ab12f9daf60b8f13
    petsc: 
        Source: (distgit) fedora
        NVR: petsc-3.20.2-4.fc40
        Commit/branch: 8c5aed103585622930c8b2287ce46b03c7a3a489
    pgp-tools: 
        Source: (distgit) fedora
        NVR: pgp-tools-2.10-10.fc39
        Commit/branch: caf0fe56330b5f1a0fcc09c81ae49a44637bd721
    phasex: 
        Source: (distgit) fedora
        NVR: phasex-0.14.97-9.20150304git4942467.fc39.2
        Commit/branch: 675027f5f916873f6d11ea50fa9f79c3bcdca8b0
    phosh: 
        Source: (distgit) fedora
        NVR: phosh-0.32.0-1.fc40
        Commit/branch: a12825eca2a73b4601cd23b1add738d0e656960e
    photoqt: 
        Source: (distgit) fedora
        NVR: photoqt-3.4-1.fc40
        Commit/branch: f8162494f64ba1bb6993df68ac49983682b4603d
    php: 
        Source: (distgit) fedora
        NVR: php-8.3.1-1.fc40
        Commit/branch: 748a1cfd8534e4077edf0295ce554749a0cbd47d
    php-ast: 
        Source: (distgit) fedora
        NVR: php-ast-1.1.1-1.fc40
        Commit/branch: 331318eec97f7d3465e57ecb5cc1960fbf195e3f
    php-maxminddb: 
        Source: (distgit) fedora
        NVR: php-maxminddb-1.11.1-1.fc40
        Commit/branch: 84f30ac9560cba7698fcf4646c38d59533cf855e
    php-pecl-amqp: 
        Source: (distgit) fedora
        NVR: php-pecl-amqp-2.1.1-1.fc40
        Commit/branch: e3cbfa4641d71616dc0711cbb72fd811e047a6fc
    php-pecl-apcu: 
        Source: (distgit) fedora
        NVR: php-pecl-apcu-5.1.23-1.fc40
        Commit/branch: 936dd387521c122646f615bfcad473ff33c4bb39
    php-pecl-dio: 
        Source: (distgit) fedora
        NVR: php-pecl-dio-0.2.1-6.fc40
        Commit/branch: 1d2402ca35b15982d22e8a41c1414bec1aebe5f0
    php-pecl-ds: 
        Source: (distgit) fedora
        NVR: php-pecl-ds-1.5.0-1.fc40
        Commit/branch: 591d74b4794685069586f327432a90e72b2b7163
    php-pecl-event: 
        Source: (distgit) fedora
        NVR: php-pecl-event-3.1.1-1.fc40
        Commit/branch: 49b9fe14b0039cd6c75ea7128a958b2a8e530afc
    php-pecl-fann: 
        Source: (distgit) fedora
        NVR: php-pecl-fann-1.2.0~RC2-5.fc40
        Commit/branch: 4c85590eb0af7349e3a51b6598da73addc556c0b
    php-pecl-gearman: 
        Source: (distgit) fedora
        NVR: php-pecl-gearman-2.1.0-12.fc40
        Commit/branch: 2315f2cfa6745e4cf787f22efaa26fc189f6b1bd
    php-pecl-geoip: 
        Source: (distgit) fedora
        NVR: php-pecl-geoip-1.1.1-24.fc40
        Commit/branch: 63b48c9de7353285c7e0b1e19ee301bc96df6b8d
    php-pecl-gmagick: 
        Source: (distgit) fedora
        NVR: php-pecl-gmagick-2.0.6~RC1-10.fc40
        Commit/branch: 5fd78c3c601e6510bd14c8ca100334f8594b192c
    php-pecl-http: 
        Source: (distgit) fedora
        NVR: php-pecl-http-4.2.4-2.fc40
        Commit/branch: 49b156bbcb5bb27fbf65d70c434188ecde0903cb
    php-pecl-igbinary: 
        Source: (distgit) fedora
        NVR: php-pecl-igbinary-3.2.15-1.fc40
        Commit/branch: 9c5943a7e399ef03136668bd7039426877812446
    php-pecl-imagick: 
        Source: (distgit) fedora
        NVR: php-pecl-imagick-3.7.0-9.fc40
        Commit/branch: 93fff9985c4a99008013d803c8e3d5a28c0649fb
    php-pecl-inotify: 
        Source: (distgit) fedora
        NVR: php-pecl-inotify-3.0.0-9.fc40
        Commit/branch: 8548bec8310df71a7716dcbcc3b1754c741c9078
    php-pecl-ip2location: 
        Source: (distgit) fedora
        NVR: php-pecl-ip2location-8.2.0-3.fc40
        Commit/branch: 8ecb14f4fc49689b6a2aaf40ace575203e94b0fb
    php-pecl-json-post: 
        Source: (distgit) fedora
        NVR: php-pecl-json-post-1.1.0-9.fc40
        Commit/branch: 89deb4695d32ec7e4c48a26a18f7af238f42c9d0
    php-pecl-krb5: 
        Source: (distgit) fedora
        NVR: php-pecl-krb5-1.1.5-3.fc40
        Commit/branch: ba1c9cfcb803516bd81be841b4e5a3eefcaeaf83
    php-pecl-lzf: 
        Source: (distgit) fedora
        NVR: php-pecl-lzf-1.7.0-8.fc40
        Commit/branch: 375cdce9e197f2f35599df4cf29a9d1f772b4686
    php-pecl-mailparse: 
        Source: (distgit) fedora
        NVR: php-pecl-mailparse-3.1.6-2.fc40
        Commit/branch: 59cc273be1d7249309bfd0dcc255e16c30555a9b
    php-pecl-memcache: 
        Source: (distgit) fedora
        NVR: php-pecl-memcache-8.2-3.fc40
        Commit/branch: 47f2d3240fde7e9fe4055a473c2213a93ef68129
    php-pecl-memcached: 
        Source: (distgit) fedora
        NVR: php-pecl-memcached-3.2.0-7.fc40
        Commit/branch: ef8261537dae9254f2d51700113f1bc212214324
    php-pecl-mongodb: 
        Source: (distgit) fedora
        NVR: php-pecl-mongodb-1.17.2-1.fc40
        Commit/branch: 909057085f4d0237af6179bc5acdec71ab6c627e
    php-pecl-oauth: 
        Source: (distgit) fedora
        NVR: php-pecl-oauth-2.0.7-13.fc40
        Commit/branch: 1876516c34fb9396c158b6db0a0456e2863b5486
    php-pecl-pcov: 
        Source: (distgit) fedora
        NVR: php-pecl-pcov-1.0.11-7.fc40
        Commit/branch: 4070cf720e14b6c0938da99a1146267ed259e575
    php-pecl-pq: 
        Source: (distgit) fedora
        NVR: php-pecl-pq-2.2.2-3.fc40
        Commit/branch: 3346a29eceebe158017100ab00852fb7091af769
    php-pecl-redis5: 
        Source: (distgit) fedora
        NVR: php-pecl-redis5-5.3.7-6.fc40
        Commit/branch: f68481d1a4f6d9366882634f4df3d2672ab6a1f3
    php-pecl-rpminfo: 
        Source: (distgit) fedora
        NVR: php-pecl-rpminfo-1.1.0-1.fc40
        Commit/branch: aca59185d8e56844b52699239d5146528cfcd172
    php-pecl-rrd: 
        Source: (distgit) fedora
        NVR: php-pecl-rrd-2.0.3-10.fc40
        Commit/branch: 7c9786ce95b2a6581cacde561e1a0816dc49ebb2
    php-pecl-selinux: 
        Source: (distgit) fedora
        NVR: php-pecl-selinux-0.6.0-7.fc40
        Commit/branch: f018dadbc9ce033162f4ac48b038fd1933643450
    php-pecl-ssdeep: 
        Source: (distgit) fedora
        NVR: php-pecl-ssdeep-1.1.0-18.fc40
        Commit/branch: 1bfaa8c1866b8ac093d1cb5287119bb97a830271
    php-pecl-ssh2: 
        Source: (distgit) fedora
        NVR: php-pecl-ssh2-1.4-3.fc40
        Commit/branch: 5265a2e04418126eeddf3ec4318e418c20c7c5ea
    php-pecl-uopz: 
        Source: (distgit) fedora
        NVR: php-pecl-uopz-7.1.1-9.fc40
        Commit/branch: 15d93316bc0e7fa99a2757775766ba1e597a6aaa
    php-pecl-uuid: 
        Source: (distgit) fedora
        NVR: php-pecl-uuid-1.2.0-11.fc40
        Commit/branch: 05595e4240aecc92ac4f1db7fbb2c80775a7ca96
    php-pecl-var-representation: 
        Source: (distgit) fedora
        NVR: php-pecl-var-representation-0.1.4-5.fc40
        Commit/branch: c94c125fcfc96ee14d45526f0a06c6d5f50e9dd4
    php-pecl-xattr: 
        Source: (distgit) fedora
        NVR: php-pecl-xattr-1.4.0-13.fc40
        Commit/branch: 75b0c8edfaaf4b1a4082ccdfb58bda36c038acac
    php-pecl-xdebug3: 
        Source: (distgit) fedora
        NVR: php-pecl-xdebug3-3.3.1-2.fc40
        Commit/branch: 690c1af0d3b410c73e9cdb7f071a6e5566583c0e
    php-pecl-yac: 
        Source: (distgit) fedora
        NVR: php-pecl-yac-2.3.1-8.fc40
        Commit/branch: 1a622c662e6b0aa6b5d3178dd7101e9ca06a4323
    php-pecl-zip: 
        Source: (distgit) fedora
        NVR: php-pecl-zip-1.22.3-1.fc40
        Commit/branch: bb3ebc20ce8ae75560175aa2b686ae486e17c4b9
    php-phpiredis: 
        Source: (distgit) fedora
        NVR: php-phpiredis-1.0.1-14.fc40
        Commit/branch: 51cc44e3d930ec90e23b7da5cead7ebf743d4a8f
    php-smbclient: 
        Source: (distgit) fedora
        NVR: php-smbclient-1.1.1-3.fc40
        Commit/branch: 335a562096a9769aa60ff005134a7fea097ab73f
    php-zmq: 
        Source: (distgit) fedora
        NVR: php-zmq-1.1.3-27.fc40
        Commit/branch: 81412672233a24ca201a95a895caba3e91385bac
    php-zstd: 
        Source: (distgit) fedora
        NVR: php-zstd-0.13.1-2.fc40
        Commit/branch: cc69ce4a1bee9d026da6cf565171a6c0de999aaa
    pkcs11-helper: 
        Source: (distgit) fedora
        NVR: pkcs11-helper-1.29.0-4.fc39
        Commit/branch: 94a13f88d8704c2284c8f31eb8d56e80afb454c2
    player: 
        Source: (distgit) fedora
        NVR: player-3.1.0-52.fc39
        Commit/branch: 6a086a22735a4f95a44167450a0ca9c2eaae5ef9
    plotdrop: 
        Source: (distgit) fedora
        NVR: plotdrop-0.5.3-33.fc40
        Commit/branch: b1ed0f7d3264286d9d53c02bffebe470db77282c
    pmars: 
        Source: (distgit) fedora
        NVR: pmars-0.9.2-31.fc40
        Commit/branch: 8b26b27217f668677684eea8325091fc367b6371
    pngnq: 
        Source: (distgit) fedora
        NVR: pngnq-1.1-31.fc39
        Commit/branch: 808ed2e4aa6db5242e4dfd604a85ec3ecd978983
    pqmarble: 
        Source: (distgit) fedora
        NVR: pqmarble-2.0.0~^1.f240b2e-1.fc39
        Commit/branch: 9c780502c0d28e554f3499c0525a00de61d11f65
    prboom: 
        Source: (distgit) fedora
        NVR: prboom-2.5.0-31.fc39
        Commit/branch: cb110880898479d1de98c6a3a99015c6bc09d0f1
    prboom-plus: 
        Source: (distgit) fedora
        NVR: prboom-plus-2.6.66-2.fc39
        Commit/branch: 423474377e6daeac5414f4af268ea536233dd534
    prelude-lml: 
        Source: (distgit) fedora
        NVR: prelude-lml-5.2.0-16.fc39
        Commit/branch: 5e99fdd2062dee93006b44b9f873892f01275ab1
    premake: 
        Source: (distgit) fedora
        NVR: premake-5.0.0-2.20231215git78afb22.fc40
        Commit/branch: 58ccfbdf60df64d877fcfe96236727e68505d59d
    procdump: 
        Source: (distgit) fedora
        NVR: procdump-2.1-1.fc40
        Commit/branch: 1acd276454e72c91c8c62e09c9bf6329203ea356
    procmail: 
        Source: (distgit) fedora
        NVR: procmail-3.24-3.fc40
        Commit/branch: 89fc886d7038af25b785bef8106c88b9e6ce1ba4
    proftpd: 
        Source: (distgit) fedora
        NVR: proftpd-1.3.8b-2.fc40
        Commit/branch: 14b2d9aeb594ad37be89b9546e36370caa896348
    progress: 
        Source: (distgit) fedora
        NVR: progress-0.14-12.fc39
        Commit/branch: b0d5d797d3063f579973f320c2d8dcf1ff98d756
    proxmark3: 
        Source: (distgit) fedora
        NVR: proxmark3-4.17511-1.fc40
        Commit/branch: 7d52ef1e0b1246fbcf001fe306d5dd05015c5347
    proxysql: 
        Source: (distgit) fedora
        NVR: proxysql-2.4.8-2.fc39
        Commit/branch: 6d0e324c840cdb617f4afa89ebdbfa161b76730d
    psfex: 
        Source: (distgit) fedora
        NVR: psfex-3.17.1-29.fc39
        Commit/branch: bd7a73a78f82777c0f2561681f244ec078b7be28
    py-radix: 
        Source: (distgit) fedora
        NVR: py-radix-0.10.0-7.fc39
        Commit/branch: c355ad7e2047872dbc9014065e6b2e209b8c6e45
    pypy: 
        Source: (distgit) fedora
        NVR: pypy-7.3.13-1.fc40
        Commit/branch: 801e654a17ea0a576e170cd1b9179bfc33ac1515
    pypy3.10: 
        Source: (distgit) fedora
        NVR: pypy3.10-7.3.13-1.3.10.fc40
        Commit/branch: 61e82ac00a4b718b7edc15072c3488e5e0b6b312
    pypy3.9: 
        Source: (distgit) fedora
        NVR: pypy3.9-7.3.13-1.3.9.fc40
        Commit/branch: 18f5682471598173bf765961f6330b1560db9690
    pythia8: 
        Source: (distgit) fedora
        NVR: pythia8-8.3.10-2.fc40
        Commit/branch: b43b57a8ffe512bbce183ba68fc80114a0edd557
    python-astropy-healpix: 
        Source: (distgit) fedora
        NVR: python-astropy-healpix-1.0.2-1.fc40
        Commit/branch: 3f971bee251e42bc7cb999f0afc2b9e6c2285f8f
    python-axolotl-curve25519: 
        Source: (distgit) fedora
        NVR: python-axolotl-curve25519-0.4.1-8.fc39
        Commit/branch: 88160219dd3ba8c2652df4db2194798c1eb87b9a
    python-cbor2: 
        Source: (distgit) fedora
        NVR: python-cbor2-5.1.2-12.fc40
        Commit/branch: 3af1d09887b2a529be1a1542e20f677fb8d5f10f
    python-datrie: 
        Source: (distgit) fedora
        NVR: python-datrie-0.8.2-22.fc40
        Commit/branch: 106460d257f6e9b684e8233317b11c282bf4bd13
    python-gammu: 
        Source: (distgit) fedora
        NVR: python-gammu-3.2.4-7.fc39
        Commit/branch: 8dbe277ccbe36a879b0cd4ac001f8e90a799998b
    python-gensim: 
        Source: (distgit) fedora
        NVR: python-gensim-0.10.0-35.fc39
        Commit/branch: 15d975241b10a9e05c73a1d76384828c91775da4
    python-gradunwarp: 
        Source: (distgit) fedora
        NVR: python-gradunwarp-1.2.1-5.fc40
        Commit/branch: 7e5d3da7d286d1449bcefecbd0c5dbde0de86a61
    python-kadmin: 
        Source: (distgit) fedora
        NVR: python-kadmin-0.1.2-20.20181207git94e50ed.fc39
        Commit/branch: 7d0fd5cd1ee7d1239e4ce06c5bbfc91f746bc003
    python-lxml: 
        Source: (distgit) fedora
        NVR: python-lxml-4.9.4-1.fc40
        Commit/branch: 63254d5e35fb70ee790ec6f395d281118195dadc
    python-mapnik: 
        Source: (distgit) fedora
        NVR: python-mapnik-3.0.23-26.20200224git7da019c.fc39
        Commit/branch: d0a4640d10a01796a9017003a51a281639968897
    python-multidict: 
        Source: (distgit) fedora
        NVR: python-multidict-6.0.4-3.fc39
        Commit/branch: d3689b8ad2f6bf16ec998aba215279eea2b7d59e
    python-nipy: 
        Source: (distgit) fedora
        NVR: python-nipy-0.5.0^20231006gite5e063b-5.fc40
        Commit/branch: 6aca3aa24d16fc8b09bab3097b0a39eef4a157a7
    python-poetry-core: 
        Source: (distgit) fedora
        NVR: python-poetry-core-1.7.0-1.fc40
        Commit/branch: d9d3eb8e2c0d41f23b598325ca3b642758ff92d7
    python-pyopengl: 
        Source: (distgit) fedora
        NVR: python-pyopengl-3.1.7-3.fc39
        Commit/branch: 66718f73b723b1c4a1219be0b54d7b55368b7e43
    python-pyqt5-sip: 
        Source: (distgit) fedora
        NVR: python-pyqt5-sip-12.13.0-1.fc40
        Commit/branch: ad8b28ff9c0aa4a3be34d9f7c18dc1e13c2a2459
    python-pysam: 
        Source: (distgit) fedora
        NVR: python-pysam-0.22.0-2.fc40
        Commit/branch: 18e228e80b2314f4b0c2b3d4c1eb3d211edc2939
    python-ruamel-yaml-clib: 
        Source: (distgit) fedora
        NVR: python-ruamel-yaml-clib-0.2.7-3.fc39
        Commit/branch: a8a99c521ee462b659ba51bfed2963a423d0b0a0
    python-scikit-learn: 
        Source: (distgit) fedora
        NVR: python-scikit-learn-1.3.1-2.fc40
        Commit/branch: 2bb36d7ba01b740158454b7623cd7e42186e00f3
    python-subvertpy: 
        Source: (distgit) fedora
        NVR: python-subvertpy-0.10.1-22.fc39
        Commit/branch: 196ee1890cb93324dc6e9868be31e0119f56703b
    python-wxpython4: 
        Source: (distgit) fedora
        NVR: python-wxpython4-4.2.1-4.fc40
        Commit/branch: fda5b836e25a5dac4fd5e50b5e517310403c6338
    python-xmlsec: 
        Source: (distgit) fedora
        NVR: python-xmlsec-1.3.13-4.fc39
        Commit/branch: 751ec16ebacceb1692ac5a4b9107da3b7324ad57
    python2.7: 
        Source: (distgit) fedora
        NVR: python2.7-2.7.18-37.fc40
        Commit/branch: 840adda0efcf34fb80efe087e82b981e6f32e7a7
    qalculate-gtk: 
        Source: (distgit) fedora
        NVR: qalculate-gtk-4.9.0-1.fc40
        Commit/branch: e575d0ccbf9c4332a764e863d6dea8caeba68ec8
    qalculate-qt: 
        Source: (distgit) fedora
        NVR: qalculate-qt-4.9.0-1.fc40
        Commit/branch: ce15df04200ba37d209e12cbf65d2fcdd70144c7
    qpdf: 
        Source: (distgit) fedora
        NVR: qpdf-11.6.4-2.fc40
        Commit/branch: 8ecc4f2c15664714391d84f4e7b50057ea9a191a
    qsopt-ex: 
        Source: (distgit) fedora
        NVR: qsopt-ex-2.5.10.3-1.20170729gite5d498f.fc40
        Commit/branch: a8d03db3a90ff9a59bbef8bf793270a12bb05eed
    qt-creator: 
        Source: (distgit) fedora
        NVR: qt-creator-12.0.1-1.fc40
        Commit/branch: 83225dc084becb8e5119e802bc23952b0b84215a
    qt5-qtlocation: 
        Source: (distgit) fedora
        NVR: qt5-qtlocation-5.15.11-1.fc40
        Commit/branch: fa5bfd9b1d5302ab9aed7b3f9d5ae8a441488efd
    qt5-qtwebkit: 
        Source: (distgit) fedora
        NVR: qt5-qtwebkit-5.212.0-0.79alpha4.fc40
        Commit/branch: 960dd8639a531d067bb98e0e456e719a000d67a6
    qt6-qtbase: 
        Source: (distgit) fedora
        NVR: qt6-qtbase-6.6.1-1.fc40
        Commit/branch: 0916bd3f34c920e843f0ff756f88a100537ccc18
    qt6-qttools: 
        Source: (distgit) fedora
        NVR: qt6-qttools-6.6.1-1.fc40
        Commit/branch: 932514bc3740a67dca5f48f4e3bedbcaa0e0a13c
    qtwebkit: 
        Source: (distgit) fedora
        NVR: qtwebkit-2.3.4-39.fc39
        Commit/branch: 9b9497e11aefbb685ec161ab9570df1fb227dced
    quadrapassel: 
        Source: (distgit) fedora
        NVR: quadrapassel-40.2-6.fc39
        Commit/branch: e021fe2444ec9957c02cc98f05b5f3901ade62ce
    quake2: 
        Source: (distgit) fedora
        NVR: quake2-8.30-1.fc40
        Commit/branch: 070a17eb76d9e793f2afa97cb46ff62e72c0db3a
    quake3: 
        Source: (distgit) fedora
        NVR: quake3-1.36-44.svn2102.fc39
        Commit/branch: 36aa90c13e23568556c9a3d40fe9151ec77f4fde
    quantum-espresso: 
        Source: (distgit) fedora
        NVR: quantum-espresso-7.0-6.fc39
        Commit/branch: 9108eb8bf4b4930f95570a8800613a06a4e3a72a
    quesoglc: 
        Source: (distgit) fedora
        NVR: quesoglc-0.7.2-36.fc39
        Commit/branch: 07a446baaaaaa35aba4bc873c49b5e57d7580dfc
    quicksynergy: 
        Source: (distgit) fedora
        NVR: quicksynergy-0.8.1-30.fc39
        Commit/branch: dcb8b318bc75640e5210081cf351d8a4a0df0a20
    quisk: 
        Source: (distgit) fedora
        NVR: quisk-4.2.28-1.fc40
        Commit/branch: cf052abf4793a821e9044624c2c640129561fb6b
    radare2: 
        Source: (distgit) fedora
        NVR: radare2-5.8.8-3.fc40
        Commit/branch: cf7acffaa8a1866b66a7a02e13888e76a68c46af
    raddump: 
        Source: (distgit) fedora
        NVR: raddump-0.3.1-31.fc39
        Commit/branch: 29899f242c84c534be9d48318f96bb5fa78267d4
    rakarrack: 
        Source: (distgit) fedora
        NVR: rakarrack-0.6.2-0.26.20150814gita620840.fc39
        Commit/branch: c5398067a883907cedbf3138f08c4e3b09a42e72
    randomx: 
        Source: (distgit) fedora
        NVR: randomx-1.2.1-1.fc40
        Commit/branch: c0aad004d6e39d85d717fa2c7a203ac923447b6c
    range-v3: 
        Source: (distgit) fedora
        NVR: range-v3-0.12.0-4.fc39
        Commit/branch: 519c5bbc1c22b750b809124603fb739a2848f0f9
    rapidjson: 
        Source: (distgit) fedora
        NVR: rapidjson-1.1.0-23.fc39
        Commit/branch: 3f85f1f55728dfd7f7e95717f3c9d21e1e711eb8
    rasdaemon: 
        Source: (distgit) fedora
        NVR: rasdaemon-0.8.0-2.fc39
        Commit/branch: 9842e566c1f0a22161e3c3c933ce68c0045acf0b
    rats: 
        Source: (distgit) fedora
        NVR: rats-2.4-24.fc39
        Commit/branch: 94cf3a6c8bdc4bde27f5f2af10db1f63494c45c2
    rcssserver: 
        Source: (distgit) fedora
        NVR: rcssserver-18.1.3-2.fc39
        Commit/branch: 2373ac89f1d364653a0bdfa1e33baa9bad9f1d83
    rdist: 
        Source: (distgit) fedora
        NVR: rdist-6.1.5-81.fc39
        Commit/branch: 89d833d053a984b89c9b60a0846b9ff2223ef611
    redis: 
        Source: (distgit) fedora
        NVR: redis-7.2.3-1.fc40
        Commit/branch: 8e80338fe2cba2c52b3f0fd9f378128984da91f1
    regextester: 
        Source: (distgit) fedora
        NVR: regextester-1.1.1-6.fc39
        Commit/branch: 964e2f38e1aa6142675499f5c41fe29571c28604
    remctl: 
        Source: (distgit) fedora
        Commit/branch: 325741a504e4e95b6c72ab6fbba7c3bb75fee045
    remmina: 
        Source: (distgit) fedora
        NVR: remmina-1.4.33-1.fc40
        Commit/branch: 5db33be80c8ab460f0a00283e5f148023efb050c
    restool: 
        Source: (distgit) fedora
        NVR: restool-2.4.0-6.fc39
        Commit/branch: c0da72a29f8548d61848ee49d86d6a03220a8f64
    retroarch: 
        Source: (distgit) fedora
        NVR: retroarch-1.16.0.3-2.fc40
        Commit/branch: 664693146827ee1cc7708c76290f615f705f028c
    rizin: 
        Source: (distgit) fedora
        NVR: rizin-0.6.3-1.fc40
        Commit/branch: 2e37a6eba481664d58616790febe780e482976ad
    rkdeveloptool: 
        Source: (distgit) fedora
        NVR: rkdeveloptool-1.32-7.fc39
        Commit/branch: c9bf2e45dcc27c069f0db76887d8af5aafdba734
    rocksdb: 
        Source: (distgit) fedora
        NVR: rocksdb-8.3.2-1.fc40
        Commit/branch: 8412f2d193acc57dc2010b39597aea5b197a0d87
    rocm-compilersupport: 
        Source: (distgit) fedora
        NVR: rocm-compilersupport-17.1-1.fc40
        Commit/branch: 9ec914e2e8924945740d348d1f6dac9473d81795
    rocm-smi: 
        Source: (distgit) fedora
        NVR: rocm-smi-6.0.0-1.fc40
        Commit/branch: 71014bf94f3a91da110e575f449c3dc33bb15408
    rpld: 
        Source: (distgit) fedora
        NVR: rpld-1.8-0.39.beta1.fc39
        Commit/branch: bc66c7acc658163ac554ab96c7ba6127c5e170b5
    rr: 
        Source: (distgit) fedora
        NVR: rr-5.7.0-9.fc40
        Commit/branch: 36da997cce53ed8e58842471863f4e26bf8e66a3
    rrdtool: 
        Source: (distgit) fedora
        NVR: rrdtool-1.8.0-12.fc39
        Commit/branch: 76f479e1055524e66f74b10c96ce5011fa78269c
    rss-glx: 
        Source: (distgit) fedora
        NVR: rss-glx-0.9.1.p-58.fc39
        Commit/branch: 67f06bd2b40391c3fff74b9ba4bf28f7cac9f76a
    rsync-bpc: 
        Source: (distgit) fedora
        NVR: rsync-bpc-3.1.3.0-9.fc40
        Commit/branch: 08f4ee84882a2c99b7cd3588d1e6cda9f046bd43
    rtklib: 
        Source: (distgit) fedora
        NVR: rtklib-2.4.3.b33-11.fc39
        Commit/branch: f306d6098fd6bf0cb62e2153b08a583d58c8989c
    rubiks: 
        Source: (distgit) fedora
        NVR: rubiks-20070912-7.fc39
        Commit/branch: 02f6cecb04fa3e2e33cb26e439769a7f01003ca6
    ruby-augeas: 
        Source: (distgit) fedora
        NVR: ruby-augeas-0.5.0-34.fc39
        Commit/branch: 429b06c213951fb3ec6629fadf143c5d3938e7bb
    ruby-gnome2: 
        Source: (distgit) fedora
        NVR: ruby-gnome2-0.90.4-15.fc40
        Commit/branch: da44099ebd91d8b413be9871ccc506da016c54d1
    rubygem-glu: 
        Source: (distgit) fedora
        NVR: rubygem-glu-8.3.0-25.fc40
        Commit/branch: d84c8fcaa8a307052c7994e2953f5d8426065455
    rubygem-glut: 
        Source: (distgit) fedora
        NVR: rubygem-glut-8.3.0-23.fc39
        Commit/branch: d7ac54d11e6c14fb8e5e9657aea38ce57984fc47
    rubygem-msgpack: 
        Source: (distgit) fedora
        NVR: rubygem-msgpack-1.4.4-5.fc39
        Commit/branch: 1280b46d9de6493d511945d0d230880ef57b5233
    rubygem-ncursesw: 
        Source: (distgit) fedora
        NVR: rubygem-ncursesw-1.4.10-11.fc39
        Commit/branch: c49e054e35533988f3d3517faa1cae488db041a7
    rubygem-nokogiri: 
        Source: (distgit) fedora
        NVR: rubygem-nokogiri-1.15.5-1.fc40
        Commit/branch: ea58e84d092cdbd3f989ce284ef44118420dd646
    rubygem-opengl: 
        Source: (distgit) fedora
        NVR: rubygem-opengl-0.10.0-25.fc40
        Commit/branch: f909f27cfefd0b0a0978048362fff6da4dc937a7
    rubygem-posix-spawn: 
        Source: (distgit) fedora
        NVR: rubygem-posix-spawn-0.3.15-10.fc39
        Commit/branch: 0c754b642a199bc2a896bac5ee4f5664681f2e80
    rubygem-serialport: 
        Source: (distgit) fedora
        NVR: rubygem-serialport-1.3.2-7.fc39
        Commit/branch: aeb7728ecf701b78300ea509f59c061f7356f757
    rubygem-syck: 
        Source: (distgit) fedora
        NVR: rubygem-syck-1.4.1-8.fc40
        Commit/branch: 819d10c6b2b5ccc69b26ec13d70adc008c6a0dad
    rubygem-unicode: 
        Source: (distgit) fedora
        NVR: rubygem-unicode-0.4.4.4-8.fc39
        Commit/branch: d04d9e6828af04b3af8e41134fe5a343179125c3
    rusers: 
        Source: (distgit) fedora
        NVR: rusers-0.17-103.fc39
        Commit/branch: d6a5a11d12f85784ad9b2c7bc94939be28fb2e0c
    samba: 
        Source: (distgit) fedora
        NVR: samba-4.19.3-2.fc40
        Commit/branch: 93059a0a6a3e5c5df282344a04f9b11caa6583df
    sane-backends: 
        Source: (distgit) fedora
        NVR: sane-backends-1.2.1-7.fc40
        Commit/branch: 34e7ef5c253d9fa614c361decc7c004cad12b3ba
    sblim-cmpi-nfsv4: 
        Source: (distgit) fedora
        NVR: sblim-cmpi-nfsv4-1.1.0-33.fc40
        Commit/branch: 5ec54d2da5f625dbfa6cbe5db94d0bb0904c22f5
    sblim-cmpi-rpm: 
        Source: (distgit) fedora
        NVR: sblim-cmpi-rpm-1.0.1-36.fc39
        Commit/branch: 45268fe0358b966930fb58ab8751b739227761e9
    sblim-sfcc: 
        Source: (distgit) fedora
        NVR: sblim-sfcc-2.2.8-23.fc39
        Commit/branch: c84c773f5c457ef8abd25587001686d6264c9634
    schedtool: 
        Source: (distgit) fedora
        NVR: schedtool-1.3.0-28.fc39
        Commit/branch: 976f8ec0638ae8c57c541114eb93c7b0e02a4e7d
    scorep: 
        Source: (distgit) fedora
        NVR: scorep-8.1-5.fc40
        Commit/branch: 0403fb34906dbcd0cfbcc41c4903618e076a54bd
    scottfree: 
        Source: (distgit) fedora
        NVR: scottfree-1.14-27.fc39
        Commit/branch: 320097ef93112146fd9b7d0c7620c31d1d054caa
    scummvm: 
        Source: (distgit) fedora
        NVR: scummvm-2.7.1-1.fc39
        Commit/branch: 90359c3ed9f7cdb2004156905586087757b472db
    sdbus-cpp: 
        Source: (distgit) fedora
        NVR: sdbus-cpp-1.4.0-1.fc40
        Commit/branch: 9491986467d707bc0ab7e1a6788ff6a031146bf3
    sdrpp: 
        Source: (distgit) fedora
        NVR: sdrpp-1.0.4-18.fc40
        Commit/branch: ad4790c9e660d8a1dee9904d4d2b81b52a817563
    seahorse: 
        Source: (distgit) fedora
        NVR: seahorse-43.0-4.fc39
        Commit/branch: b5f0b5a5d524b0e3e99239fa1a78e15a85dcfec2
    sequeler: 
        Source: (distgit) fedora
        NVR: sequeler-0.8.2-21.fc40
        Commit/branch: 0c4d8cf923d6f8b530cb01670220c39fb2af6e5e
    serdisplib: 
        Source: (distgit) fedora
        NVR: serdisplib-1.97.9-26.fc38
        Commit/branch: cf7d1e5a620e9f745cc1726955151259bf60bbf1
    sicktoolbox: 
        Source: (distgit) fedora
        NVR: sicktoolbox-1.0.1-21.fc39
        Commit/branch: 327e9684337c2b2aa4a0180ce9dc2c9aa1471648
    simspark: 
        Source: (distgit) fedora
        NVR: simspark-0.3.5-2.fc39
        Commit/branch: 3941caa87da8951415112e72c19daf0e0a462015
    sing: 
        Source: (distgit) fedora
        NVR: sing-1.1-28.fc39
        Commit/branch: b0b2f8c752f1a05e50e407f88c888bd47713037b
    sip: 
        Source: (distgit) fedora
        NVR: sip-4.19.25-9.fc39
        Commit/branch: ecd053a61b179f82eb2cdaacfc433fe0304ebbc4
    skf: 
        Source: (distgit) fedora
        NVR: skf-2.10.16-2.fc39
        Commit/branch: 4145a5afee04ba755a3da95ebd4b3c799aa347ef
    slashem: 
        Source: (distgit) fedora
        NVR: slashem-0.0.8-0.37.E0F1.fc39
        Commit/branch: b70b173a87bdd9f6151e78a62202d9780c3935e0
    sleef: 
        Source: (distgit) fedora
        NVR: sleef-3.5.1-29.fc40
        Commit/branch: 5c9874ba7e98b55d8bf43db3ed34acff2567813c
    smartsim: 
        Source: (distgit) fedora
        NVR: smartsim-1.4-21.fc39
        Commit/branch: eacd4e36554aaf85cb73ff306cda3127ca16775a
    smatch: 
        Source: (distgit) fedora
        NVR: smatch-1.73-2.fc40
        Commit/branch: b1a21d86fbdd9ad30c07fd03450afc8e58b16b5e
    sofia-sip: 
        Source: (distgit) fedora
        NVR: sofia-sip-1.13.16-2.fc39
        Commit/branch: 6e1dc5cd45116223a48396d886522a5ff24c1b25
    spacefm: 
        Source: (distgit) fedora
        NVR: spacefm-1.0.6-14.fc39
        Commit/branch: 20c3921d61b25fb1c47004310a0b5be3dbd6a824
    sphinx: 
        Source: (distgit) fedora
        NVR: sphinx-2.2.11-28.fc40
        Commit/branch: 0827c4c5202e77a2bcd914bf182b24d6f4248c49
    splat: 
        Source: (distgit) fedora
        NVR: splat-1.4.2-19.fc39
        Commit/branch: 9fe814f01219dddabe594ea54e5fe43d082261c0
    squid: 
        Source: (distgit) fedora
        NVR: squid-6.6-1.fc40
        Commit/branch: 5580eab2d9db7b0f14a66cd8e8c17fa905490a51
    sslh: 
        Source: (distgit) fedora
        NVR: sslh-1.21c-7.fc39
        Commit/branch: 0595669241d7f64b0055c294cb2aac4674b1250c
    ssmtp: 
        Source: (distgit) fedora
        NVR: ssmtp-2.64-33.fc39
        Commit/branch: aa92059266bb56f338946603959cece3d6c9d34c
    staden-io_lib: 
        Source: (distgit) fedora
        NVR: staden-io_lib-1.14.8-2.fc39
        Commit/branch: 60efc20c8890f17975309d28ed8a98ef45914fc9
    stellarium: 
        Source: (distgit) fedora
        NVR: stellarium-23.4-1.fc40
        Commit/branch: 4a6dd0cbf2c94d7da0a9caaa6bea047b6807b998
    stellarsolver: 
        Source: (distgit) fedora
        NVR: stellarsolver-2.5-2.fc40
        Commit/branch: 1ce245e315b8da5fa29cdac1a7ce7ed6eb358a4c
    streameye: 
        Source: (distgit) fedora
        NVR: streameye-0.9-10.fc39
        Commit/branch: ab958e16193d6eab9040ca0ea2b6dbca12c83d38
    suricata: 
        Source: (distgit) fedora
        NVR: suricata-6.0.15-1.fc40
        Commit/branch: 44c1e15cf3e2745cc4463d32814bd5a0e45eb7a8
    swaybg: 
        Source: (distgit) fedora
        NVR: swaybg-1.2.0-3.fc39
        Commit/branch: 4e36cfc1f768660014499d91dbf55b47f6197238
    swig: 
        Source: (distgit) fedora
        NVR: swig-4.1.1-15.fc40
        Commit/branch: f77b41fc2ebad319ae54a1dfb3aeef1046c9b371
    switchtec: 
        Source: (distgit) fedora
        NVR: switchtec-4.1-2.fc39
        Commit/branch: 5e86348047d46601f62c42ab63461ff0b3f84f07
    sword: 
        Source: (distgit) fedora
        NVR: sword-1.9.0-22.fc40
        Commit/branch: 7b1439e7bc52bad6ddbcdfb092c023df9d184798
    sylpheed: 
        Source: (distgit) fedora
        NVR: sylpheed-3.7.0-15.fc39
        Commit/branch: 78c614b7c6e4b67c6d085020c8dd81b6a5054984
    sympy: 
        Source: (distgit) fedora
        NVR: sympy-1.12-3.fc40
        Commit/branch: e6b3f6e09470c082f0ed3c0589905ccd09798a82
    synapse: 
        Source: (distgit) fedora
        NVR: synapse-0.2.99.4-13.fc39
        Commit/branch: 6d7b1af9b41ef8b3d52da410fb8421dc9eca8762
    systemd: 
        Source: (distgit) fedora
        NVR: systemd-255.1-1.fc40
        Commit/branch: 3211e4adfcca38dfe24188e28a65b1cf385ecfd6
    systemtap: 
        Source: (distgit) fedora
        NVR: systemtap-5.0~pre16958465gca71442b-1.fc40
        Commit/branch: b7cac72f196f1b5ac290e43df1d132277fb21163
    t4k_common: 
        Source: (distgit) fedora
        NVR: t4k_common-0.1.1-34.fc39
        Commit/branch: 3495b04fd69ecc64c8bad5abc5f7df7d208b4e0a
    tcd-utils: 
        Source: (distgit) fedora
        NVR: tcd-utils-20120115-23.fc39
        Commit/branch: 7a96b14ea97e876030da59ede206ae17b495ef3c
    tcl-signal: 
        Source: (distgit) fedora
        NVR: tcl-signal-1.4-28.fc39
        Commit/branch: 50820e2f8c7873c5f565e9793a77f17526d6ff22
    tcl-snack: 
        Source: (distgit) fedora
        NVR: tcl-snack-2.2.10-57.fc39
        Commit/branch: f1ade317260c133d165de5c4b42b078b1bb12a59
    tcl-tclxml: 
        Source: (distgit) fedora
        NVR: tcl-tclxml-3.2-34.fc39
        Commit/branch: 290994106883dc18939899ac9f760b03ccc0b747
    tcl-trf: 
        Source: (distgit) fedora
        NVR: tcl-trf-2.1.4-29.fc39
        Commit/branch: 55b16956b2e3adb75cde4e79e762f49b7b2c32e0
    tcpick: 
        Source: (distgit) fedora
        NVR: tcpick-0.2.1-46.fc40
        Commit/branch: 15ce9cb96658c3a3a5f93e6f530219ae7e0a45d2
    tdlib: 
        Source: (distgit) fedora
        NVR: tdlib-1.8.0-4.fc39
        Commit/branch: 8053922ba7f278e4c48b0bb01ba361066fa3d0ae
    telepathy-glib: 
        Source: (distgit) fedora
        NVR: telepathy-glib-0.24.2-9.fc40
        Commit/branch: f60337be4fa9f3e7bd33ad1eb2e0f165e0754715
    testdisk: 
        Source: (distgit) fedora
        NVR: testdisk-7.1-11.fc39
        Commit/branch: d9a47d0146dd6ea25e134756b204d9101afaa768
    tetgen: 
        Source: (distgit) fedora
        NVR: tetgen-1.5.0-24.fc39
        Commit/branch: 8155fc53ed9fa1fc4d02718584a004995b4a1975
    texlive-base: 
        Source: (distgit) fedora
        NVR: texlive-base-20230311-77.fc39
        Commit/branch: 2b476ad04f91800ec0438eb0ef544444f04078c1
    tilibs: 
        Source: (distgit) fedora
        NVR: tilibs-1.19-7.20220716git8ffa244.fc39
        Commit/branch: d55be9b11e9b99dd5bdc1f132b33ac4eb0882847
    tintin: 
        Source: (distgit) fedora
        NVR: tintin-2.02.30-3.fc39
        Commit/branch: 47735f57c7e8658a253242d471a0e1e7ac2b77b8
    tinyfugue: 
        Source: (distgit) fedora
        NVR: tinyfugue-5.0-0.109.b8.fc39
        Commit/branch: 0953e5ce9a635a9bc28d46bfcee2bc6c29d7fdf2
    tix: 
        Source: (distgit) fedora
        NVR: tix-8.4.3-38.fc39
        Commit/branch: 1634b8ff13db8aa153a72dc69b5b80d7c939b002
    tlpi: 
        Source: (distgit) fedora
        NVR: tlpi-231120-1.fc40
        Commit/branch: 2d145641f5a39b9f61cacddeb957b6ac0b0999eb
    tlsh: 
        Source: (distgit) fedora
        NVR: tlsh-4.11.2-8.fc39
        Commit/branch: 859c2050c55c02f65f32cfec7fabcb3d545c3604
    tokodon: 
        Source: (distgit) fedora
        NVR: tokodon-24.01.85-1.fc40
        Commit/branch: 751e554777e3151ec021b7d48f07c4d4b026b040
    toppler: 
        Source: (distgit) fedora
        NVR: toppler-1.3-1.fc40
        Commit/branch: 3065e2ccfa3c7efedfee5397e09a38898fd9f4d0
    torcs: 
        Source: (distgit) fedora
        NVR: torcs-1.3.7-19.fc39
        Commit/branch: 8118dd6169342f30eebbc2e3e355fa4fba276cba
    torque: 
        Source: (distgit) fedora
        NVR: torque-6.1.3-9.fc39
        Commit/branch: ad0982c7bb6860e340c380b1c1e0624b2c898bc7
    tpm2-tss-engine: 
        Source: (distgit) fedora
        NVR: tpm2-tss-engine-1.2.0-2.fc39
        Commit/branch: eb518d75dc5e5691118ab7a0a54f6af715659d92
    tracker: 
        Source: (distgit) fedora
        NVR: tracker-3.6.0-1.fc40
        Commit/branch: a1959a7838cd381f0e32609ea334d849e3d8b949
    trafficserver: 
        Source: (distgit) fedora
        NVR: trafficserver-9.2.3-1.fc40
        Commit/branch: d1a41acd919d61017c4eda0982cf29dc0a656180
    tremulous: 
        Source: (distgit) fedora
        NVR: tremulous-1.2.0-0.35.beta1.fc39
        Commit/branch: c55040ce6613a4307de5164b4ef105ae6ba36c96
    ttf2pt1: 
        Source: (distgit) fedora
        NVR: ttf2pt1-3.4.4-38.fc39
        Commit/branch: 0a16a99be178573651b89874dab1b049d41a9b06
    ttfautohint: 
        Source: (distgit) fedora
        NVR: ttfautohint-1.8.4-7.fc39
        Commit/branch: b94baa5e58529291d90c35bbbb21f6bc12c8028e
    tth: 
        Source: (distgit) fedora
        NVR: tth-4.16-3.fc39
        Commit/branch: 06f949e7c72d23fe57ae56a5e2d718bfe253e673
    tuxguitar: 
        Source: (distgit) fedora
        NVR: tuxguitar-1.5.4-8.fc39
        Commit/branch: 76669bb791e3bc19381a4485a8df0b47f14d1cf1
    tuxmath: 
        Source: (distgit) fedora
        NVR: tuxmath-2.0.3-16.fc39
        Commit/branch: 1e861d813ababda8c7ccfdef4f80113f215b187a
    typespeed: 
        Source: (distgit) fedora
        NVR: typespeed-0.6.5-28.fc39
        Commit/branch: 3ea0ce507dd0ad1efad80ccb9882e5a1bbf4eeaf
    ucblogo: 
        Source: (distgit) fedora
        NVR: ucblogo-6.2.3-4.fc39
        Commit/branch: d64e4ad7a6dde9e2d132af18c2c701affd2feaba
    ufraw: 
        Source: (distgit) fedora
        NVR: ufraw-0.23-0.17.20210425.fc39
        Commit/branch: cab3ba9259a97abb18cc1c72412dacac2dffb7d7
    ugene: 
        Source: (distgit) fedora
        NVR: ugene-49.1-1.fc40
        Commit/branch: 74c1d260d34b0c4b9f70b986798dbaf452bdbc04
    ularn: 
        Source: (distgit) fedora
        NVR: ularn-1.5p4-45.fc40
        Commit/branch: 6e0a504fc6c48d68aaf5337be3c47857e46d64bf
    ultimatestunts: 
        Source: (distgit) fedora
        NVR: ultimatestunts-0.7.7-24.fc39
        Commit/branch: 08848c22b762e507637fe884b0de6300dfa6f413
    unar: 
        Source: (distgit) fedora
        NVR: unar-1.10.8-3.fc40
        Commit/branch: 3f7a50540fb00f3beeb6c1bc7dc190300f81064d
    unifont: 
        Source: (distgit) fedora
        NVR: unifont-15.0.01-3.fc39
        Commit/branch: 30404c76eb45abde2bafdf339f09d18d013536d5
    uucp: 
        Source: (distgit) fedora
        NVR: uucp-1.07-69.fc39
        Commit/branch: 6dcf5a690bf153bf5903603bdc4f5d7ec304d795
    uudeview: 
        Source: (distgit) fedora
        NVR: uudeview-0.5.20-53.fc39
        Commit/branch: f18230b9913ca77089a4cca91e992e8954d64d3a
    uwsgi: 
        Source: (distgit) fedora
        NVR: uwsgi-2.0.23-1.fc40
        Commit/branch: b6771ca19fd5acfb2834ccf85ade717739f3a232
    v4l-utils: 
        Source: (distgit) fedora
        NVR: v4l-utils-1.26.1-1.fc40
        Commit/branch: 34a18ca80dd21f9d704c215da3854b5d82f11116
    vacuum-im: 
        Source: (distgit) fedora
        NVR: vacuum-im-1.3.0-0.28.20211209git0abd5e1.fc39
        Commit/branch: 01d08371ea6d781848d379fd10d92984b69b899e
    vala-language-server: 
        Source: (distgit) fedora
        NVR: vala-language-server-0.48.7-1.fc40
        Commit/branch: b76a662cf7f58ba7cfe64a52307dae38a35a3384
    vdr: 
        Source: (distgit) fedora
        NVR: vdr-2.6.4-2.fc40
        Commit/branch: baef91de0aa6ee6c7aad2aa17fe2f698a9886ab2
    vdr-skinelchihd: 
        Source: (distgit) fedora
        NVR: vdr-skinelchihd-1.2.3-1.fc40
        Commit/branch: df529b6ba0646cc3b8290c2da4880cf575a91832
    vdr-streamdev: 
        Source: (distgit) fedora
        NVR: vdr-streamdev-0.6.3-6.fc39
        Commit/branch: 9bee036b02d190d9f647da51dac2fee5e9a317b9
    vecgeom: 
        Source: (distgit) fedora
        NVR: vecgeom-1.2.7-1.fc40
        Commit/branch: 69e491b1cecdb62c3fed1202270814aa4636f8e4
    verilator: 
        Source: (distgit) fedora
        NVR: verilator-5.014-2.fc40
        Commit/branch: 6f92137ff1ace98f4b81835f0e171b2f9730f259
    vfrnav: 
        Source: (distgit) fedora
        NVR: vfrnav-20230429-4.fc40
        Commit/branch: d4bfc343fd344b3c71b0929f55a3e1614cd5fa8e
    vlc: 
        Source: (distgit) fedora
        NVR: vlc-3.0.20-6.fc40
        Commit/branch: 81277a94f1bc808dcbfa23533dcbb30933c2debe
    volpack: 
        Source: (distgit) fedora
        NVR: volpack-1.0c7-29.fc39
        Commit/branch: 3d448b44f3cda5baa4a1ec6962be77b05dcf336a
    vtk: 
        Source: (distgit) fedora
        NVR: vtk-9.2.6-11.fc40
        Commit/branch: 669f31d92649c2adbd7695111003c9a5ff3cb352
    vxl: 
        Source: (distgit) fedora
        NVR: vxl-2.0.2-28.fc40
        Commit/branch: 1f24f8bc47645555fc19adc79bd45d32f580ea7d
    wagyu: 
        Source: (distgit) fedora
        NVR: wagyu-0.5.0-8.fc39
        Commit/branch: 1de31986bae7bc4d5915aa7aee6204db4274a94e
    wapanel: 
        Source: (distgit) fedora
        NVR: wapanel-1.1.0-5.fc39
        Commit/branch: 6154a20b847601dc5b328ed927bf1eda1cccf6e1
    warble: 
        Source: (distgit) fedora
        NVR: warble-1.5.0-3.fc39
        Commit/branch: 9ed8cf091d6f7531f5c67a6f13b662b023fe07b5
    warzone2100: 
        Source: (distgit) fedora
        NVR: warzone2100-4.3.5-4.fc40
        Commit/branch: 149939e4d98d24cd91e65d5469971ad859ea1330
    waybar: 
        Source: (distgit) fedora
        NVR: waybar-0.9.24-1.fc40
        Commit/branch: d92b15180d7f38d059a7859553bdfcb846175c3b
    wcslib: 
        Source: (distgit) fedora
        NVR: wcslib-7.12-3.fc39
        Commit/branch: bfcd4b9b02922b971bd793241bf80c08fd83a235
    wdune: 
        Source: (distgit) fedora
        NVR: wdune-1.958-12.fc39
        Commit/branch: 79e0a90ce231928ce6ee215b01d1e2e344a8de99
    wf-config: 
        Source: (distgit) fedora
        NVR: wf-config-0.8.0-1.fc40
        Commit/branch: a2547417bb2d59a0b2b33cbca6f4debfa9137401
    whereami: 
        Source: (distgit) fedora
        NVR: whereami-1.0-26.fc39
        Commit/branch: b80ed578af6603b6576c3252238d93da009f04d3
    wingpanel: 
        Source: (distgit) fedora
        NVR: wingpanel-3.0.5-3.20231206.git0fb4a14.fc40
        Commit/branch: b70893e3cd525b8b5ca538e3055f9cbe22995ef6
    wise2: 
        Source: (distgit) fedora
        NVR: wise2-2.4.1-23.fc40
        Commit/branch: e6979f69155efc3f71961260452c769134aa400f
    wlroots: 
        Source: (distgit) fedora
        NVR: wlroots-0.17.1-1.fc40
        Commit/branch: 99aa7067f08a3faa3bf05d729efc06f9a20a601e
    wlroots0.15: 
        Source: (distgit) fedora
        NVR: wlroots0.15-0.15.1-3.fc39
        Commit/branch: d343bcb83dece32ddd8c9f83ac580c6c9a8abb15
    wlroots0.16: 
        Source: (distgit) fedora
        NVR: wlroots0.16-0.16.2-1.fc40
        Commit/branch: abb0d1ae2a6e3f3cfb2296c240910b97a1a94cbb
    worminator: 
        Source: (distgit) fedora
        NVR: worminator-3.0R2.1-41.fc39
        Commit/branch: 5b17fc43c643f9690c61c43af20a77a498231b90
    wsjtx: 
        Source: (distgit) fedora
        NVR: wsjtx-2.6.1-2.fc39
        Commit/branch: 3f438e2ef4724f64f0828493bca4f043326c86be
    x11vnc: 
        Source: (distgit) fedora
        NVR: x11vnc-0.9.16^20230801gita61f364-1.fc40
        Commit/branch: e33ae8390e666fac1974068a50266722105201a0
    x2goclient: 
        Source: (distgit) fedora
        NVR: x2goclient-4.1.2.2-8.fc38
        Commit/branch: 3125ca8217932aaf55c2bba6a00dd4fcfe420cc9
    xawtv: 
        Source: (distgit) fedora
        NVR: xawtv-3.107-9.fc39
        Commit/branch: efb101deb2506134cd5ca8de109a414957917f91
    xbae: 
        Source: (distgit) fedora
        NVR: xbae-4.60.4-39.fc39
        Commit/branch: 5ce2245688d13cf1a3a46c0daa2e9e35980073e8
    xdg-desktop-portal-gnome: 
        Source: (distgit) fedora
        NVR: xdg-desktop-portal-gnome-45.1-1.fc40
        Commit/branch: f0285b5618c89098fa5f5f40b3b09215189ef4a9
    xdg-desktop-portal-hyprland: 
        Source: (distgit) fedora
        NVR: xdg-desktop-portal-hyprland-1.2.6-1.fc40
        Commit/branch: 9c391014034f49eab6fa64716c75bb8a604535d8
    xdp-tools: 
        Source: (distgit) fedora
        NVR: xdp-tools-1.4.1-1.fc40
        Commit/branch: 83b549d07dd21dfed7fb17e66b600ad065e2547f
    xfce4-notes-plugin: 
        Source: (distgit) fedora
        NVR: xfce4-notes-plugin-1.10.0-2.fc39
        Commit/branch: bc17a9e12c10a14d6f9c91de327b011e0513d09d
    xflr5: 
        Source: (distgit) fedora
        NVR: xflr5-6.47-13.fc39
        Commit/branch: df990f6e2f617423609a06b245f4eec0165ea6bc
    xfmpc: 
        Source: (distgit) fedora
        NVR: xfmpc-0.3.1-2.fc39
        Commit/branch: 17e98dbcda11098349fb11d4e140f0f5e6b05b04
    xkeycaps: 
        Source: (distgit) fedora
        NVR: xkeycaps-2.46-34.fc39
        Commit/branch: aad71e517492fcb9add12a8d0fa1510f2070e897
    xloadimage: 
        Source: (distgit) fedora
        NVR: xloadimage-4.1-36.fc39
        Commit/branch: 608932692efefdc58488b38424ac65998a69e1d8
    xlsfonts: 
        Source: (distgit) fedora
        NVR: xlsfonts-1.0.7-4.fc39
        Commit/branch: 4a12da0a2d5ce593a8905597ca9818052f65ee97
    xmakemol: 
        Source: (distgit) fedora
        NVR: xmakemol-5.16-17.fc39
        Commit/branch: a104f500460704976f6a2c1f1ba3d77e155777db
    xmlrpc-epi: 
        Source: (distgit) fedora
        NVR: xmlrpc-epi-0.54.2-18.fc39
        Commit/branch: 4e00ef8eaa24f42655c706ccf41bac1125c8ae6b
    xnnpack: 
        Source: (distgit) fedora
        NVR: xnnpack-0.0^git20221221.51a9875-2.fc40
        Commit/branch: d26548a40445825dd989ad4b7dc674003183efff
    xonotic: 
        Source: (distgit) fedora
        NVR: xonotic-0.8.6-2.fc39
        Commit/branch: 6e8d9ad79afd968e63d2b528d04e2ffa26c4d506
    xpilot-ng: 
        Source: (distgit) fedora
        NVR: xpilot-ng-4.7.3-29.fc39
        Commit/branch: c88d59c02d3653c019992f7fac2c48c3c95c6d7e
    xpra: 
        Source: (distgit) fedora
        NVR: xpra-5.0.3-1.fc40
        Commit/branch: dfa1606cde140e5362409f43cb966983625ad31d
    xtb: 
        Source: (distgit) fedora
        NVR: xtb-6.6.1-1.fc40
        Commit/branch: 1b5e84693063a65057264b54cb099b89441198ef
    yosyshq-abc: 
        Source: (distgit) fedora
        NVR: yosyshq-abc-0.35-1.20231108git896e5e7.fc40
        Commit/branch: 73f5b43213b8353abb8b5455d19efa0bd4098e0b
    z88dk: 
        Source: (distgit) fedora
        NVR: z88dk-1.10.1-28.20150709cvs.fc40
        Commit/branch: 8a807de4b68bb4a9046c3b9d73bf9fbe308a269c
    zabbix: 
        Source: (distgit) fedora
        NVR: zabbix-6.0.22-3.fc40
        Commit/branch: f7d2d5eae69125e185b2196ea51e599aa978e91e
    zbar: 
        Source: (distgit) fedora
        NVR: zbar-0.23.90-11.fc39
        Commit/branch: 8304c4ae98a7366ee455fe15987e53b4b566dd13
    zeitgeist: 
        Source: (distgit) fedora
        NVR: zeitgeist-1.0.4-11.fc40
        Commit/branch: 14a5e182aa18d9dad20f36cac2b51d28814b95d0
    zile: 
        Source: (distgit) fedora
        NVR: zile-2.6.2-3.fc39
        Commit/branch: d30c3ffb51c06fdd09a386b8b37fb692d8ff7bf3
    zipper: 
        Source: (distgit) fedora
        NVR: zipper-1.0.3-6.fc39
        Commit/branch: 3997046e657e0514e5ab2b3bc90b99848bd11e7d
    zulucrypt: 
        Source: (distgit) fedora
        NVR: zulucrypt-6.2.0-3.fc39
        Commit/branch: a06f874015df2c7b13088c89392cec8bea29619b
    zziplib: 
        Source: (distgit) fedora
        NVR: zziplib-0.13.72-5.fc39
        Commit/branch: 820f1b5c11d7caf913352021b0f6ba2cffc1dcb3

## List of packages with unknown status

    0ad: 
        Source: (distgit) fedora
        NVR: 0ad-0.0.26-14.fc40
        Commit/branch: f832f129b1a0cc5508223846fac14ae3fac9f41a
    389-ds-base: 
        Source: (distgit) fedora
        NVR: 389-ds-base-2.4.3-1.fc39
        Commit/branch: 298baaf1c040510d751e42e1690bf870e5459430
    AusweisApp2: 
        Source: (distgit) fedora
        NVR: AusweisApp2-2.0.1-3.fc40
        Commit/branch: f333be81e9c7f4b4bd55ae6c79bfa0d958c92232
    BibTool: 
        Source: (distgit) fedora
        NVR: BibTool-2.68-11.fc39
        Commit/branch: 84fda0fda26d9fdbea4a62d611a1053a1ace2b8c
    Io-language: 
        Source: (distgit) fedora
        NVR: Io-language-20170906-14.fc40
        Commit/branch: 8e10d76e30d2da55ee4836a2f67368a0ed22f0a5
    Macaulay2: 
        Source: (distgit) fedora
        NVR: Macaulay2-1.22-1.fc40
        Commit/branch: d2d3d3bb3cf63e8a3855a6c675fdcf99943e193d
    Mayavi: 
        Source: (distgit) fedora
        NVR: Mayavi-4.8.1-5.fc39
        Commit/branch: ced6ef586684bc22287d03bf61ac79c1469bbac6
    NetworkManager-iodine: 
        Source: (distgit) fedora
        NVR: NetworkManager-iodine-1.2.0-17.fc38
        Commit/branch: 833ed5c17567e4fad2506ed1c3c080df78f1bed7
    Singular: 
        Source: (distgit) fedora
        NVR: Singular-4.3.1p1-2.fc38
        Commit/branch: 965f5d38788a1ed05b0121375a5ff7f483803071
    aardvark-dns: 
        Source: (distgit) fedora
        NVR: aardvark-dns-1.9.0-1.fc40
        Commit/branch: 80c15942a93145cff426cc4a7efcdd544dd7dc3f
    abiword: 
        Source: (distgit) fedora
        NVR: abiword-3.0.5-9.fc40
        Commit/branch: 57149a875dec5c87fe20f25ee1d4394eeafbddbd
    aerc: 
        Source: (distgit) fedora
        NVR: aerc-0.15.2-2.fc39
        Commit/branch: d12a4b4c2f3b2df24d2c8c64c61b0e57fa2ac42e
    ahven: 
        Source: (distgit) fedora
        NVR: ahven-2.8-6.fc38
        Commit/branch: 60e1579f28062f05c615cd2d91ca7798ba89f7f0
    android-tools: 
        Source: (distgit) fedora
        NVR: android-tools-33.0.3p1-1.fc38
        Commit/branch: 9f8de37e28e955bbc5fb598634fc3649d564d3ab
    applet-window-buttons: 
        Source: (distgit) fedora
        NVR: applet-window-buttons-0.11.1-7.fc39
        Commit/branch: 0a0ef4cc24fa8dbad9e25a05ff26139bedef39b1
    astrometry: 
        Source: (distgit) fedora
        NVR: astrometry-0.94-1.fc40
        Commit/branch: ea1160fd22bb792d5b5c8115381c6f0dcd58b201
    audacity: 
        Source: (distgit) fedora
        NVR: audacity-3.3.3-1.fc40
        Commit/branch: ccdb110130c2d0b9c70913e57b798e2135be8323
    automoc: 
        Source: (distgit) fedora
        NVR: automoc-1.0-0.43.rc3.fc39
        Commit/branch: b77cf69944d0afbe563aeed773b6834d016d53a1
    bes: 
        Source: (distgit) fedora
        NVR: bes-3.20.13-11.fc40
        Commit/branch: 27fb0cee74e575c0b7fe7770fbac92cd476257d0
    biboumi: 
        Source: (distgit) fedora
        NVR: biboumi-9.0-3.fc38
        Commit/branch: 3f1bedee0230a231d083ba0ff03d66f4027eca61
    binaryen: 
        Source: (distgit) fedora
        NVR: binaryen-116-1.fc40
        Commit/branch: 1a9dd659e71e67025275c1711c0182139595db08
    bindfs: 
        Source: (distgit) fedora
        NVR: bindfs-1.17.4-2.fc40
        Commit/branch: f4851997c4c2045701fc451374c2b89269e82d01
    bismuth: 
        Source: (distgit) fedora
        NVR: bismuth-3.1.4-3.fc39
        Commit/branch: 8d44e47353abb1ca215152907854510d74aad759
    botan2: 
        Source: (distgit) fedora
        NVR: botan2-2.19.3-5.fc40
        Commit/branch: 0c8c3566170996da92b6dda16f8cefa712b5d209
    box64: 
        Source: (distgit) fedora
        NVR: box64-0.2.4-1.fc40
        Commit/branch: cc7636b870b9027048857da6b3ad0005e87ce733
    calligraplan: 
        Source: (distgit) fedora
        NVR: calligraplan-3.3.0-6.fc39
        Commit/branch: aa7b2e33e80c23d607f3728690b856b9bdb42d02
    castget: 
        Source: (distgit) fedora
        NVR: castget-2.0.1-9.fc39
        Commit/branch: 90bef8df66f03a01fcc57e6839ce3251a61272a2
    cataclysm-dda: 
        Source: (distgit) fedora
        NVR: cataclysm-dda-0.G-1.fc39
        Commit/branch: 41ad99eda7ff19e0a36afbbc6d388a2fcd3ebc6c
    cbonsai: 
        Source: (distgit) fedora
        NVR: cbonsai-1.3.1-4.fc39
        Commit/branch: 19c8e2493232c1228369eeeda104747b37142f64
    ckb-next: 
        Source: (distgit) fedora
        NVR: ckb-next-0.6.0-2.fc39
        Commit/branch: 8b877bd7ce82f72901ad0b80611dc4058a3c9129
    cockpit: 
        Source: (distgit) fedora
        NVR: cockpit-307-1.fc40
        Commit/branch: a9bc186555a5d2361cb94ea5d22ad271c33398db
    coeurl: 
        Source: (distgit) fedora
        NVR: coeurl-0.3.0-5.fc39
        Commit/branch: 360aef670d7f5a5a668c24a8c5eb8163e939953f
    community-mysql: 
        Source: (distgit) fedora
        NVR: community-mysql-8.0.35-2.fc40
        Commit/branch: 42f0633aee313b9c6e61e246dfaafe6cf6dab916
    compiz-plugins-experimental: 
        Source: (distgit) fedora
        NVR: compiz-plugins-experimental-0.8.18-7.fc39
        Commit/branch: ca8597c1b740a086906a7aae644a5bb495d720f5
    cp2k: 
        Source: (distgit) fedora
        NVR: cp2k-2023.1-2.fc38
        Commit/branch: 4cddfdd81f6e4f9ac95e022f02faeff264d1fba5
    cppcodec: 
        Source: (distgit) fedora
        NVR: cppcodec-0.2-9.fc38
        Commit/branch: 2aed684b0b6d405fd0d501ada58dc05a32d5ad94
    curl: 
        Source: (distgit) fedora
        NVR: curl-8.5.0-1.fc40
        Commit/branch: 7d149f66f5cb4d9b9af3d383835889f3b7753a15
    cvc5: 
        Source: (distgit) fedora
        NVR: cvc5-1.0.8-1.fc40
        Commit/branch: fee791d29f4fd0ff5797a6e3f3681fc417d69278
    cxxtools: 
        Source: (distgit) fedora
        NVR: cxxtools-3.0-8.fc39
        Commit/branch: aed9ff4791073d549992e9ce01d456ff3299e252
    darktable: 
        Source: (distgit) fedora
        NVR: darktable-4.4.2-1.fc40
        Commit/branch: 837b3af23082219791911f631438b8806be38b87
    dateutils: 
        Source: (distgit) fedora
        NVR: dateutils-0.4.10-5.fc39
        Commit/branch: f17676dec98b9067f14f722058d6c979314adcc3
    dbus-parsec: 
        Source: (distgit) fedora
        NVR: dbus-parsec-0.4.0-5.fc39
        Commit/branch: bc736f2a4f2f4b70df14921861caf4cff901e67a
    ddnet: 
        Source: (distgit) fedora
        NVR: ddnet-17.4.2-1.fc40
        Commit/branch: b106397921bfd922510fa2e02ba7a887d76bdab8
    deepin-file-manager: 
        Source: (distgit) fedora
        NVR: deepin-file-manager-5.8.3-1.fc40
        Commit/branch: 8ce1291de55c7adf41deb0e342a8bed75168d034
    deepin-kwin: 
        Source: (distgit) fedora
        NVR: deepin-kwin-5.4.26-8.fc39
        Commit/branch: 6f2289099829bf5d0d465a558329fe3d4a9d05d2
    deepin-system-monitor: 
        Source: (distgit) fedora
        NVR: deepin-system-monitor-6.0.3-1.fc40
        Commit/branch: 5196f6048c1cd06d87946d52be114833e7baa8a8
    deepin-terminal: 
        Source: (distgit) fedora
        NVR: deepin-terminal-5.9.43-1.fc40
        Commit/branch: 2bd962602bff992a90d1c3f26b76759380d21101
    device-mapper-persistent-data: 
        Source: (distgit) fedora
        NVR: device-mapper-persistent-data-1.0.9-1.fc40
        Commit/branch: c950fe5f288e3f2bbb2cccb6f87b3379d8978286
    dino: 
        Source: (distgit) fedora
        NVR: dino-0.4.3-2.fc39
        Commit/branch: d451287856bf53cad9fc5673c672bdcf80c5880e
    dolphin-emu: 
        Source: (distgit) fedora
        NVR: dolphin-emu-5.0.20347-3.fc40
        Commit/branch: e86deb38e74b46e3868324602aea0d8c2f733de5
    dwgrep: 
        Source: (distgit) fedora
        NVR: dwgrep-0.4-15.fc38
        Commit/branch: 59c39fc16e439aa2cbd54ef3ad10c4ab72fa3798
    eegdev: 
        Source: (distgit) fedora
        NVR: eegdev-0.2-26.fc40
        Commit/branch: 510f8cebd7968fa0765347fa3914fdb5010e3c9d
    efax: 
        Source: (distgit) fedora
        NVR: efax-0.9a-41.001114.fc39
        Commit/branch: ca73667d82a5669649be14c369f05a0066a7a3b6
    efitools: 
        Source: (distgit) fedora
        NVR: efitools-1.9.2-9.fc38
        Commit/branch: 02f9b8d7b94013b92ea43855708925285724f1d7
    elfutils: 
        Source: (distgit) fedora
        NVR: elfutils-0.190-4.fc40
        Commit/branch: 0aecb034b0e5b4a81685b09aeb9d81a70b1a5ecc
    emacspeak: 
        Source: (distgit) fedora
        NVR: emacspeak-54.0-8.fc39
        Commit/branch: e216bd0c70fdd247c9ef1d12e3d6cf5fbead6930
    erlang-corba: 
        Source: (distgit) fedora
        NVR: erlang-corba-5.1-2.fc38
        Commit/branch: 9bd60e20e271e6fc496c136361952e454a42d14d
    espresso: 
        Source: (distgit) fedora
        NVR: espresso-4.2.1-5.fc39
        Commit/branch: 551561897f274d74d5fb200c4a128d4d5878c955
    eurephia: 
        Source: (distgit) fedora
        NVR: eurephia-1.1.1-11.fc39
        Commit/branch: d255987de315194a2f226e0a82b29dc289dba1dd
    fapolicy-analyzer: 
        Source: (distgit) fedora
        NVR: fapolicy-analyzer-1.2.1-1.fc40
        Commit/branch: 00a1232f065dd6eca7c55391ef386e7920302f36
    fatcat: 
        Source: (distgit) fedora
        NVR: fatcat-1.1.0-5.fc39
        Commit/branch: cc5e658b3306f3ca8e288d7c88a66c9ffc4db516
    fcitx5-lua: 
        Source: (distgit) fedora
        NVR: fcitx5-lua-5.0.11-1.fc40
        Commit/branch: 23ffbe1629d12b5f31bcbfd6fdac98262433b2a0
    fido-device-onboard: 
        Source: (distgit) fedora
        NVR: fido-device-onboard-0.4.12-3.fc40
        Commit/branch: 4c223c644c40a231134386e2abc76297a79a86f5
    flickcurl: 
        Source: (distgit) fedora
        NVR: flickcurl-1.26-21.fc39
        Commit/branch: 9e92e0b0dd27e8e3737f052d73d6fee3dbbc1985
    folly: 
        Source: (distgit) fedora
        NVR: folly-2023.10.16.00-1.fc40
        Commit/branch: 530556fb77e764b225af32c8a16d02b64421d952
    frama-c: 
        Source: (distgit) fedora
        NVR: frama-c-28.0-1.fc40
        Commit/branch: 0ddaac75d64bf4481bab3977c742757767857b4d
    freefem++: 
        Source: (distgit) fedora
        NVR: freefem++-4.14-2.fc40
        Commit/branch: d774b8d83f16f02979f586867f551a7198ac9c07
    fwupd: 
        Source: (distgit) fedora
        NVR: fwupd-1.9.10-1.fc40
        Commit/branch: 0ea004ebdc47157e2bf26d6b904ce60ac1756921
    fwupd-efi: 
        Source: (distgit) fedora
        NVR: fwupd-efi-1.4-1.fc38
        Commit/branch: 217e8cb3300824dbc47554791e221bf330fed524
    galera: 
        Source: (distgit) fedora
        NVR: galera-26.4.16-1.fc40
        Commit/branch: b874ebf0702c4117c3407a59fe96175ead02caaa
    gamerzillagobj: 
        Source: (distgit) fedora
        NVR: gamerzillagobj-0.1.2-1.fc40
        Commit/branch: a1e311ef883aa5fd46f52d181ba5d2c01a4cf2a8
    gap-pkg-semigroups: 
        Source: (distgit) fedora
        NVR: gap-pkg-semigroups-5.3.2-1.fc40
        Commit/branch: 9246c3f2fd1cbab10cfc88fb1a8bf0590503fd42
    gcl: 
        Source: (distgit) fedora
        NVR: gcl-2.6.14-3.fc39
        Commit/branch: 8c4f4f87365c82d94c9bba2f2199fae0a10e8f6e
    ghc9.0: 
        Source: (distgit) fedora
        NVR: ghc9.0-9.0.2-14.fc39
        Commit/branch: 5a938310b0d2c95d8817dfaeac49c2e6df9587cf
    ghc9.2: 
        Source: (distgit) fedora
        NVR: ghc9.2-9.2.8-21.fc39
        Commit/branch: 5fa6c4c7d6b70e275334e711b6c3af71e1735c21
    ghdl: 
        Source: (distgit) fedora
        NVR: ghdl-0.38~dev-16.20201208git83dfd49.fc37
        Commit/branch: 12452ed634135cc621ff387a3bc4b590c5b5b345
    git: 
        Source: (distgit) fedora
        NVR: git-2.43.0-1.fc40
        Commit/branch: f2b3ab26cc456c3905b9b19725937f0faa281d6c
    glibd: 
        Source: (distgit) fedora
        NVR: glibd-2.3.0-10.fc39
        Commit/branch: f5e79988f30a68e224047e909a962d1a4c893b10
    global: 
        Source: (distgit) fedora
        NVR: global-6.6.5-9.fc39
        Commit/branch: 238b1020a4c02c178de71fecbe52ffccfdfeaed4
    gmic: 
        Source: (distgit) fedora
        NVR: gmic-3.2.6-3.fc39
        Commit/branch: a7e77a6bd10546f8e2d530c3d517bd786d1703d1
    gnucobol: 
        Source: (distgit) fedora
        NVR: gnucobol-3.2-2.fc40
        Commit/branch: 3d72265a41b12e827446c23a55a8f567d62877dc
    gpscorrelate: 
        Source: (distgit) fedora
        NVR: gpscorrelate-2.0-8.fc39
        Commit/branch: 576c552a1ee9d4a0499416be20fbe4a6ae76f38a
    grantlee: 
        Source: (distgit) fedora
        NVR: grantlee-0.5.1-24.fc39
        Commit/branch: b4aaa8a7ba82d957af07ee2b109d8fc07bcaaa0b
    gsl-lite: 
        Source: (distgit) fedora
        NVR: gsl-lite-0.41.0-2.fc39
        Commit/branch: f26eb31a72998733674e54a168a3234475218f8b
    gtk-doc: 
        Source: (distgit) fedora
        NVR: gtk-doc-1.33.2-9.fc39
        Commit/branch: ab6bf729d864621a08b5c360c7e809603af3979a
    gtkpod: 
        Source: (distgit) fedora
        NVR: gtkpod-2.1.5-25.fc39
        Commit/branch: 1bdd0c6cafedb74aec63c4285f308a1fe97ddf1c
    guile-gnutls: 
        Source: (distgit) fedora
        NVR: guile-gnutls-3.7.11-1.fc39
        Commit/branch: 0c93be24f7238e5d07efe1b61ac15f598fe23b36
    guile-reader: 
        Source: (distgit) fedora
        NVR: guile-reader-0.6.3-1.fc40
        Commit/branch: b1bacf474f3e0c008398d245dcb091304a8508a5
    guvcview: 
        Source: (distgit) fedora
        NVR: guvcview-2.0.8-6.fc39
        Commit/branch: a81dd2669d28d15c0349df33da6dd66813108faf
    hdf5: 
        Source: (distgit) fedora
        NVR: hdf5-1.12.1-15.fc40
        Commit/branch: 75dcb5c8d3c3ca3f1435741c3b4848129b3b4d5d
    hedgewars: 
        Source: (distgit) fedora
        NVR: hedgewars-1.0.2-5.fc39
        Commit/branch: b2f30a0a3bc4a2057c9b841012686b8170c4cea3
    hydra: 
        Source: (distgit) fedora
        NVR: hydra-9.5-3.fc39
        Commit/branch: 9a56e3a7e2cccc8a5012fcf67b67b28a62b065af
    inchi: 
        Source: (distgit) fedora
        NVR: inchi-1.0.6-7.fc39
        Commit/branch: 26c0145bd960c7ec437ae411246b24a5c15d3a29
    iwyu: 
        Source: (distgit) fedora
        NVR: iwyu-0.20-3.fc39
        Commit/branch: 9aa9202727c0b71d7e724fc03bc2b6404602f114
    j4-dmenu-desktop: 
        Source: (distgit) fedora
        NVR: j4-dmenu-desktop-2.18-7.fc37
        Commit/branch: 9c7acc154a9e8013fd9d70a8faec0eec8fabe36c
    jack_capture: 
        Source: (distgit) fedora
        NVR: jack_capture-0.9.73-14.fc38
        Commit/branch: 365c3c58743d565dc3be18a4ad88489e83afda81
    jaero: 
        Source: (distgit) fedora
        NVR: jaero-1.0.4.11-10.fc39
        Commit/branch: 8632f98ea029677612953b1ae5cba613834c867d
    java-1.8.0-openjdk: 
        Source: (distgit) fedora
        NVR: java-1.8.0-openjdk-1.8.0.392.b08-6.fc40
        Commit/branch: 848d4c77d0c365b71ff17882f7f1638ee8a190a8
    jose: 
        Source: (distgit) fedora
        NVR: jose-11-8.fc39
        Commit/branch: dc1ce14ffd8c2be842ff2ef0e8cdb3ac0b8e1bd8
    jreen: 
        Source: (distgit) fedora
        NVR: jreen-1.2.1-22.fc37
        Commit/branch: 6caa95e7bf452d4c7434d3c7589bf67cd058eed9
    k4dirstat: 
        Source: (distgit) fedora
        NVR: k4dirstat-3.2.0-7.fc39
        Commit/branch: f0b9492f82bce5a6c04ad8d44f3762e6cbfa5885
    kalendar: 
        Source: (distgit) fedora
        NVR: kalendar-23.04.3-2.fc39
        Commit/branch: 28bed0970b47ec20e61e42624cf6cde79d68680e
    kalzium: 
        Source: (distgit) fedora
        NVR: kalzium-24.01.85-1.fc40
        Commit/branch: c1ad44a9529fc0cd12ef5363b05a35e10bb5602e
    kanagram: 
        Source: (distgit) fedora
        NVR: kanagram-24.01.85-1.fc40
        Commit/branch: 68fc0e90cc89051ab9d300cd437ca9a81a88311c
    kbilliards: 
        Source: (distgit) fedora
        NVR: kbilliards-0.8.7b-42.fc39
        Commit/branch: 844e265821354a44f2b6b4af970cd0d221a71a96
    kcm_systemd: 
        Source: (distgit) fedora
        NVR: kcm_systemd-1.2.1-22.fc38
        Commit/branch: 4b2a10384f0f81bcc476a27e102bff90e793d155
    kdb: 
        Source: (distgit) fedora
        NVR: kdb-3.2.0-16.fc39
        Commit/branch: 8157091ab76a57c3aadf211a281a48a99aea22a2
    kdevelop-pg-qt: 
        Source: (distgit) fedora
        NVR: kdevelop-pg-qt-2.2.1-7.fc37
        Commit/branch: 4933d2f1ebcc36ae315a9e9007f120570ab534ca
    kdewebdev: 
        Source: (distgit) fedora
        NVR: kdewebdev-3.5.10-52.fc39
        Commit/branch: af3aea9833a5ccf4e73975df4a0f41796d6879e4
    keysmith: 
        Source: (distgit) fedora
        NVR: keysmith-24.01.85-1.fc40
        Commit/branch: bb604a74f80793adb618b1b95fdce9082d1de20d
    kf5-kmailtransport: 
        Source: (distgit) fedora
        NVR: kf5-kmailtransport-23.08.2-1.fc40
        Commit/branch: ee6d7ed8e4c18c51298a06e2996c67dcd90c0538
    kf6-kcmutils: 
        Source: (distgit) fedora
        NVR: kf6-kcmutils-5.247.0-1.fc40
        Commit/branch: 51cf13b9d31adb1c2e431c5b83312f68e641965d
    kf6-kio: 
        Source: (distgit) fedora
        NVR: kf6-kio-5.247.0-1.fc40
        Commit/branch: fca3652e3e263b27c81f944fc1cd5ac34c698adb
    kf6-krunner: 
        Source: (distgit) fedora
        NVR: kf6-krunner-5.247.0-1.fc40
        Commit/branch: a9bedc1af82c4cec2bb096749efca8e6afb6c43c
    kf6-kuserfeedback: 
        Source: (distgit) fedora
        NVR: kf6-kuserfeedback-5.247.0-1.fc40
        Commit/branch: 0b956d43358226912c67b05100dbfd146856ce72
    kfloppy: 
        Source: (distgit) fedora
        NVR: kfloppy-23.04.3-2.fc39
        Commit/branch: 5985fbea1cd1070f536b68b5edcf32d93688e8f2
    kid3: 
        Source: (distgit) fedora
        NVR: kid3-3.9.4-2.fc39
        Commit/branch: 80e51e279d12f4e44519bd7d6683cee2b47d1562
    kirigami-gallery: 
        Source: (distgit) fedora
        NVR: kirigami-gallery-23.08.2-1.fc40
        Commit/branch: 674aeaa4b5b564f5107255edf5abc708cc61f61a
    kitty: 
        Source: (distgit) fedora
        NVR: kitty-0.31.0-1.fc40
        Commit/branch: 112536ea54fbb88eabb09701b25b356ad0245a2b
    knemo: 
        Source: (distgit) fedora
        NVR: knemo-0.7.7-22.20170520git.fc39
        Commit/branch: 2811ca6eec89926fe0482770cf5df7c6511725a0
    kommit: 
        Source: (distgit) fedora
        NVR: kommit-1.3.0-1.fc40
        Commit/branch: 397f5db218853c13e0424a3d3c9d016b9ca3cd7e
    kpipewire: 
        Source: (distgit) fedora
        NVR: kpipewire-5.91.0-1.fc40
        Commit/branch: 8520f9cdeeb16da66e567d1a596f0bc8dd742b17
    kpkpass: 
        Source: (distgit) fedora
        NVR: kpkpass-24.01.85-1.fc40
        Commit/branch: b1eb0ec8e0f2110b5a9cc6f7ed2f24a586f7ffd6
    kproperty: 
        Source: (distgit) fedora
        NVR: kproperty-3.2.0-8.fc39
        Commit/branch: a5a7dcf861b0e3243223f792adfceb7ae1ff0603
    krecorder: 
        Source: (distgit) fedora
        NVR: krecorder-23.08.2-1.fc40
        Commit/branch: b5bf712d334d4e1cbcb3906c760604d862187583
    kreport: 
        Source: (distgit) fedora
        NVR: kreport-3.2.0-12.fc39
        Commit/branch: 05022adcfbce5637a882726c9a1e57db820d8177
    ksensors: 
        Source: (distgit) fedora
        NVR: ksensors-0.7.3-51.fc38
        Commit/branch: 4f18496fa215abcf407cd9b15c84a56f6c94fa2e
    ksysguard: 
        Source: (distgit) fedora
        NVR: ksysguard-5.22.0-10.fc39
        Commit/branch: cc92de59c82f6ec5492aa78c9bafe05f30518f83
    ktrip: 
        Source: (distgit) fedora
        NVR: ktrip-24.01.85-1.fc40
        Commit/branch: d5b9a0e05d4eb0dd67130a43ef0df9f232c88c40
    kwrited: 
        Source: (distgit) fedora
        NVR: kwrited-5.91.0-1.fc40
        Commit/branch: 5afc4d887c62cb1329f97aa5935a9e7a83ca9c86
    labwc: 
        Source: (distgit) fedora
        NVR: labwc-0.6.6-1.fc40
        Commit/branch: b9791736e479bfdb77b0a38212d993eda03b6792
    lammps: 
        Source: (distgit) fedora
        NVR: lammps-20220623.4-4.fc39
        Commit/branch: d1d15451dd5aed2b8dc5ceb361eae75d9aefac65
    libffi: 
        Source: (distgit) fedora
        NVR: libffi-3.4.4-5.fc40
        Commit/branch: 586a259cea7f24bd5b35fb32bd47218a5528bc6b
    libgit2: 
        Source: (distgit) fedora
        NVR: libgit2-1.7.1-2.fc40
        Commit/branch: 26de4b0cba47571f39be64191030991de5d77234
    libgit2_1.5: 
        Source: (distgit) fedora
        NVR: libgit2_1.5-1.5.2-3.fc39
        Commit/branch: 9a200063de1f51d74c64e2ea5ad46da8450d6238
    libgit2_1.6: 
        Source: (distgit) fedora
        NVR: libgit2_1.6-1.6.4-1.fc40
        Commit/branch: 25d17a39c31cec8df34432f7f4228d99f96e0c62
    libgsystem: 
        Source: (distgit) fedora
        NVR: libgsystem-2015.2-17.fc38
        Commit/branch: 8d605f656ded730a66351010af890689a5b2838b
    libkscreen: 
        Source: (distgit) fedora
        NVR: libkscreen-5.91.0-1.fc40
        Commit/branch: 5439f898bbe4e53cf3fa203f98522562a9def2d1
    libksysguard: 
        Source: (distgit) fedora
        NVR: libksysguard-5.91.0-1.fc40
        Commit/branch: db0924426181237c5494589b9eeb4cdbfcead7f9
    liblouisutdml: 
        Source: (distgit) fedora
        NVR: liblouisutdml-2.11.0-5.fc39
        Commit/branch: 07b7cffe491de9b486dac1c45fd3e19bf73137ad
    libmusicbrainz5: 
        Source: (distgit) fedora
        NVR: libmusicbrainz5-5.1.0-22.fc39
        Commit/branch: b1c77e6acff02c813401de35a664fac04473433a
    libpal: 
        Source: (distgit) fedora
        NVR: libpal-0.9.8-7.fc39
        Commit/branch: e5695550096fc2ec72a08296ad134b8a759c0842
    libratbag: 
        Source: (distgit) fedora
        NVR: libratbag-0.17-2.fc38
        Commit/branch: 4d1269fecaa520a092f91893e6848f1da3817820
    libreoffice: 
        Source: (distgit) fedora
        NVR: libreoffice-7.6.3.1-3.fc40
        Commit/branch: 4d5b9e4cdd148cf6532c0856391fafd4b41184ef
    libreport: 
        Source: (distgit) fedora
        NVR: libreport-2.17.11-3.fc39
        Commit/branch: d1f5aea9e358aadc68aaa2de63233770be685540
    libsbml: 
        Source: (distgit) fedora
        NVR: libsbml-5.20.2-4.fc40
        Commit/branch: a7c9879d1470be3f2abef67561dce523ac53e28c
    libshumate: 
        Source: (distgit) fedora
        NVR: libshumate-1.1.0-1.fc40
        Commit/branch: 7f22457adbb07935a4709df5b5915dec07e2c823
    libsrtp: 
        Source: (distgit) fedora
        NVR: libsrtp-2.3.0-12.fc39
        Commit/branch: 22cb23cb35fca1efd39f9dbe4df565bdbe8765cb
    libtpcmisc: 
        Source: (distgit) fedora
        NVR: libtpcmisc-1.4.8-30.fc38
        Commit/branch: c9ab1af568afe2959170652545b5ddb6e090a9af
    libucl: 
        Source: (distgit) fedora
        NVR: libucl-0.8.2-3.fc39
        Commit/branch: 1ea887cec1862a4ae7dd0cc48945c5a28b979124
    libunicap: 
        Source: (distgit) fedora
        NVR: libunicap-0.9.12-32.fc38
        Commit/branch: c4cb1ea32ab6d93533fc8cdb47fcd1848f40fa5a
    libunicapgtk: 
        Source: (distgit) fedora
        NVR: libunicapgtk-0.9.8-29.fc38
        Commit/branch: 98994a34d80111c5967ca9d5cc33ce9cad38d1e8
    libva-v4l2-request: 
        Source: (distgit) fedora
        NVR: libva-v4l2-request-1.0.0-7.20190517gita3c2476.fc37
        Commit/branch: 1444d4ccc26d7dd243cc328ea9b9438bdf91cfb7
    libvirt-cim: 
        Source: (distgit) fedora
        NVR: libvirt-cim-0.6.3-21.fc39
        Commit/branch: bcc973b764a19e3b5cff31fcfa588f0dd0bc6c24
    libx86emu: 
        Source: (distgit) fedora
        NVR: libx86emu-3.5-4.fc39
        Commit/branch: 0f3fdab46cfb4077572f90c9b337fa5090e1e70e
    libzypp: 
        Source: (distgit) fedora
        NVR: libzypp-17.31.8-2.fc39
        Commit/branch: f7b7ee67518828443477794c30c35d6c555df3bc
    liferea: 
        Source: (distgit) fedora
        NVR: liferea-1.15.4-2.fc40
        Commit/branch: ca5aeae5f8e7f22babfb429473b1eb5bd0a1c1fc
    lightly: 
        Source: (distgit) fedora
        NVR: lightly-0.4.1-6.fc39
        Commit/branch: c27d0d3282b0fb551a9b016ba07671592b307092
    lxc: 
        Source: (distgit) fedora
        NVR: lxc-4.0.12-2.fc37
        Commit/branch: 2f4eed52c2de12e464053f62d667e755241b196e
    lzma-sdk: 
        Source: (distgit) fedora
        NVR: lzma-sdk-22.01-1.fc40
        Commit/branch: eb8fb78a909c27a489b0625634eae3afb8d4e8e6
    mangohud: 
        Source: (distgit) fedora
        NVR: mangohud-0.7.0-6.fc40
        Commit/branch: e3a6b95ba376766c8393559dea6fdde2045183e5
    mapnik: 
        Source: (distgit) fedora
        NVR: mapnik-3.1.0-29.fc40
        Commit/branch: 59bf1478d7158f266121170e94d3c8d873f0b2d1
    megapixels: 
        Source: (distgit) fedora
        NVR: megapixels-1.7.0-1.fc39
        Commit/branch: 67f5a994ee416625160561e3aa41563e99359855
    miller: 
        Source: (distgit) fedora
        NVR: miller-5.10.2-5.fc39
        Commit/branch: a87661aca28faec903baabc8c48ac4fb0cd53b14
    mingw-numpy: 
        Source: (distgit) fedora
        NVR: mingw-numpy-1.24.4-1.fc39
        Commit/branch: 153ca882f059d97e6b54d463bbb537dbb0155298
    molsketch: 
        Source: (distgit) fedora
        NVR: molsketch-0.8.0-2.fc39
        Commit/branch: bb3e435fe62fbec5e40e16256eea5e0d75a228d7
    mstflint: 
        Source: (distgit) fedora
        NVR: mstflint-4.24.0-3.fc39
        Commit/branch: 2f2ee24fbdff334a86e5b73139773f8a69b56223
    nest: 
        Source: (distgit) fedora
        NVR: nest-3.4-1.fc39
        Commit/branch: ef611010765a7e3e37911e3f42d678968e882510
    netavark: 
        Source: (distgit) fedora
        NVR: netavark-1.9.0-1.fc40
        Commit/branch: ed427e38c5028166a578398d29919bf0e02c201d
    neverball: 
        Source: (distgit) fedora
        NVR: neverball-1.6.0-29.fc39
        Commit/branch: 644f3d658aa30a3c797a6f58ac052a84d93b5e5d
    new-session-manager: 
        Source: (distgit) fedora
        NVR: new-session-manager-1.3.2-8.fc38
        Commit/branch: 4f90bcdefd333a1d98eccb659af8cac21cd325fe
    nginx-mod-modsecurity: 
        Source: (distgit) fedora
        NVR: nginx-mod-modsecurity-1.0.3-4.fc39
        Commit/branch: ed0dafdc486fe1fefe7228a4270c88ebc03796c6
    nheko: 
        Source: (distgit) fedora
        NVR: nheko-0.11.3-5.fc39
        Commit/branch: d6593df8bc93abd21bbff1dfb836b95ad69d3bc6
    non-ntk: 
        Source: (distgit) fedora
        NVR: non-ntk-1.3.1000-0.9.20190925gitdae1771.fc38
        Commit/branch: d21411befbd1bebbbd8551b7c2ef60e054d1783c
    obconf-qt: 
        Source: (distgit) fedora
        NVR: obconf-qt-0.16.2-4.fc39
        Commit/branch: 2b82d64a5672a5d7de78564f5fe3b1ae3460913c
    octave-miscellaneous: 
        Source: (distgit) fedora
        NVR: octave-miscellaneous-1.3.0-12.fc39
        Commit/branch: a529b6e472884336a1367fc06f7b7a5f3db24d2b
    octave-odepkg: 
        Source: (distgit) fedora
        NVR: octave-odepkg-0.9.1-0.21.20210827hg611.fc38
        Commit/branch: 0e54e3ec24578ca716ecf8d33df18e4ffcbeddde
    ofono: 
        Source: (distgit) fedora
        NVR: ofono-1.34-2.fc37
        Commit/branch: c511c3a7b4ee338839c5bf34bc9d227291cd7233
    openbios: 
        Source: (distgit) fedora
        NVR: openbios-20230126-1.gitaf97fd7.fc40
        Commit/branch: ef1c3398b8439b4af5b2ff72091b5df7a0ed9a1d
    openclonk: 
        Source: (distgit) fedora
        NVR: openclonk-8.1-22.20210103git701bcf3.fc39
        Commit/branch: 458dd3360a078d6d2688dee93825308731161860
    opendnssec: 
        Source: (distgit) fedora
        NVR: opendnssec-2.1.10-7.fc39
        Commit/branch: c919b74c3bab14cc893081a0bc8026bcf08f27e3
    openlierox: 
        Source: (distgit) fedora
        NVR: openlierox-0.58-0.30.rc5.fc39
        Commit/branch: bf6cb1265d2a0a6b97a8f96d6c63736070337576
    openms: 
        Source: (distgit) fedora
        NVR: openms-3.1.0-0.4.pre1.fc40
        Commit/branch: 13fb611516a436d58c03ccd142aa040bd454560b
    opentoonz: 
        Source: (distgit) fedora
        NVR: opentoonz-1.7.1-4.fc39
        Commit/branch: a45e3e27ee9102f4619d716c356b77cd012775a4
    osinfo-db-tools: 
        Source: (distgit) fedora
        NVR: osinfo-db-tools-1.11.0-2.fc40
        Commit/branch: 14e94b1b57aaa7e960099bdfcc6c29efd52c32cc
    osmium-tool: 
        Source: (distgit) fedora
        NVR: osmium-tool-1.16.0-1.fc40
        Commit/branch: 1841af4488eaec0036621b65f00fa354ad40debc
    p7zip: 
        Source: (distgit) fedora
        NVR: p7zip-16.02-26.fc39
        Commit/branch: 2e0d8c51858eaaf525498bb55b916c0219d9ee97
    parsec: 
        Source: (distgit) fedora
        NVR: parsec-1.1.0-5.fc39
        Commit/branch: 5ec17d2c22a3b34f7f6b47843474bfa7618c3c51
    parsec-tool: 
        Source: (distgit) fedora
        NVR: parsec-tool-0.4.0-3.fc39
        Commit/branch: 129ace9e9b4399ff30dc6d7e86df993855b6c61e
    perl: 
        Source: (distgit) fedora
        NVR: perl-5.38.2-503.fc40
        Commit/branch: b26fbaf78a425b0e5db3701c55e29bbc7e4b146d
    perl-Authen-Krb5: 
        Source: (distgit) fedora
        Commit/branch: 403755f9b21ac2e4069df4a6b24b1efe97047c61
    perl-Image-PNG-Libpng: 
        Source: (distgit) fedora
        NVR: perl-Image-PNG-Libpng-0.57-10.fc39
        Commit/branch: 37568bcf5f60af789f4c7c46d44c75a2f78c205b
    perl-XML-DifferenceMarkup: 
        Source: (distgit) fedora
        NVR: perl-XML-DifferenceMarkup-1.05-33.fc39
        Commit/branch: bd03fca99590d995bebc2a1dafe9f9e1c06c8c86
    perl-gettext: 
        Source: (distgit) fedora
        NVR: perl-gettext-1.07-28.fc39
        Commit/branch: a3c5e18be18a4350f291655105729df9e852f6bb
    pg_auto_failover: 
        Source: (distgit) fedora
        NVR: pg_auto_failover-2.1-1.fc40
        Commit/branch: 24cfc6cc51fbd16fd5bf587777f444fec8228d30
    pgmodeler: 
        Source: (distgit) fedora
        NVR: pgmodeler-1.0.4-2.fc39
        Commit/branch: c791c98d1876820933bf0e29be5fa2132c2a26f3
    phonon-backend-gstreamer: 
        Source: (distgit) fedora
        NVR: phonon-backend-gstreamer-4.10.0-10.fc39
        Commit/branch: 0964fe7b7ee20cfd970cd81ac37b91b2671e252a
    phonon-qt4: 
        Source: (distgit) fedora
        NVR: phonon-qt4-4.10.3-20.fc39
        Commit/branch: 5464b049110cbe6b20b71a8f3182f2a5675edec6
    phonon-qt4-backend-gstreamer: 
        Source: (distgit) fedora
        NVR: phonon-qt4-backend-gstreamer-4.9.1-19.fc39
        Commit/branch: b9e0c38659a03fd65b10809213418d44790c4a57
    php-pecl-xmlrpc: 
        Source: (distgit) fedora
        NVR: php-pecl-xmlrpc-1.0.0~rc3-8.fc40
        Commit/branch: 150c95e761af13a352fb354a3e4bf38b36e10dcd
    pidgin-sipe: 
        Source: (distgit) fedora
        NVR: pidgin-sipe-1.25.0-17.fc39
        Commit/branch: f402f45602a31018d1ce0c15aaab789e50967ea2
    pkcs11-provider: 
        Source: (distgit) fedora
        NVR: pkcs11-provider-0.2-2.fc39
        Commit/branch: 744c82e95326789901e213671d27f1b61f9b1c8a
    pl: 
        Source: (distgit) fedora
        NVR: pl-9.0.4-3.fc39
        Commit/branch: d194cc2eb5ea7221da70ef6d63bfda5aa623df32
    plasma-bigscreen: 
        Source: (distgit) fedora
        NVR: plasma-bigscreen-5.27.9-1.fc40
        Commit/branch: 2427ba394adacda78c350cb6fca39ae1d8ea1592
    plasma-dialer: 
        Source: (distgit) fedora
        NVR: plasma-dialer-23.01.0-5.fc40
        Commit/branch: 44813ecd840b1a7d5a3979bdaab9b4870a0e989d
    plasma-discover: 
        Source: (distgit) fedora
        NVR: plasma-discover-5.91.0-2.fc40
        Commit/branch: 91b5a0ae712dd18ff37eacac4862e74cde94ce7e
    plasma-firewall: 
        Source: (distgit) fedora
        NVR: plasma-firewall-5.91.0-1.fc40
        Commit/branch: 606b1faf9407283a7a93894d9145270cc9bed8c8
    plasma-nano: 
        Source: (distgit) fedora
        NVR: plasma-nano-5.91.0-1.fc40
        Commit/branch: 10380388db739098eee6afb17b0c5baf69a24c3f
    plasma-wallpapers-dynamic: 
        Source: (distgit) fedora
        NVR: plasma-wallpapers-dynamic-4.4.0-10.fc39
        Commit/branch: 8d20a6842a16b0c14f6fcfb86a64bc4b482a28e7
    plasma-welcome: 
        Source: (distgit) fedora
        NVR: plasma-welcome-5.91.0-1.fc40
        Commit/branch: 286ae74f97bf5c4b5881d19d84a674d1b1b87db4
    pngquant: 
        Source: (distgit) fedora
        NVR: pngquant-2.18.0-1.fc38
        Commit/branch: fb4b680b6f6e040635cf928775165ed645d7ef6d
    pocl: 
        Source: (distgit) fedora
        NVR: pocl-4.0-3.fc39
        Commit/branch: fd67288a3dde7952854860562d0326af039a3946
    postgresql-odbc: 
        Source: (distgit) fedora
        NVR: postgresql-odbc-13.01.0000-5.fc39
        Commit/branch: e25b43c47e16110bd8dfc9c34dd63129c679bbb7
    postgresql15: 
        Source: (distgit) fedora
        NVR: postgresql15-15.5-1.fc40
        Commit/branch: acf2e909606fb16a00ce2854feb4c3fcd78c9f1e
    postgresql16: 
        Source: (distgit) fedora
        NVR: postgresql16-16.1-1.fc40
        Commit/branch: 346ba9791a5e13fb713654a06d11b7cf21183f2c
    pptpd: 
        Source: (distgit) fedora
        NVR: pptpd-1.4.0-33.fc39
        Commit/branch: e03cbb6910f9ec19df8ae6bbb022f90db7acf743
    prelude-manager: 
        Source: (distgit) fedora
        NVR: prelude-manager-5.2.0-10.fc39
        Commit/branch: 204d53c97a7cd677102a39b5d4acfa9289d638d5
    proxygen: 
        Source: (distgit) fedora
        NVR: proxygen-2023.10.16.00-1.fc40
        Commit/branch: 3f95394d8ce3db3fc68a34ba5f4c7697ff3cf772
    prusa-slicer: 
        Source: (distgit) fedora
        NVR: prusa-slicer-2.4.2-11.fc40
        Commit/branch: a588603a376e011d3d2e22ccd5dde969e9a391bb
    psi-plus: 
        Source: (distgit) fedora
        NVR: psi-plus-1.5.1650-2.fc39
        Commit/branch: c709d758425b346825da65e7822ca032de782121
    pspp: 
        Source: (distgit) fedora
        NVR: pspp-1.6.2-8.fc40
        Commit/branch: f914ed5df585b5f9558576c733b9ff8d09cf0bff
    pthsem: 
        Source: (distgit) fedora
        NVR: pthsem-2.0.7-25.fc37
        Commit/branch: 897dbe9934893dd11d8d216a39d9f3ad79c6b419
    pygsl: 
        Source: (distgit) fedora
        NVR: pygsl-2.3.3-1.fc40
        Commit/branch: f9a5e70a5983accc461aff2724e9f9759b49d35b
    python-Traits: 
        Source: (distgit) fedora
        NVR: python-Traits-6.4.2-1.fc40
        Commit/branch: 6f31e4bc1b271188869de5e3a9ff7daf91e7e15a
    python-aiohttp: 
        Source: (distgit) fedora
        NVR: python-aiohttp-3.9.1-1.fc40
        Commit/branch: 4cf3da2c93e0a016c8cd2baeeb69258329474d00
    python-apsw: 
        Source: (distgit) fedora
        NVR: python-apsw-3.43.1.0-1.fc40
        Commit/branch: f82c9edbee35918544cec19444fbd13e9b72d92d
    python-astropy: 
        Source: (distgit) fedora
        NVR: python-astropy-5.3.2-2.fc40
        Commit/branch: 5adbb6a6d1161f2b7bdc8e67cf8b5ea6e1ecfcf7
    python-astroscrappy: 
        Source: (distgit) fedora
        NVR: python-astroscrappy-1.1.0-7.fc39
        Commit/branch: 7cd2c433a97764e557b86c1b267898700f19203f
    python-cysignals: 
        Source: (distgit) fedora
        NVR: python-cysignals-1.11.2-6.fc39
        Commit/branch: ae06b0bffc232b2269fcba854d58edf1d72db2ce
    python-djvulibre: 
        Source: (distgit) fedora
        NVR: python-djvulibre-0.8.7-4.fc39
        Commit/branch: 598d71a73c8d5df8d9770e56c9bb9590f21881f8
    python-elephant: 
        Source: (distgit) fedora
        NVR: python-elephant-0.12.0-4.fc39
        Commit/branch: f491f7385ddc7234fb1799192038067baf3b5fe5
    python-fastavro: 
        Source: (distgit) fedora
        NVR: python-fastavro-1.8.4-3.fc40
        Commit/branch: b1e63cb028902feb453ad992d3c7086d333b4ad1
    python-gmpy2: 
        Source: (distgit) fedora
        NVR: python-gmpy2-2.1.5-4.fc39
        Commit/branch: ac6437c5c6efdf5a642ac01b34829c46ccfbdaac
    python-html5-parser: 
        Source: (distgit) fedora
        NVR: python-html5-parser-0.4.10-7.fc39
        Commit/branch: 27d69845504b77cbe37f66bce0f4722ba75c6286
    python-imagecodecs: 
        Source: (distgit) fedora
        NVR: python-imagecodecs-2022.9.26-1.fc39
        Commit/branch: 249dc27019f4992553225ecb6bf4bf634b4ea361
    python-intbitset: 
        Source: (distgit) fedora
        NVR: python-intbitset-3.0.2-1.fc40
        Commit/branch: 76f7812c9bcf9a74a4a0b4ce4cf7f199060ea70d
    python-lazy-object-proxy: 
        Source: (distgit) fedora
        NVR: python-lazy-object-proxy-1.10.0-1.fc40
        Commit/branch: dfe58f61ab88a0855f89629d56bf4c2b8493efda
    python-mplcairo: 
        Source: (distgit) fedora
        NVR: python-mplcairo-0.5-7.fc39
        Commit/branch: 897b162d32b493cda6ea45d583c3eb7fdda7c05a
    python-orderedset: 
        Source: (distgit) fedora
        NVR: python-orderedset-2.0.3-11.fc39
        Commit/branch: 95765d941f30114e9cb7f12229a4c794d30cdca1
    python-psutil: 
        Source: (distgit) fedora
        NVR: python-psutil-5.9.5-2.fc40
        Commit/branch: 5e48a77b0f0817d7dd4e6f4d02c4acbbf1a9e72f
    python-pyclipper: 
        Source: (distgit) fedora
        NVR: python-pyclipper-1.2.0-10.fc39
        Commit/branch: e88a6011c0d2449c72867bab791c5ebde3a56f7a
    python-pygit2: 
        Source: (distgit) fedora
        NVR: python-pygit2-1.13.3-1.fc40
        Commit/branch: 488f7a1c539bc8c773d8b722aa09222d5fadfd60
    python-pyshtools: 
        Source: (distgit) fedora
        NVR: python-pyshtools-4.10.4-1.fc40
        Commit/branch: 219639f1fd4125e847058665b5b8382b0ea19f8a
    python-pywlroots: 
        Source: (distgit) fedora
        NVR: python-pywlroots-0.16.4-1.fc40
        Commit/branch: 107e75237c134c542c9adfd06a8f181e0284fbc2
    python-reproject: 
        Source: (distgit) fedora
        NVR: python-reproject-0.13.0-2.fc40
        Commit/branch: 5a7acf7950cbd4488202880cdcbab9b02b7c9938
    python-rtmidi: 
        Source: (distgit) fedora
        NVR: python-rtmidi-1.3.1-12.fc39
        Commit/branch: 2e0644c83f0dc9c73c682156de6460c7fa214c8e
    python-scikit-build-core: 
        Source: (distgit) fedora
        NVR: python-scikit-build-core-0.6.1-1.fc40
        Commit/branch: a3f1645580c15e906eb8ff8792546ea374df0ad8
    python-scss: 
        Source: (distgit) fedora
        NVR: python-scss-1.4.0-3.fc39
        Commit/branch: 4fe23e57defd672b73c6818a18437219691ecd24
    python-smartcols: 
        Source: (distgit) fedora
        NVR: python-smartcols-0.3.0-20.fc39
        Commit/branch: ef606fe099b7708837753baa16059fb24fa6ac04
    python-zmq: 
        Source: (distgit) fedora
        NVR: python-zmq-25.1.1-3.fc40
        Commit/branch: cecf430ee5c7edca401bd4aa945e1f50cff326e8
    python3.10: 
        Source: (distgit) fedora
        NVR: python3.10-3.10.13-2.fc40
        Commit/branch: 034e3e58be9a5246404c09b6c871fc039004ee9f
    python3.11: 
        Source: (distgit) fedora
        NVR: python3.11-3.11.7-2.fc40
        Commit/branch: 7434c7a69a4493b00898fabf631b78f8a79a0a6b
    python3.13: 
        Source: (distgit) fedora
        NVR: python3.13-3.13.0~a2-2.fc40
        Commit/branch: c01336e5dcfbedd316aaa9be157fd520e7fe957b
    python3.6: 
        Source: (distgit) fedora
        NVR: python3.6-3.6.15-22.fc40
        Commit/branch: d0484132530c1bc572ac059c06e686fe7c4df210
    python3.8: 
        Source: (distgit) fedora
        NVR: python3.8-3.8.18-3.fc40
        Commit/branch: 10524d7a1c6f7ff0251b7808f8b0f1aafd73ae19
    python3.9: 
        Source: (distgit) fedora
        NVR: python3.9-3.9.18-3.fc40
        Commit/branch: 1341447949b1e50097b7173f7e95e5f0850ce84b
    qemu-sanity-check: 
        Source: (distgit) fedora
        NVR: qemu-sanity-check-1.1.6-11.fc40
        Commit/branch: ca33a5b7b4924c7bc4454f1b140a4b6d3687a948
    qm-vamp-plugins: 
        Source: (distgit) fedora
        NVR: qm-vamp-plugins-1.7.1-19.fc39
        Commit/branch: 4c6461d61a3bae347ed4174bcdf659e1524f9674
    qmlkonsole: 
        Source: (distgit) fedora
        NVR: qmlkonsole-24.01.85-1.fc40
        Commit/branch: 7d526353ef184b3cac2c9e4239afb674f3542c1a
    rEFInd: 
        Source: (distgit) fedora
        NVR: rEFInd-0.13.3.1-6.fc38
        Commit/branch: 4ff57d34898de256ba0c77446c3ef940324516f9
    raft: 
        Source: (distgit) fedora
        NVR: raft-0.18.3-1.fc40
        Commit/branch: 584e56e5dc4913e18f3f7681f3a09be01a3f7c97
    raptor2: 
        Source: (distgit) fedora
        NVR: raptor2-2.0.15-39.fc39
        Commit/branch: b2593e71164ddfef1429e8b1e07a66cb600a90c7
    rawstudio: 
        Source: (distgit) fedora
        NVR: rawstudio-2.1-0.35.20210527.gitc140a5e.s20231112gitc753388.fc40
        Commit/branch: a679cb59df16da842efb68cf31c82814eff8d8ea
    recoll: 
        Source: (distgit) fedora
        NVR: recoll-1.36.1-1.fc40
        Commit/branch: b5763483764a61da319540e1c9f4b0e5b2b1742e
    river: 
        Source: (distgit) fedora
        NVR: river-0.2.1-3.fc39
        Commit/branch: 0f41d11200866a971af2f3fee9d0b53d9233dd22
    roc-toolkit: 
        Source: (distgit) fedora
        NVR: roc-toolkit-0.2.1-2.fc38
        Commit/branch: f865099a01de1ddcccaa26338b76e91567d116cf
    rocksndiamonds: 
        Source: (distgit) fedora
        NVR: rocksndiamonds-4.3.2.0-4.fc39
        Commit/branch: 36580bc2792fcd5ca5a15b54606aac9ff43967ad
    rocm-runtime: 
        Source: (distgit) fedora
        NVR: rocm-runtime-6.0.0-1.fc40
        Commit/branch: dc15ea99a8d832d6f1f407d57d813781fae1fcf1
    root: 
        Source: (distgit) fedora
        NVR: root-6.30.02-4.fc40
        Commit/branch: b4f122310f19cf59072c0e56c268d251d5e34b74
    rust-afterburn: 
        Source: (distgit) fedora
        NVR: rust-afterburn-5.4.3-1.fc40
        Commit/branch: b4cca435b6b03ca15b79ad946bac3c86d709c67d
    rust-drg: 
        Source: (distgit) fedora
        NVR: rust-drg-0.5.1-6.fc37
        Commit/branch: 5567007e8fc83556e9c0f0c1f03ea082ad822306
    rust-pretty-bytes: 
        Source: (distgit) fedora
        NVR: rust-pretty-bytes-0.2.0-4.fc39
        Commit/branch: 217a2716d14c39ae58928d93efe3b6e0ce78d353
    s390utils: 
        Source: (distgit) fedora
        NVR: s390utils-2.30.0-1.fc40
        Commit/branch: 0f03699129760490a28f63770bf1a4d59862cdcf
    satyr: 
        Source: (distgit) fedora
        NVR: satyr-0.42-4.fc39
        Commit/branch: 68b57f10297cd7ae4e59b80166dd7817dfbca372
    sawfish: 
        Source: (distgit) fedora
        NVR: sawfish-1.13.0-4.fc39
        Commit/branch: cc67523e674e5bf1fcc2d5b3b4ddd95368faadd6
    sc: 
        Source: (distgit) fedora
        NVR: sc-7.16-22.fc39
        Commit/branch: ae077f22c0ac8bf690ed574523a220d8781b539b
    scidavis: 
        Source: (distgit) fedora
        NVR: scidavis-2.9.0-9.fc39
        Commit/branch: fb32dee3685db974939065bb42bd814bd6939ec7
    seqan3: 
        Source: (distgit) fedora
        NVR: seqan3-3.3.0-0.6.rc2.fc40
        Commit/branch: cf3067c970de69001aa1448a6e1e62ea83c320d5
    setBfree: 
        Source: (distgit) fedora
        NVR: setBfree-0.8.11-11.fc39
        Commit/branch: 446a8cdf9065f48246d5579bcb2c898d3162b2c0
    sigul: 
        Source: (distgit) fedora
        NVR: sigul-1.1-7.fc38
        Commit/branch: 9fa6b37239a347756c957137f38c347762d4c3b1
    smoldyn: 
        Source: (distgit) fedora
        NVR: smoldyn-2.72-2.fc40
        Commit/branch: f9bbafd09065774ad4ad6b77d8e9e0f3afa6ad93
    softhsm: 
        Source: (distgit) fedora
        NVR: softhsm-2.6.1-5.fc39.7
        Commit/branch: 91a9db733548caa9a08ac5aa2a3b0aa001f085a5
    soprano: 
        Source: (distgit) fedora
        NVR: soprano-2.9.4-31.fc39
        Commit/branch: 606dc36455228094d1b69dab39fc3b182f560223
    spacebar: 
        Source: (distgit) fedora
        NVR: spacebar-23.01.0-5.fc40
        Commit/branch: 327b4f9b89aa419adfbd67991ba0ff8fb028a746
    squashfs-tools-ng: 
        Source: (distgit) fedora
        NVR: squashfs-tools-ng-1.2.0-2.fc40
        Commit/branch: e3297888f5804ed4c1c1b6d1a0a9efc78a86476a
    squeak-vm: 
        Source: (distgit) fedora
        NVR: squeak-vm-4.10.2.2614-32.fc39
        Commit/branch: d79e9a6c210f06b1f961f2dbb2b76abd43322dc5
    srcpd: 
        Source: (distgit) fedora
        NVR: srcpd-2.1.6-2.fc39
        Commit/branch: 117d6b0ccd68abef6d709f7b8412fa900f1eefd4
    strace: 
        Source: (distgit) fedora
        NVR: strace-6.6-1.fc40
        Commit/branch: 5d00846a32456b4b1432ec4ce3b8898ab033297d
    subversion: 
        Source: (distgit) fedora
        NVR: subversion-1.14.2-22.fc40
        Commit/branch: 75534963a4b0f634befa5fa80d457d5239be1308
    sundials2: 
        Source: (distgit) fedora
        NVR: sundials2-2.7.0-13.fc39
        Commit/branch: 37afd874a3f7322de3fa918c4a22a96c2e22c8de
    swift-lang: 
        Source: (distgit) fedora
        NVR: swift-lang-5.8.1-2.fc40
        Commit/branch: 77885c532d3786061eb733e374547495958ee5c4
    sympa: 
        Source: (distgit) fedora
        NVR: sympa-6.2.72-3.fc39.1
        Commit/branch: 265ddf123b2a2b151c9f74c2e9824794ae928684
    taxipilot: 
        Source: (distgit) fedora
        NVR: taxipilot-0.9.2-42.fc39
        Commit/branch: 6720cdbd55322810d882624a6f3b07cba58452dd
    tegrarcm: 
        Source: (distgit) fedora
        NVR: tegrarcm-1.8-16.fc39
        Commit/branch: bea932c85031da9ab2ba9ef81b095156f673ab5f
    telepathy-qt: 
        Source: (distgit) fedora
        Commit/branch: 54b568ab7b64f86e878b0f626c49d8a59fa0f752
    tiled: 
        Source: (distgit) fedora
        NVR: tiled-1.10.2-1.fc39
        Commit/branch: 0ee4af1480146f0cd28db351888ba70ddb406969
    ucl: 
        Source: (distgit) fedora
        NVR: ucl-1.03-36.fc39
        Commit/branch: ac75676845ee434bd0396526a3165d75670d3f2b
    ucx: 
        Source: (distgit) fedora
        NVR: ucx-1.15.0-1.fc40
        Commit/branch: 38482f56276efd36d232de925873998ac2f4d17e
    unicornscan: 
        Source: (distgit) fedora
        NVR: unicornscan-0.4.7-32.fc38
        Commit/branch: 0184fb9420bde42d467a3f34ee0a7b1660838275
    vakzination: 
        Source: (distgit) fedora
        NVR: vakzination-23.01.0-5.fc39
        Commit/branch: 7c84df09c153ed707c6a0eee0c0e119db44da79a
    vcpkg: 
        Source: (distgit) fedora
        NVR: vcpkg-2023.06.22-3.fc39
        Commit/branch: 687b8ccc19cde602157c88984b1669033772c741
    vhostmd: 
        Source: (distgit) fedora
        NVR: vhostmd-1.1-14.fc39
        Commit/branch: 2485551a731bcc88ce469708ae94c22e3a51c617
    wabt: 
        Source: (distgit) fedora
        NVR: wabt-1.0.33-2.fc39
        Commit/branch: 914597eda30b29e35397b16d040eff3c427aff19
    wangle: 
        Source: (distgit) fedora
        NVR: wangle-2023.10.16.00-1.fc40
        Commit/branch: bf4ba338e2e5f1dab7d455a5e0d85acaa36b75d8
    wayfire: 
        Source: (distgit) fedora
        NVR: wayfire-0.8.0-2.fc40
        Commit/branch: 7ba35d04327b9bd8354f8a9a986715c38782385a
    wine-mono: 
        Source: (distgit) fedora
        NVR: wine-mono-8.1.0-1.fc40
        Commit/branch: ccd3189dc30857d9bfa913b253bc1f14c2e1de23
    wv: 
        Source: (distgit) fedora
        NVR: wv-1.2.9-28.fc39
        Commit/branch: 8a7eb570f9be3d0efdf4da1ae538d97a70a4c802
    wxGTK: 
        Source: (distgit) fedora
        NVR: wxGTK-3.2.4-1.fc40
        Commit/branch: 02731d94638b0ee1885c7a8fde164dfed5930ae3
    xmlcopyeditor: 
        Source: (distgit) fedora
        NVR: xmlcopyeditor-1.2.1.3-22.fc39
        Commit/branch: 45dc56b01e7e6700d00f98d2c78f590ae7e277df
    xorg-x11-drv-armada: 
        Source: (distgit) fedora
        NVR: xorg-x11-drv-armada-0.0.0-10.unstable.20180829git78e7116a5.fc38
        Commit/branch: c20c432cd4d6966ca4640252f34544d408db86e2
    xrootd: 
        Source: (distgit) fedora
        NVR: xrootd-5.6.4-1.fc40
        Commit/branch: 07036ad772ccf9a4473482215c73dc533c2a0047
    xu4: 
        Source: (distgit) fedora
        NVR: xu4-1.1-0.46.20150221svn3087.fc39
        Commit/branch: bcdc79355fcc0a530c3a3c6f2dca6bba180f2dc4
    yyjson: 
        Source: (distgit) fedora
        NVR: yyjson-0.8.0-1.20231205gite0bacd5.fc40
        Commit/branch: 4d90f7f494d81142ef1478e1793e526f780fdd72
    zig: 
        Source: (distgit) fedora
        NVR: zig-0.9.1-4.fc39
        Commit/branch: cb82d9f39e10c76e260608999f616d12d47a39f9
    zynaddsubfx: 
        Source: (distgit) fedora
        NVR: zynaddsubfx-3.0.6-1.fc38
        Commit/branch: bce8a97c501913335d7f0c9103688b609442f5b1
