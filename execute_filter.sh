#!/bin/bash

IGNORE0="Copr build error:|ERROR: Command|ERROR: Exception\(|error: Bad exit status|Error: \$"
IGNORE1="Cannot download, all mirrors were already tried without success"
IGNORE2="Errors during downloading metadata for repository"
IGNORE3="but none of the providers can be installed"
IGNORE4="redefinition of ‘class std::numeric_limits<__float128>’"

IGNORE="${IGNORE0}|${IGNORE1}|${IGNORE2}|${IGNORE3}|${IGNORE4}"

INT_CONVERSION="\[-Wint-conversion\]"
INCOMPATIBLE="\[-Wincompatible-pointer-types\]"
IMPLICIT="\[-Wimplicit-function-declaration\]"
IMPLICIT_INT="\[-Wimplicit-int\]"
RETURN_MISMATCH="\[-Wreturn-mismatch\]"

C99="${INT_CONVERSION}|${INCOMPATIBLE}|${IMPLICIT}|${IMPLICIT_INT}|${RETURN_MISMATCH}"

CALLOC="\[-Werror=calloc-transposed-args\]"

DWARF2OUT="dwarf2out_frame_debug_cfa_offset"
REGISTER_TUPLE="register_tuple_type"
SEGV="Segmentation fault"
AT_DIE_REF="add_AT_die_ref"
BACKWARD_PASS="backward_pass"

ICE="${DWARF2OUT}|${REGISTER_TUPLE}|${SEGV}|${AT_DIE_REF}|${BACKWARD_PASS}"
ICE="internal compiler error"

PARSE_ERROR="parse error before"

./filter.sh -i $1 -c "CALLOC" -f ${CALLOC}

./filter.sh -i $1 -c "C99" -f ${C99}

./filter.sh -i $1 -c "ICE/DWARF2OUT" -f ${DWARF2OUT}
./filter.sh -i $1 -c "ICE/REGISTER_TUPLE" -f ${REGISTER_TUPLE}
./filter.sh -i $1 -c "ICE/SEGV" -f ${SEGV}
./filter.sh -i $1 -c "ICE/AT_DIE_REF" -f ${AT_DIE_REF}
./filter.sh -i $1 -c "ICE/BACKWARD_PASS" -f ${BACKWARD_PASS}

./filter.sh -i $1 -c "everything_else" -n -f "${ICE}|${C99}|${CALLOC}|${IGNORE}"
